! $Id: biem_laplace.f90,v 1.9 2006/10/19 21:59:16 kris Exp kris $
program laplace_biem_main
implicit none

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! DECLARE VARIABLES

!!!!! constants
integer, parameter :: DP = selected_real_kind(p=15,r=300)
integer, parameter :: I4B = selected_int_kind(r=8)
real(DP), parameter :: EPS = 1.0D-6  ! a small number
real(DP), parameter :: PI = 3.141592653589793238462643383279503_DP
real(DP), parameter :: TWOPI = 6.28318530717958647692528676655901_DP
real(DP), parameter :: ZERO = 0.0_DP, HALF = 0.5_DP, ONE = 1.0_DP, TWO = 2.0_DP

!!!!! element coordinates and connectivity (G = global, L = local)
real(DP), allocatable :: coordG(:,:), coordL(:,:), bc(:,:)
integer(I4B), allocatable :: connectG(:,:), connectL(:), type(:), indx(:)
!!!!! solution vectors/matricies
real(DP), allocatable :: sol_u(:,:), sol_t(:,:), sol_tu(:,:), obsLoc(:,:), obsRes(:,:)
!!!!! things related to Gauss Quadrature
real(DP), dimension(8) :: GaussCoord, GaussWeight, GLCoord, GLWeight, xsi2

!! Locally-indexed matricies of boundary integrals => H=flux, G=potential
!! Globally-indexed matrix of unknown boundary values => A
!! Globally-indexed vector of known/unknown boundary quantities => b, x
real(DP), allocatable :: G(:,:,:), H(:,:,:), A(:,:), b(:), x(:)

integer(I4B) :: nElement, nodesPer, maxNode, intOrd, nObsPts,otherBC(2)
integer(I4B) ::  i, j, e, n ,m
!!!!! Jacobian and intermediate results
real(DP) :: Jac, Jac2, int1, int2, k, J1, J2, arg, offdiag, length
!!$!!!!! intermediate results during integration inside domain
real(DP) :: delT,delU,delSx,delSy,delRx,delRy
!!!!! basis functions at gauss-points
real(DP), allocatable :: GN(:,:), GLN(:,:)
!!!!! 2D vectors
real(DP), dimension(2) :: norm, field, n1, n2

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! INPUT PARAMETERS + ECHO TO OUTPUT

open(unit=20, file='input.dat', status='old', action='read')
open(unit=30, file='output.dat', status='replace', action='write')

read(20,*) nElement, nodesPer, intOrd, nObsPts, k
write(30,*) 'number of elements= ',nElement
write(30,*) 'number of nodes per element= ',nodesPer
write(30,*) 'integration order= ',intOrd
write(30,*) 'number of calcualtion points= ', nObsPts
write(30,*) 'hydraulic conductivity = ', k

allocate(connectG(nodesPer,nElement),GN(nodesPer,8),GLN(nodesPer,8),&
     & type(nElement), bc(nElement,nodesPer))

do i=1,nElement
   ! loop over elements, reading in global indicies, bc type, and bc values
   read(20,*) connectG(1:nodesPer,i), type(i), bc(i,1:nodesPer)
   write(30,*) 'elem:',i,' node:',connectG(1:nodesPer,i),&
        & ' bc:',type(i),' bc_val:',bc(i,1:nodesPer)
end do

maxNode = maxval(connectG(:,:))
allocate(coordG(2,maxNode), connectL(nodesPer), coordL(2,nodesPer),&
     & obsLoc(2,nObsPts))

do i=1,maxNode
   ! loop over nodes, reading in global coordinates (x,y)
   ! type = 1 : Dirichlet
   ! type = 2 : Neumann
   read(20,*) coordG(1:2,i)
end do

! echo input node coordinates for debugging ###############
do i=1,maxNode
   write(30,'(A5,I3,A7,2(1X,F7.3))') 'node:',i,' (x,y):',coordG(:,i)
end do

do i=1,nObsPts
   read(20,*) obsLoc(:,i)
end do

! echo input observation locations inside domain for debugging ###############
do i=1,nObsPts
   write(30,'(A8,I3,A7,2(1X,F7.3))') 'calc_pt:',i,' (x,y):',obsLoc(:,i)
end do

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! INTEGRATION OF GREEN'S FUNCTIONS + BASIS FUNCTIONS

! matricies which hold numerical integration results
allocate(H(maxNode,nElement,nodesPer),G(maxNode,nElement,nodesPer),&
     & A(maxNode,maxNode),b(maxNode),x(maxNode),indx(maxNode))

! initialize
H = ZERO; G = ZERO
call GaussSetup(intOrd,GaussCoord,GaussWeight)  ! same intOrd for each element

!!$write(30,*) 'integration initialization:'
!!$write(30,*) intOrd,'xsi:',GaussCoord(1:intOrd),'w:',GaussWeight(1:intOrd)

! calculate basis functions for all nodes & gauss pts
GN(1:nodesPer,1:intOrd) = basis(GaussCoord(1:intOrd),nodesPer)

!!$write(30,*) 'basis function at Gauss Pts:'
!!$do i=1,intOrd
!!$   write(30,'(A,I2,2(A,F7.4))') 'xsi:',i,' phi1:',GN(1,i),' phi2:',GN(2,i)
!!$end do

do i=1,maxnode  ! collocation point (source pt)
   do e=1,nElement  ! elements (field points)
      connectL(1:nodesPer) = connectG(1:nodesPer,e)
      coordL(1:2,1:nodesPer) = coordG(1:2,connectL(1:nodesPer))

!!! assumes straigh-line elements, with nodes 1 and 2 at ends of element
      length = sqrt(dot_product(coordL(:,1)-coordL(:,2),coordL(:,1)-coordL(:,2)))
!!$      write(30,'(2(A,I2),A,F8.5)') 'i:',i,' e:',e,' length:',length

      do n=1,nodesPer ! local numbering of nodes

         ! non-singular integrations *********
         if(i /= connectL(n)) then
            call normalJacobian(norm(1:2),Jac,ZERO,2,coordL(1:2,1:nodesPer))
!!$            write(30,'(3(A,I2),2(A,F7.4),1X,F7.4)') 'std:',i,' el:',e,' node:',n,' Jac:',Jac,' norm:',norm

            do m=1,intOrd             
               ! calculate location of mth Gauss point in global coordinates
               field(1:2) = sum(spread(GN(1:nodesPer,m),dim=1,ncopies=2)* &
                    & coordL(1:2,1:nodesPer),dim=2)
!!$               write(30,'(A,I2,A,F7.4,1X,F7.4)') 'gauss pt ',m,' (x,y):',field(1:2)
               
               ! potential integral
               G(i,e,n) = G(i,e,n) + &
                    & GN(n,m)*u(coordG(1:2,i),field(1:2))*Jac*GaussWeight(m)

               ! flux integral
               H(i,e,n) = H(i,e,n) + GN(n,m)*&
                    & q(coordG(1:2,i),field(1:2),norm(1:2))*Jac*GaussWeight(m)
            end do
            write(30,'(2(I3),1X,2(A,F8.5))') e,n,'G:',G(i,e,n),' H:',H(i,e,n)
          
         ! singular integrations (diagonals) **********
         ! collocation point coincides with this node
         elseif(i == connectL(n)) then 

!!$            H(i,e,n) = ZERO; ! will be set later

            ! determine H(i,i) for singular integral 
            ! (fraction of angle or -sum of off-diagonal terms)
            otherBC(1:2) = nextBC(e,n)
            call normalJacobian(n1(1:2),J1,ZERO,2,coordL(1:2,1:2)) ! this element
            call normalJacobian(n2(1:2),J2,ZERO,2,&
                 & coordG(1:2,connectG(1:2,otherBC(1)))) !adjoining element

            ! fraction of whole circle this interior angle makes
            ! 180 - theta = interior angle; where theta is angle between normals
            
            if(n1(1)==n2(1) .and. n1(2)==n2(2)) then ! a flat stretch
               H(i,e,n) = HALF*HALF
            else ! some other angle
               arg = dot_product(n1,n2)/(J1*J2)
               if (arg > ONE) then  ! reflect about x=1
                  arg = ONE - (arg - ONE)
               end if
               H(i,e,n) = (PI - acos(arg))/TWOPI*HALF
            end if

               !!! Analytic solutions for singular integrals            
            if (nodesPer == 2) then
               ! determine G(i,i) terms for singular integrals
!!$               call normalJacobian(norm(1:2),Jac,ZERO,2,coordL(1:2,1:nodesPer))
!!$               if(n == 1) then
                  G(i,e,n) = length/(TWO*k*TWOPI)*(1.5_DP + log(ONE/length))
!!$               elseif(n == 2) then
!!$                  G(i,e,n) = length/(TWO*k)*(HALF + log(ONE/length))
!!$               end if
            end if

!!$            write(30,'(3(A,I2))') 'SNG:',i,' el:',e,' node:',n
            write(30,'(2(I3),1X,2(A,F8.5))') e,n,'sG:',G(i,e,n), 'sH:',H(i,e,n)
         end if
      end do
   end do
end do

!!$do i=1,maxNode
!!$   offdiag=ZERO
!!$
!!$   ! determine sum of off diagonal terms
!!$   do e=1,nElement
!!$      connectL(1:nodesPer) = connectG(1:nodesPer,e)
!!$      do n=1,2
!!$         if(i/=connectL(n)) then ! off-diagonal terms
!!$            offdiag = offdiag + H(i,e,n)
!!$         end if
!!$      end do
!!$   end do
!!$   
!!$   ! set diagonal terms based on calcuated sum
!!$   do e=1,nElement
!!$      connectL(1:nodesPer) = connectG(1:nodesPer,e)
!!$      do n=1,2
!!$         if(i==connectL(n)) then ! on-diagonal terms
!!$            write(30,'(A,F8.5,1X,F8.5)') 'H ',H(i,e,n), offdiag*HALF
!!$         end if
!!$      end do
!!$   end do
!!$end do

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! ASSEMBLE GLOBAL LHS AND RHS FROM LOCALLY INDEXED MATRICIES AND SPECIFIED BC

A(:,:) = ZERO; b(:) = ZERO   ! lhs, rhs

! DelU == G (potential integral)*(flux); begins on RHS
! DelT == H (flux integral)*(potential); begins on LHS

do e=1,nElement
   connectL(1:nodesPer) = connectG(1:nodesPer,e)
   do n=1,nodesPer
      select case(type(e))
      case(2) ! Neumann bc
         ! flux is known (put on RHS)
         ! set rhs: (computed potential integral)*(specified normal flux)
!!$         write(30,'(2(A,F8.5),2(I3))') 'b= +G:',G(1,e,n),' II bc',bc(e,n),e,n
         b(1:maxNode) = b(1:maxNode) + G(1:maxNode,e,n)*bc(e,n)
         

         ! determine other element which shares this node
         otherBC(1:2) = nextBC(e,n)

         select case(type(otherBC(1)))
         case(2) ! adjoining element is also a Neumann bc
            ! potential is unknown (put on LHS)
            ! set lhs: computed flux integral
!!$            write(30,'(1(A,F8.5))') 'A= +H:',H(1,e,n)
            A(1:maxNode,connectL(n)) = A(1:maxNode,connectL(n)) + H(1:maxNode,e,n)
         case(1) ! adjoining element is Dirichlet bc
            ! potential is known (put on RHS)
            ! set rhs: (-1)*(computed flux integral)*(specified head)
!!$            write(30,'(2(A,F8.5),2(I3))') 'b= -H:',-H(1,e,n),' I bc:',bc(otherBC(1),otherBC(2)),e,n
            b(1:maxNode) = b(1:maxNode) - H(1:maxNode,e,n)*bc(otherBC(1),otherBC(2))
         end select
      case(1) ! Dirichlet bc
         ! flux is unknown (put on LHS)
         ! set lhs: (-1)*(computed head integral)
!!$         write(30,'(1(A,F8.5))') 'A= -G:',-G(1,e,n)
         A(1:maxNode,connectL(n)) = A(1:maxNode,connectL(n)) - G(1:maxNode,e,n)
         ! head is known (put on RHS)
         ! set rhs: (-1)*(computed flux integral)*(specified head)
!!$         write(30,'(2(A,F8.5),2(I3))') 'b= -H:',-H(1,e,n),' I bc:',bc(e,n),e,n
         b(1:maxNode) = b(1:maxNode) - H(1:maxNode,e,n)*bc(e,n)
      case default
         print *, 'only type I or II bc currently handled ',bc(e,n)
         stop
      end select
   end do
end do

!!$write(30,*) 'A:'
!!$do i=1,maxNode
!!$   write(30,*) 'row',i,A(i,:)
!!$end do
!!$
!!$write(30,*) 'b:'
!!$write(30,*) b(:)


!!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! SOLVE MATRIX PROBLEM USING LU DECOMPOSITION

call ludcmp(A,indx)
x = b
call lubksb(A,indx,x)

write(30,*) 'x:',x

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! BUILD UP VECTORS OF BOUNDARY HEAD AND N-FLUX FOR POST-PROCESSING

allocate(sol_u(nElement,nodesPer),sol_t(nElement,nodesPer),&
     & sol_tu(nElement,nodesPer), obsRes(nObsPts,3))

do e=1,nElement
   connectL(1:nodesPer) = connectG(1:nodesPer,e)
   do n=1,nodesPer
      select case(type(e))
      case(2)
         ! Neumann BC - specified flux
         sol_t(e,n) = bc(e,n)
         
         otherBC(1:2) = nextBC(e,n)
         select case(type(otherBC(1)))
         case(2)
            ! adjoining element is also Neumann
            ! therefore head was calculated
            sol_u(e,n) = x(connectL(n))
         case(1)
            ! adjoining element is Dirichlet
            ! therefore head was specified (for other element)
            sol_u(e,n) = bc(otherBC(1),otherBC(2))
         end select
      case(1)
         ! Dirichlet BC - specified head, computed flux
         sol_u(e,n) = bc(e,n)
         sol_t(e,n) = x(connectL(n))
      end select
   end do
end do

write(30,*) 'sol_u',sol_u
write(30,*) 'sol_t',sol_t

!!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! CALCULATE TANGENTIAL FLUX ALONG BOUNDARY ELEMENTS

do e=1,nElement
   connectL(1:nodesPer) = connectG(1:nodesPer,e)
   coordL(1:2,1:nodesPer) = coordG(1:2,connectL(1:nodesPer))
   if(nodesPer == 2) then
      call normalJacobian(n1(1:2),Jac,ZERO,2,coordL(1:2,1:2))
   
      sol_tu(e,1) = sum(deriv_basis(-ONE,2)*Jac*sol_u(e,1:2))
      sol_tu(e,2) = sum(deriv_basis(+ONE,2)*Jac*sol_u(e,1:2)) ! same
   end if
end do

!!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! WRITE BOUNDARY RESULTS TO OUTPUT FILE

write(30,*) 'boundary head & flux.  &
     & (element, node, (x,y), head, normal flux, tangent flux)'
do e=1,nElement
   connectL(1:nodesPer) = connectG(1:nodesPer,e)
   coordL(1:2,1:nodesPer) = coordG(1:2,connectL(1:nodesPer))
   do n=1,nodesPer
      write(30,'(2(1X,I3),5(1X,F9.4))') e,n,coordL(1:2,n),sol_u(e,n),sol_t(e,n),sol_tu(e,n)
   end do
end do

open(unit=40,file='output-plot.dat',status='replace',action='write')

! write potential, x-flux and y-flux at boundary nodes
do e=1,nElement
   connectL(1:nodesPer) = connectG(1:nodesPer,e)
   coordL(1:2,1:nodesPer) = coordG(1:2,connectL(1:nodesPer))

   ! more general (assumes fluxes constant over element)
!!$   call normalJacobian(n1(1:2),Jac,ZERO,2,coordL(1:2,1:2))
!!$   write(40,'(5(1X,ES15.7))') coordL(1:2,1),sol_u(e,1),&
!!$        & dot_product((/ONE,ZERO/),(/sol_t(e,1),sol_tu(e,1)/)), &
!!$        & dot_product((/ZERO,ONE/),(/sol_t(e,1),sol_tu(e,1)/))

   ! specific to rectangular domain!!!!
   if(abs(n1(1))-ONE < EPS .and. n1(2) < EPS) then  ! vertical element
      write(40,'(3(1X,ES15.7))') coordL(1:2,1),sol_u(e,1),sol_t(e,1),sol_tu(e,1)
   else ! horizontal element
      write(40,'(3(1X,ES15.7))') coordL(1:2,1),sol_u(e,1),sol_tu(e,1),sol_t(e,1)
   end if
   
end do


!!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! CALCULATE POTENTIAL AND FLUX VECTOR AT POINTS INSIDE DOMAIN

obsRes(:,:) = ZERO ! obs index; head,x-flux,y-flux

do i=1,nObsPts
   do e=1,nElement
      connectL(1:nodesPer) = connectG(1:nodesPer,e)
      coordL(1:2,1:nodesPer) = coordG(1:2,connectL(1:nodesPer))
      call normalJacobian(norm(1:2),Jac,ZERO,2,coordL(1:2,1:nodesPer))

      do n=1,nodesPer

         ! calculate potential inside the domain
         delT=ZERO; delU=ZERO; delSx=ZERO; delSy=ZERO;
         delRx=ZERO; delRy=ZERO;

         do m=1,intOrd
            field(1:2) = sum(spread(GN(1:nodesPer,m),dim=1,ncopies=2)* &
                 & coordL(1:2,1:nodesPer),dim=2)

            ! potential
            delU = delU + GN(n,m)*u(obsLoc(:,i),field(:))*Jac*GaussWeight(m)
            delT = delT + &
                 & GN(n,m)*q(obsLoc(:,i),field(:),norm(:))*Jac*GaussWeight(m)

            ! x-flux
            delSx = delSx + GN(n,m)*Dux(obsLoc(:,i),field(:))*Jac*GaussWeight(m)
            delRx = delRx + &
                 & GN(n,m)*Dqx(obsLoc(:,i),field(:),norm(:))*Jac*GaussWeight(m)
            
            ! y-flux
            delSy = delSy + GN(n,m)*Duy(obsLoc(:,i),field(:))*Jac*GaussWeight(m)
            delRy = delRy + &
                 & GN(n,m)*Dqy(obsLoc(:,i),field(:),norm(:))*Jac*GaussWeight(m)
         end do
         ! potential
         obsRes(i,1) = obsRes(i,1) - delT*sol_u(e,n) + delU*sol_t(e,n) 
         ! x-flux
         obsRes(i,2) = obsRes(i,2) - delRx*sol_u(e,n) + delSx*sol_t(e,n)  
         ! y-flux
         obsRes(i,3) = obsRes(i,3) - delRy*sol_u(e,n) + delSy*sol_t(e,n) 
      end do
   end do
end do

! apply conductivity to fluxes
obsRes(:,2:3) = k*obsRes(:,2:3)

!!! write results to a table
write(30,*) 'results inside the domain: (x,y), head, x-flux, y-flux'
do i=1,nObsPts
   write(30,'(5(1X,F9.4))') obsLoc(1:2,i),obsRes(i,1:3)
   write(40,'(5(1X,ES15.7))') obsLoc(1:2,i),obsRes(i,1:3)
end do
close(20); close(30); close(40)


!!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
!!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
! end of program flow

contains

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! ABCISSA AND WEIGHTS FOR GAUSS QUADRATURE

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subroutine GaussSetup(intOrd,coord,weight)
  integer(I4B), intent(in) :: intOrd    ! Gauss integration order
  real(DP), intent(out), dimension(:) :: coord,weight

  ! initialize
  coord(:) = -9.99E9
  weight(:) = ZERO

  ! Abramowitz & Stegun Handbook (Table 25.4), p916
  select case (intOrd)
  case(1)  ! one Gauss point (center)
     coord(1) = ZERO
     weight(1) = TWO
  case(2)  ! two points
     coord(1) = 0.577350269189626_DP
     coord(2) = -coord(1)
     weight(1:2) = ONE
  case(3)  ! three points
     coord(1:2) = (/ 0.774596669241483_DP, ZERO /)
     coord(3) = -coord(1)
     weight(1:2) = (/ 0.555555555555556_DP, 0.888888888888889_DP /)
     weight(3) = weight(1)
  case(4)  ! four points
     coord(1:2) = (/ 0.861136311594053_DP, 0.339981043584856_DP /)
     coord(3:4) = -coord(2:1:-1)
     weight(1:2) = (/ 0.347854845137454_DP, 0.652145154862546_DP /)
     weight(3:4) = weight(2:1:-1)
  case(5)  ! five points
     coord(1:3) = (/ 0.906179845938664_DP, 0.538469310105683_DP, &
                   &  ZERO /)
     coord(4:5) = -coord(2:1:-1)
     weight(1:3) = (/ 0.236926885056189_DP, 0.478628670499366_DP, &
                   &  0.568888888888889_DP /)
     weight(4:5) = weight(2:1:-1)
  case(6)  ! six points
     coord(1:3) = (/ 0.932469514203152_DP, 0.661209386466265_DP, &
                   & 0.238619186083197_DP /)
     coord(4:6) = -coord(3:1:-1)
     weight(1:3) = (/ 0.171324492379170_DP, 0.360761573048139_DP, &
                   &  0.467913934572691_DP /)
     weight(4:6) = weight(3:1:-1)
  case(7)
     coord(1:4) = (/ 0.949107912342759_DP, 0.741531185599394_DP, &
                   & 0.405845151377397_DP, ZERO /)
     coord(5:7) = -coord(3:1:-1)
     weight(1:4) = (/ 0.129484966168870_DP, 0.279705391489277_DP, &
                    & 0.381830050505119_DP, 0.417959183673469_DP /)
     weight(5:7) = weight(3:1:-1)
  case(8)
     coord(1:4) = (/ 0.960289856497536_DP, 0.796666477413627_DP, &
                   & 0.525532409916329_DP, 0.183434642495650_DP /)
     coord(5:8) = -coord(4:1:-1)
     weight(1:4) = (/ 0.101228536290376_DP, 0.222381034453374_DP, &
                    & 0.313706645877887_DP, 0.362683783378362_DP /)
     weight(5:8) = weight(4:1:-1)
  case default
     print *, 'unsupported Gaussian Quadrature integration order:', intOrd
     stop
  end select

end subroutine GaussSetup

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! DETERMINE WHICH ELEMENT SHARES CURRENT NODE (2D, 2 NODE ELEMENTS ONLY)

function nextBC(ee,nn) result(otherBC)
  integer(I4B), intent(in) :: ee,nn ! element and node 
  integer(I4B), dimension(2) :: otherBC
  
  integer(I4B) :: ii,jj

  ! this uses global variables defined in parent (which contains this function);
  ! it is sloppy coding, but it makes the function's arguments much simpler

  findBC: do ii=1,nElement 
     if (ii/=ee) then ! loop over other elements
        do jj=1,nodesPer ! loop over their nodes
           ! find other node with same global index
           if (connectG(jj,ii) == connectL(nn)) then 
              otherBC(1:2) = (/ ii,jj /)
              exit findBC ! done, only one node to find
           end if
        end do
     end if
  end do findBC

end function nextBC

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! BASIS FUNCTIONS, NORMALS AND JACOBIANS

! derivative of the basis functions
function deriv_basis(xsi,nodes) result(Dxsi)
  real(DP), intent(in) :: xsi
  integer, intent(in) :: nodes
  real(DP), dimension(nodes) :: Dxsi

  Dxsi(:) = ZERO

  ! derivative of basis functions
  select case(nodes)
  case(1)
     Dxsi(1) = ZERO    
  case(2)
     Dxsi(1:2) = (/ -HALF, HALF /)
  case(3)
     Dxsi(1:3) = (/ xsi-HALF, xsi+HALF, -TWO*xsi /)
  case default
     print *, 'number of nodes per element not implemented:', nodes
     stop
  end select

end function deriv_basis


!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subroutine normalJacobian(norm,jac,xsi,nodes,coord)
  real(DP), dimension(2), intent(out) :: norm  ! normal to element
  real(DP), intent(out) :: jac         ! jacobian*2 = element length (linear elements)
  real(DP), intent(in) :: xsi          ! arc length where jac & norm are needed 
  ! (same for all xsi for linear elements)
  integer(I4B), intent(in) :: nodes    ! # nodes per element
  real(DP), intent(in) :: coord(:,:)   ! global coords of nodes in element

  real(DP), dimension(size(coord,dim=2)) :: Dbasis
  real(DP), dimension(2) :: tangent 

  ! get derivatives of basis functions
  Dbasis(1:nodes) = deriv_basis(xsi,nodes)
  
  ! sum of deriv of basis * location vector across all nodes
  tangent(1:2) = sum(spread(Dbasis(:),dim=1,ncopies=2)*coord(1:2,:),dim=2)

  norm(1) = +tangent(2)
  norm(2) = -tangent(1)

!!$  norm(1:2) = -norm(1:2)  !!! <<< a temporary fix to follow Ligget & Liu's convention

  ! jacobian is length of normal vector
  jac = sqrt(dot_product(norm(:),norm(:)))

  ! normalize normal vector to unit length
  if (jac > 0.0) then
     norm(1:2) = norm(1:2)/jac
  end if

end subroutine normalJacobian

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function basis(xsi, nodes) result(N)
  real(DP), intent(in), dimension(:) :: xsi   ! Gauss pts
  integer(I4B), intent(in) :: nodes           ! number of nodes (# of basis fcns) 
  real(DP), dimension(nodes,size(xsi)) :: N   ! all the basis fcns at all the pts

  integer(I4B) :: nPts

  ! first dimension of basis is the basis function
  ! second dimension of basis is the gauss pt location

  nPts = size(xsi)
  if(maxval(abs(xsi)) > ONE) print *, 'local coordinate out of range:',xsi

  select case(nodes)
  case(1)
     ! constant elements (not a function of xsi)
     N(1,1:nPts) = ONE
  case(2)
     ! linear elements
     N(1,1:nPts) = HALF*(ONE - xsi(:)) ! node 1 at xsi=-1
     N(2,1:nPts) = HALF*(ONE + xsi(:)) ! node 2 at xsi=+1
  case(3)
     ! quadratic elements
     N(3,1:nPts) = ONE - xsi(:)**2   ! node 3 at xsi=0
     N(1,1:nPts) = HALF*xsi(:)*(xsi(:) - ONE) ! node 1 at xsi=-1
     N(2,1:nPts) = HALF*xsi(:)*(xsi(:) + ONE) ! node 2 at xsi=+1
  case default
     print *, 'unsupported number of element nodes:',nodes
  end select

end function basis

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! GREEN'S FUNCTIONS

! basic 2D Green's function
!##################################################
function u(x1,x2) result(pot)
  real(DP), intent(in), dimension(2) :: x1,x2 ! source pt (P), field pt (Q)
  real(DP) :: pot  ! potential at each point
  real(DP), dimension(2) :: r ! vector pointing from source to field point

  r(1:2) = x2(1:2) - x1(1:2)

  ! Green's function for 2D Laplace solution
  pot = log(ONE/sqrt(dot_product(r,r)))/(TWOPI*k)

end function u

! x-deriv of basic greens fcn
!##################################################
function Dux(x1,x2) result(dpot)
  real(DP), intent(in), dimension(2) :: x1,x2 ! source pt (P), field pt (Q)
  real(DP) :: distsq,dpot  ! potential at each point
  real(DP), dimension(2) :: r ! vector pointing from source to field point

  r(1:2) = x2(1:2) - x1(1:2)
  distsq = dot_product(r,r)

  ! Green's function for 2D Laplace solution
  dpot = r(1)/(TWOPI*distsq)

end function Dux

! y-deriv of basic greens fcn
!##################################################
function Duy(x1,x2) result(dpot)
  real(DP), intent(in), dimension(2) :: x1,x2 ! source pt (P), field pt (Q)
  real(DP) :: distsq,dpot  ! potential at each point
  real(DP), dimension(2) :: r ! vector pointing from source to field point

  r(1:2) = x2(1:2) - x1(1:2)
  distsq = dot_product(r,r)

  ! Green's function for 2D Laplace solution
  dpot = r(2)/(TWOPI*distsq)

end function Duy


! directional derivative of Green's function 
!##################################################
function q(x1,x2,nn) result(flux)
  real(DP), intent(in), dimension(2) :: x1,x2,nn  ! source pt, field pt, unit normal
  real(DP) :: distsq,flux  ! length of r squared, deriviative of potential
  real(DP), dimension(2) :: r ! vector pointing from source to field point

  r(1:2) = x2(1:2) - x1(1:2)
  distsq = dot_product(r,r)
  
  flux = -dot_product(nn,r)/(TWOPI*distsq)

end function q

! X-deriv of normal derivative of Green's function
!##################################################
function Dqx(x1,x2,nn) result(Dfx)
  real(DP), intent(in), dimension(2) :: x1,x2,nn  ! source pt, field pt, normal
  real(DP) :: Dfx, distsq
  real(DP), dimension(2) :: r ! vector pointing from source to field point

  r(1:2) = x2(1:2) - x1(1:2)
  distsq = dot_product(r,r)
  
  Dfx = nn(1)/(TWOPI*distsq) - dot_product(nn,r)*r(1)/(PI*distsq**2)

end function Dqx

! Y-deriv of normal derivative of Green's function
!##################################################
function Dqy(x1,x2,nn) result(Dfy)
  real(DP), intent(in), dimension(2) :: x1,x2,nn  ! source pt, field pt, normal
  real(DP) :: Dfy, distsq
  real(DP), dimension(2) :: r ! vector pointing x2 -> x1

  r(1:2) = x2(1:2) - x1(1:2)
  distsq = dot_product(r,r)
  
  Dfy = nn(2)/(TWOPI*distsq) - dot_product(nn,r)*r(2)/(PI*distsq**2)

end function Dqy

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! MATRIX SOLVER (LU DECOMPOSITION)
! simplified version of NR version
! Press, etal, Numerical Recipes in f90, p1016
!##################################################
subroutine ludcmp(a,indx)
  real(DP), dimension(:,:), intent(inout) :: a
  integer(I4B), dimension(:), intent(out) :: indx
  real(DP), dimension(size(a,dim=1)) :: vv
  real(DP), parameter :: TINY = 1.0E-20_DP
  integer(I4B) :: j,n,imax
  
  n = size(a,dim=1)
  vv = maxval(abs(a),dim=2)
  if (any(vv == 0.0)) print *, 'singular matrix in LUDCMP'
  vv = ONE/vv
  do j=1,n
     imax = (j-1) + sum(maxloc(vv(j:n)*abs(a(j:n,j))))
     if (j /= imax) then
        call swap(a(imax,:),a(j,:))
        vv(imax) = vv(j)
     end if
     indx(j) = imax
     if (a(j,j) == 0.0) a(j,j) = TINY
     a(j+1:n,j) = a(j+1:n,j)/a(j,j)
     a(j+1:n,j+1:n) = a(j+1:n,j+1:n) - outerprod(a(j+1:n,j),a(j,j+1:n))
  end do
end subroutine ludcmp

!##################################################
subroutine lubksb(a,indx,b)
  real(DP), intent(in), dimension(:,:) :: a
  integer(I4B), intent(in), dimension(:) :: indx
  real(DP), intent(inout), dimension(:) :: b
  
  integer(I4B) :: i,n,ii,ll
  real(DP) :: summ

  n = size(a,dim=1)
  ii = 0
  do i=1,n
     ll = indx(i)
     summ = b(ll)
     b(ll) = b(i)
     if (ii /= 0) then
        summ = summ - dot_product(a(i,ii:i-1),b(ii:i-1))
     elseif (summ /= 0.0) then
        ii = i
     end if
     b(i) = summ
  end do
  do i=n,1,-1
     b(i) = (b(i) - dot_product(a(i,i+1:n),b(i+1:n)))/a(i,i)
  end do
end subroutine lubksb

! numerical recipes utility routines
!##################################################
subroutine swap(a,b)
  real(DP), intent(inout), dimension(:) :: a,b
  real(DP), dimension(size(a)) :: tmp

  tmp = a
  a = b
  b = tmp
end subroutine swap

function outerprod(a,b) result(c)
  real(DP), intent(in), dimension(:) :: a,b
  real(DP), dimension(size(a),size(b)) :: c

  c = spread(a,dim=2,ncopies=size(b))* &
    & spread(b,dim=1,ncopies=size(a))
end function outerprod

end program laplace_biem_main
