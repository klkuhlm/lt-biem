program dehoog_smoothing 
  use constants, only : DP, PI

  implicit none

  integer, parameter :: NUMT = 150, NFUN = 2

  character(2) :: charf
  real(DP), dimension(NUMT) :: t, ft, ftsm
  real(DP) :: alpha, tol, tee, steep, shift
  integer, parameter :: M = 60

  complex(DP), dimension(2*M+1) :: p, fp, cerr
  integer :: i, k, j !, fcn

  steep = 20.0_DP
  shift = 2.0_DP*PI

  alpha = 1.0D-7
  tol = 1.0D-9

  ! spread times out logarithmically between 1-10
  t = 10.0_DP**([(real(i,DP)/real(NUMT,DP), i=0,NUMT-1)])
  
  do i= 1,NFUN

     write(charf,'(I2.2)') i

  ! loop over times
  do k = 1, NUMT
     tee = PI
     p = cmplx(alpha - log(tol)/(2.0_DP*tee), real([(i,i=0,2*M)],DP)*PI/tee, DP)
     
     select case(i)
     case (1)
        ! simple step at t=shift
        fp = exp(-shift*p)/p
     case(2)
        ! error function shifted to t=shift
        do j = 1, size(p) 
           call cerror(p(j)/(2.0_DP*steep),cerr(j))
        end do
        cerr = 1.0_DP - cerr
        fp = 0.5_DP*((1.0_DP + exp((p/(2.0_DP*steep))**2)*cerr)/p)*exp(-shift*p)
        
     case default
        stop "error"
     end select
     
     ftsm(k) = deHoog_invlap(alpha,tol,t(k),tee,fp(:),M,.true.,.false.)
     ft(k) = deHoog_invlap(alpha,tol,t(k),tee,fp(:),M,.false.,.false.)

  end do
  
  ! output results
  open(unit=20, file='smooth-test'//charf//'.out',status='replace',action='write')

  select case(i)
  case(1)
     write(20,*) '# fcn: Heaviside step at t=',shift
  case(2)
     write(20,*) '# fcn: error function with parameter=',steep,' at t=',shift
  end select
  
  write(20,*) '# time,   actual function,    inverted no smoothing,    inverted with smoothing'
  do k = 1,NUMT
     write(20,*) t(k), funt(t(k),i,shift,steep), ft(k), ftsm(k)
  end do
end do


contains
  real(DP) function funt(t,nfcn,shift2,steep2)
    implicit none
    real(DP), intent(in) :: t
    integer, intent(in) :: nfcn
    real(DP), intent(in) :: shift2, steep2

    select case(nfcn)
    case(1)
       ! standard step at 5
       if(t <= shift2) then
          funt = 0.0_DP
       else
          funt = 1.0_DP
       end if
    case(2)
       funt = 0.5_DP*(derf((t-shift2)*steep2) + 1.0_DP)
    end select
    

  end function funt
  
  !! an implementation of the de Hoog method
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  function deHoog_invLap(alpha,tol,t,tee,fp,M,smooth,accel) result(ft)
    use constants, only : DP, PI, CZERO, CONE, EYE, HALF, RONE, RZERO
    real(DP), intent(in) :: alpha  ! abcissa of convergence
    real(DP), intent(in) :: tol    ! desired accuracy
    real(DP), intent(in) :: t,tee  ! time and scaling factor
    integer, intent(in) :: M
    complex(DP), intent(in), dimension(0:2*M) :: fp
    logical, intent(in) :: smooth, accel
    real(DP) :: ft ! output

    real(DP), dimension(0:2*M) :: run

    real(DP), dimension(0:2*M) :: lanczos
    real(DP) :: gamma, arg, r2M
    integer :: r, rq, n, max, j
    complex(DP), dimension(0:2*M,0:M) :: e
    complex(DP), dimension(0:2*M,1:M) :: q
    complex(DP), dimension(0:2*M) :: d
    complex(DP), dimension(-1:2*M) :: A,B
    complex(DP) :: brem,rem,z

    ! there will be problems is fp(:)=0
    if(maxval(abs(fp)) > epsilon(1.0D0)) then

       lanczos(0:2*M) = RONE
       !! sigma_0 = 1
       !! sigma_N = 0 (last term (one more than your are summing) drops off)
       if(smooth) then
          r2M = real(2*M+1,DP)
          do j=1,2*M
             arg = PI*real(j,DP)/r2M
             lanczos(j) = sin(arg)/arg  !! sinc function
          end do
       end if
       
       ! Re(p) -- this is the de Hoog parameter c
       gamma = alpha - log(tol)/(2.0_DP*tee)

       if(accel) then

       ! initialize Q-D table 
       e(0:2*M,0) = CZERO
       q(0,1) = fp(1)*lanczos(1)/(fp(0)*HALF) ! half first term
       q(1:2*M-1,1) = fp(2:2*M)/fp(1:2*M-1)

       ! rhombus rule for filling in triangular Q-D table
       do r = 1,M
          ! start with e, column 1, 0:2*M-2
          max = 2*(M-r)
          e(0:max,r) = q(1:max+1,r) - q(0:max,r) + e(1:max+1,r-1)
          if (r /= M) then
             ! start with q, column 2, 0:2*M-3
             rq = r+1
             max = 2*(M-rq)+1
             q(0:max,rq) = q(1:max+1,rq-1) * e(1:max+1,rq-1) / e(0:max,rq-1)
          end if
       end do

       ! build up continued fraction coefficients
       d(0) = fp(0)*HALF ! half first term
       forall(r = 1:M)
          d(2*r-1) = -q(0,r) ! even terms
          d(2*r)   = -e(0,r) ! odd terms
       end forall

       ! seed A and B vectors for recurrence
       A(-1) = CZERO
       A(0) = d(0)
       B(-1:0) = CONE

       ! base of the power series
       z = exp(EYE*PI*t/tee)

       ! coefficients of Pade approximation
       ! using recurrence for all but last term
       do n = 1,2*M-1
          A(n) = A(n-1) + d(n)*A(n-2)*z
          B(n) = B(n-1) + d(n)*B(n-2)*z
       end do

       ! "improved remainder" to continued fraction
       brem = (CONE + (d(2*M-1) - d(2*M))*z)*HALF
       rem = -brem*(CONE - sqrt(CONE + d(2*M)*z/brem**2))

       ! last term of recurrence using new remainder
       A(2*M) = A(2*M-1) + rem*A(2*M-2)
       B(2*M) = B(2*M-1) + rem*B(2*M-2)

       ! diagonal Pade approximation
       ! F=A/B represents accelerated trapezoid rule
       ft =  exp(gamma*t)/tee * real(A(2*M)/B(2*M))
    else
       
       lanczos(0) = 0.5_DP  ! put factor of 1/2 into lanczos vector
       run(0:2*M) = real([(i,i=0,2*M)],DP)

       ! unaccelerated trapezoid rule
       ft = exp(gamma*t)/tee * real(sum(lanczos(0:2*M)*fp(0:2*M)*exp(EYE*PI*t*run(0:2*M)/tee)))
    end if
    

    else
       ft = RZERO
    end if

  end function deHoog_invLap



end program dehoog_smoothing
