! $Id: biem_shared_data.f90,v 1.6 2006/11/16 01:37:29 kris Exp $
module biem_shared_data
  use constants, only : DP, I4B

  private
  public :: otherBC, Jac, length, norm

  !! calculated attributes of boundary elements / nodes
  !! but which does not depend on the value of the Laplace parameter
  
  ! elem & node of neighboring element at that node
  integer(I4B), allocatable :: otherBC(:,:,:) 
  real(DP), allocatable :: Jac(:)    ! Jacobian for this element
  real(DP), allocatable :: length(:) ! length of this element (1/Jac)
  real(DP), allocatable :: norm(:,:) ! outward normal for element


end module biem_shared_data

