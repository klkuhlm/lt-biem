! $Id: biem_transient_module.f90,v 1.17 2006/11/20 21:10:36 kris Exp kris $
module biem_transient
  use constants, only : DP, I4B

  implicit none

  private
  public :: biem_transient_solution, gaussSetup, basis

contains

  ! only for 2-node elements currently
  !!##################################################
  subroutine biem_transient_solution(coordG,connectG,intOrd,bc,type,sol_u,sol_t,p,alpha,cond, &
       & obsResults,obsLoc,bcflag)
    use complex_bessel, only : cbesk
    use constants, only : PI, TWOPI, CZERO, HALF, RONE, RTWO, RZERO, PIOV2, EYE
    use biem_shared_data, only : jac,norm,length,otherBC

    real(DP), intent(in), dimension(:,:) :: coordG
    integer(I4B), intent(in), dimension(:,:) :: connectG
    integer(I4B), intent(in) :: intOrd
    real(DP), intent(in), dimension(:,:) :: bc
    integer(I4B), intent(in), dimension(:) :: type
    complex(DP), intent(out), dimension(size(bc,dim=1),2) :: sol_u,sol_t
    complex(DP), intent(in) :: p
    real(DP), intent(in) :: alpha,cond
    real(DP), intent(in) :: obsLoc(:,:) ! (x,y),nObsPts
    complex(DP), intent(out) :: obsResults(size(obsLoc,dim=2),3) ! nObsPts, (pot,x-flux,y-flux)
    integer(I4B), intent(in) :: bcflag

    ! maximum Gauss Quadrature order
    integer, parameter :: MAXINTORD = 10
    real(DP), parameter :: MAXSINGULAR = 25.0_DP

!!!! things related to Gauss Quadrature
    real(DP), dimension(MAXINTORD) :: GaussCoord, GaussWeight

!!!! Locally-indexed matricies of boundary integrals => H=flux, G=potential
    complex(DP), allocatable :: G(:,:,:), H(:,:,:)

!!!! coordinates and connectivity for current element
    real(DP) :: coordL(2,2) ! always 2 dim x 2 nodes per element
    integer(I4B) :: connectL(2)  ! always 2 nodes per element

!!!! Globally-indexed matrix of unknown boundary values => A
!!!! Globally-indexed vector of known/unknown boundary quantities => b, x
    complex(DP), allocatable :: A(:,:), b(:), x(:)

!!!!! intermediate integration results
    real(DP) :: arg !int1, int2, 

!!!! specified boundary conditions in Laplace space (1/p)    
    complex(DP), dimension(size(connectG,dim=2),2) :: bcLap

!!!!! intermediate results during integration inside domain
    complex(DP) :: delT,delU,delSx,delSy,delRx,delRy

!!!!! basis functions at Gauss quadrature points
    real(DP), dimension(2,MAXINTORD) :: GN

!!!!! 2D vector field point
    real(DP), dimension(2) :: field

!!!!! Bessel and Struve function results
    complex(DP) :: K(0:1), L(-1:0)
    complex(DP) :: pq

    !! related to singular integral sub-division
    complex(DP) :: cutoff
    real(DP) :: newlen, Jac2, zeta

!!!! counters/indicies
    integer(I4B) ::  i, e, n ,m
    integer(I4B) :: nz, ierr, nObsPts, nElement, nNode

    pq = sqrt(p/alpha)
    write(30,*) 'p:',p,'alpha:',alpha,'q:',pq
    nObsPts = size(obsLoc,dim=2)
    nElement = size(connectG,dim=2)
    nNode = size(coordG,dim=2)

    !$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    !$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    ! INTEGRATION OF GREEN'S FUNCTIONS + BASIS FUNCTIONS

    ! matricies which hold numerical integration results
    allocate(H(nNode,nElement,2),G(nNode,nElement,2),&
         & A(nNode,nNode),b(nNode),x(nNode))

    ! initialize
    H = CZERO; G = CZERO
    call GaussSetup(intOrd,GaussCoord,GaussWeight)  ! same intOrd for each element

    ! calculate basis functions for all nodes & gauss pts
    GN(1:2,1:intOrd) = basis(GaussCoord(1:intOrd))

    do i=1,nNode  ! collocation point (source pt)
       do e=1,nElement  ! elements (field points)
          connectL(1:2) = connectG(1:2,e)
          coordL(1:2,1:2) = coordG(1:2,connectL(1:2)) ! (x,y),(node1,node2)

          do n=1,2 ! local numbering of nodes

             ! non-singular integrations *********
             if(i /= connectL(n)) then
                do m=1,intOrd
                   ! calculate location of Gauss points in global coordinates
                   field(1:2) = sum(spread(GN(1:2,m),dim=1,ncopies=2)* &
                        & coordL(1:2,1:2),dim=2)

                   ! potential integral
                   G(i,e,n) = G(i,e,n) + &
                        & GN(n,m)*u(coordG(1:2,i),field(1:2),pq)*Jac(e)*GaussWeight(m)

                   ! normal flux integral (apply -cond term here)
                   H(i,e,n) = H(i,e,n) + GN(n,m)*&
                        & q(coordG(1:2,i),field(1:2),pq,norm(e,1:2))*Jac(e)*GaussWeight(m)
                end do
!!$                write(30,'(2(I3,1X),2(A,2(ES16.8,1X)))') e,n,'G:',G(i,e,n),' H:',H(i,e,n)

                ! singular integrations (diagonals) **********
                ! collocation point coincides with this node
             elseif(i == connectL(n)) then 

                ! determine H(i,i) for singular integral 
                ! (fraction of angle or -sum of off-diagonal terms)

                ! fraction of whole circle this interior angle makes
                ! 180 - theta = interior angle; where theta is angle between normals

                if(norm(e,1) == norm(otherBC(e,n,1),1) .and. &
                 & norm(e,2) == norm(otherBC(e,n,1),2)) then ! a flat stretch
                   H(i,e,n) = HALF*HALF
                else ! some other angle
                   arg = dot_product(norm(e,:),norm(otherBC(e,n,1),:))/&
                        & (Jac(e)*Jac(otherBC(e,n,1)))
                   if (arg > RONE) then  ! reflect about x=1
                      arg = RONE - (arg - RONE)
                   end if
                   H(i,e,n) = (PI - acos(arg))/TWOPI*HALF
                end if

                ! determine G(i,i) for singular integrals
                ! The integral is broken into two parts; the first line is
                ! the integral of K0(x) from 0:l, while the second line
                ! is the integral of x*K0(x) from 0:l.

                ! for large values of p, must split the integration
                ! due to poor convergence of StruveL for large argument
                if(abs(length(e)*pq) < MAXSINGULAR) then

                   call cbesk(length(e)*pq,RZERO,1,2,K(0:1),nz,ierr)
                   if(ierr /= 0) print *, 'error in Bessel function routine',ierr
                   L(-1) = struveL(-1,length(e)*pq)
                   L(0) =  struveL(0,length(e)*pq)
                   
                   G(i,e,n) = (PIOV2*length(e)*(K(0)*L(-1) + K(1)*L(0)) - &
                        & (RONE/(pq*length(e)) - K(1))/pq)/TWOPI
                   
!!$                   write(30,'(A,2(1X,ES23.16))') 'G(analytic-whole-interval):', G(i,e,n)

                else ! split singular integral into two parts

                   ! new complex number with same absolute value as MAXSINGULAR
                   ! and same direction (arg) as length(e)*pq
                   cutoff = MAXSINGULAR*exp(EYE* &
                        & atan2(aimag(length(e)*pq),real(length(e)*pq)))
                   newlen = real(cutoff/pq)

!!$                   write(30,*) 'element too large:', length(e)*pq
!!$                   write(30,*) 'old length:', length(e)
!!$                   write(30,*) 'new length:', newlen
!!$                   write(30,*) 'new r*q:', cutoff
                   
                   call cbesk(cutoff,RZERO,1,2,K(0:1),nz,ierr)
                   if(ierr /= 0) print *, 'error in Bessel function routine',ierr
                   L(-1) = struveL(-1,cutoff)
                   L(0) =  struveL(0,cutoff)
                   
                   G(i,e,n) = (PIOV2*newlen*(K(0)*L(-1) + K(1)*L(0)) - &
                        & (RONE - cutoff*K(1))/(length(e)*pq**2))/TWOPI

!!$                   write(30,'(A,2(1X,ES23.16))') 'G(analytic):', G(i,e,n)

                   ! now compute the rest of the interval using Gauss Quadrature
                   ! when q is big, this part of the integral is likely
                   ! insignificant (within machine precision)  compared to the part 
                   ! calculated using the analytic solution.

                   Jac2 = 0.5_DP*(length(e) - newlen)
!!$                   write(30,*) 'Jac2:', Jac2
                   do m=1,intOrd
                      
                      zeta = Jac2*GaussCoord(m) + 0.5_DP*(length(e) + newlen)

                      if(n == 1) then
                         field(1:2) = (1.0_DP - zeta/length(e))*coordL(:,1) + &
                              & (zeta/length(e))*coordL(:,2)
                      else
                         field(1:2) = (1.0_DP - zeta/length(e))*coordL(:,2) + &
                              & (zeta/length(e))*coordL(:,1)
                      end if
!!$                      write(30,*) 'm:',m,' zeta:',zeta, ' field:',field                       
                      ! potential integral
                      G(i,e,n) = G(i,e,n) + (1.0_DP - zeta/length(e))* &
                           & u(coordG(1:2,i),field(1:2),pq)*Jac2*GaussWeight(m)
                   end do
                end if
                               
!!$                write(30,'(3(A,I2))') 'SNG:',i,' el:',e,' node:',n
!!$                write(30,'(2(A,2(ES23.16,1X)))') 'G:',G(i,e,n), 'H:',H(i,e,n)
             end if
          end do
       end do
    end do

    !$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    !$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    ! ASSEMBLE GLOBAL LHS AND RHS FROM LOCALLY INDEXED MATRICIES AND SPECIFIED BC


    select case(bcflag)
    case(1)
       ! steady-state boundary conditions
       bcLap(:,:) = bc(:,:)/p;
    case(2)
       ! constant boundary condition * cos(4*t)
       bcLap(:,:) = bc(:,:)*p/(p**2 + 16.0_DP)
    case(3)
       !! a pulse from t=0.05 to t=0.5
       bcLap(:,:) = bc(:,:)*exp(-0.025_DP*p)*(1.0_DP - exp(-0.1_DP*p))/p**2
    case(4)
       !! a step on at t = 0.05
       bcLap(:,:) = bc(:,:)*exp(-0.08_DP*p)/p
    case(5)
       !! a step off at t = 0.05
       bcLap(:,:) = bc(:,:)*(1.0_DP - exp(-0.08_DP*p))/p
    case default
       stop 'incorrect bc flag specified'
    end select
    
    A(:,:) = CZERO; b(:) = CZERO   ! lhs, rhs

    ! DelU == G (potential integral)*(flux); begins on RHS
    ! DelT == H (flux integral)*(potential); begins on LHS

    do e=1,nElement
       connectL(1:2) = connectG(1:2,e)
       do n=1,2
          select case(type(e))
          case(2) ! Neumann bc
             ! flux is known (put on RHS)
             ! set rhs: (computed potential integral)*(specified normal flux)
!!$             write(30,'(2(A,2(F11.8,1X)),2(I3))') 'b= +G:',G(1,e,n),' II bc',bcLap(e,n),e,n
             b(1:nNode) = b(1:nNode) + G(1:nNode,e,n)*bcLap(e,n)

             select case(type(otherBC(e,n,1)))
             case(2) ! adjoining element is also a Neumann bc
                ! potential is unknown (put on LHS)
                ! set lhs: computed flux integral
!!$                write(30,'(1(A,2(F11.8,1X)))') 'A= +H:',H(1,e,n)
                A(1:nNode,connectL(n)) = A(1:nNode,connectL(n)) + H(1:nNode,e,n)
             case(1) ! adjoining element is Dirichlet bc
                ! potential is known (put on RHS)
                ! set rhs: (-1)*(computed flux integral)*(specified head)
!!$                write(30,'(2(A,2(F11.8,1X)),2(I3))') 'b= -H:',-H(1,e,n),' I bc:',&
!!$                     & bcLap(otherBC(e,n,1),otherBC(e,n,2)),e,n
                b(1:nNode) = b(1:nNode) - &
                     & H(1:nNode,e,n)*bcLap(otherBC(e,n,1),otherBC(e,n,2))
             end select
          case(1) ! Dirichlet bc
             ! flux is unknown (put on LHS)
             ! set lhs: (-1)*(computed head integral)
!!$             write(30,'(1(A,2(F11.8,1X)))') 'A= -G:',-G(1,e,n)
             A(1:nNode,connectL(n)) = A(1:nNode,connectL(n)) - G(1:nNode,e,n)
             ! head is known (put on RHS)
             ! set rhs: (-1)*(computed flux integral)*(specified head)
!!$             write(30,'(2(A,2(F11.8,1X)),2(I3))') 'b= -H:',-H(1,e,n),' I bc:',bcLap(e,n),e,n
             b(1:nNode) = b(1:nNode) - H(1:nNode,e,n)*bcLap(e,n)
          case default
             print *, 'only type I or II bc currently handled ',bcLap(e,n)
             stop
          end select
       end do
    end do

!!$    write(30,*) 'A, p:',p
!!$    do i=1,2
!!$       write(30,*) 'row',i,A(i,:)
!!$    end do
!!$    
!!$    write(30,*) 'b, p:',p
!!$    write(30,*) b(:)


    !$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    !$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    ! SOLVE MATRIX PROBLEM USING LU DECOMPOSITION

    x = solve(A,b) ! a wrapper around LAPACK routines
    if(maxval(abs(matmul(A,x)-b)) > 1.0D-13) then
       write(30,*) 'LU residual too LARGE',maxval(abs(matmul(A,x)-b))
       write(*,*)  'warning: matrix solution residual too LARGE',maxval(abs(matmul(A,x)-b))
    end if
        
!!$    write(30,*) 'x, p:',p
!!$    write(30,*) x(:)

    !$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    !$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    ! BUILD UP VECTORS OF BOUNDARY HEAD AND N-FLUX FOR POST-PROCESSING
    
    do e=1,nElement
       connectL(1:2) = connectG(1:2,e)
       do n=1,2
          select case(type(e))
          case(2)
             ! Neumann BC - specified flux
             sol_t(e,n) = bcLap(e,n)
!!$             write(30,'(A,I3,I3)',advance='no') 'current type II:',e,n

             select case(type(otherBC(e,n,1)))
             case(2)
                ! adjoining element is also Neumann
                ! therefore head was calculated
                sol_u(e,n) = x(connectL(n))
!!$                write(30,*) ' next type II:',e,n
             case(1)
                ! adjoining element is Dirichlet
                ! therefore head was specified (for other element)
                sol_u(e,n) = bcLap(otherBC(e,n,1),otherBC(e,n,2))
!!$                write(30,*) ' next type I:',e,n
             end select
          case(1)
             ! Dirichlet BC - specified head, computed flux
!!$             write(30,*) 'current type I:',e,n
             sol_u(e,n) = bcLap(e,n)
             sol_t(e,n) = x(connectL(n))
          end select
       end do
    end do

!!$    write(30,*) 'dump of boundary potential values:'
!!$    write(30,*) sol_u
!!$    write(30,*) 'dump of boundary normal flux values:'
!!$    write(30,*) sol_t
    

    !$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    !$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    ! CALCULATE POTENTIAL AND FLUX VECTOR AT POINTS INSIDE DOMAIN (post-processing)

    obsResults(:,:) = CZERO ! obs index; head,x-flux,y-flux

    do i=1,nObsPts
       do e=1,nElement
          connectL(1:2) = connectG(1:2,e)
          coordL(1:2,1:2) = coordG(1:2,connectL(1:2))

          do n=1,2

             ! calculate potential inside the domain
             delT=CZERO; delU=CZERO; 
             delSx=CZERO; delSy=CZERO;
             delRx=CZERO; delRy=CZERO;

             do m=1,intOrd
                field(1:2) = sum(spread(GN(1:2,m),dim=1,ncopies=2)*coordL(1:2,1:2),dim=2)

                ! potential
                delU = delU + GN(n,m)* &
                     & u(obsLoc(:,i),field(:),pq)*Jac(e)*GaussWeight(m)
                delT = delT + GN(n,m)* &
                     & q(obsLoc(:,i),field(:),pq,norm(e,:))*Jac(e)*GaussWeight(m)

                ! x-deriv of above (times -k)
                delSx = delSx + GN(n,m)* &
                     & Du(obsLoc(:,i),field(:),pq,1)*Jac(e)*GaussWeight(m)
                delRx = delRx + GN(n,m)* &
                     & Dq(obsLoc(:,i),field(:),pq,norm(e,:),1)*Jac(e)*GaussWeight(m)

                ! y-deriv of above (times -k)
                delSy = delSy + GN(n,m)* &
                     & Du(obsLoc(:,i),field(:),pq,2)*Jac(e)*GaussWeight(m)
                delRy = delRy + GN(n,m)* &
                     & Dq(obsLoc(:,i),field(:),pq,norm(e,:),2)*Jac(e)*GaussWeight(m)
             end do
             ! potential
             obsResults(i,1) = obsResults(i,1) - delT* sol_u(e,n) + delU* sol_t(e,n)
             ! x-flux
             obsResults(i,2) = obsResults(i,2) - delRx*sol_u(e,n) + delSx*sol_t(e,n)
             ! y-flux
             obsResults(i,3) = obsResults(i,3) - delRy*sol_u(e,n) + delSy*sol_t(e,n)
          end do
       end do
    end do

    obsResults(:,2:3) = -cond*obsResults(:,2:3)

  end subroutine biem_transient_solution


  !$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
  !$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
  ! ABCISSA AND WEIGHTS FOR GAUSS QUADRATURE

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  subroutine GaussSetup(intOrd,coord,weight)
    use constants, only : DP, RONE, RTWO, RZERO

    integer(I4B), intent(in) :: intOrd    ! Gauss integration order
    real(DP), intent(out), dimension(:) :: coord,weight

    ! initialize
    coord(:) = -9.99E9
    weight(:) = RZERO

    ! from Stroud & Secrest "Gaussian Quadrature Formulas", 1966: Table 1
    select case (intOrd)
    case(1)
       coord(1) = RZERO
       weight(1) = RTWO
    case(2)
       coord(1) = 0.57735026918962576450_DP
       coord(2) = -coord(1)
       weight(1:2) = RONE
    case(3)
       coord(1:2) = (/ 0.77459666924148337704_DP, RZERO /)
       coord(3) = -coord(1)
       weight(1:2) = (/ 0.55555555555555555556_DP, 0.88888888888888888889_DP /)
       weight(3) = weight(1)
    case(4)
       coord(1:2) = (/ 0.86113631159405257522_DP, 0.33998104358485626480_DP /)
       coord(3:4) = -coord(2:1:-1)
       weight(1:2) = (/ 0.34785484513745385737_DP, 0.65214515486254614263_DP /)
       weight(3:4) = weight(2:1:-1)
    case(5)
       coord(1:3) = (/ 0.90617984593866399280_DP, 0.53846931010568309104_DP, &
                    &  RZERO /)
       coord(4:5) = -coord(2:1:-1)
       weight(1:3) = (/ 0.23692688505618908751_DP, 0.47862867049936646804_DP, &
                     &  0.56888888888888888889_DP /)
       weight(4:5) = weight(2:1:-1)
    case(6)
       coord(1:3) = (/ 0.93246951420315202781_DP, 0.66120938646626451366_DP, &
                     & 0.23861918608319690863_DP /)
       coord(4:6) = -coord(3:1:-1)
       weight(1:3) = (/ 0.17132449237917034504_DP, 0.36076157304813860757_DP, &
                     &  0.46791393457269104639_DP /)
       weight(4:6) = weight(3:1:-1)
    case(7)
       coord(1:4) = (/ 0.94910791234275852453_DP, 0.74153118559939443986_DP, &
                     & 0.40584515137739716691_DP, RZERO /)
       coord(5:7) = -coord(3:1:-1)
       weight(1:4) = (/ 0.12948496616886969327_DP, 0.27970539148927666790_DP, &
                      & 0.38183005050511894495_DP, 0.41795918367346938776_DP /)
       weight(5:7) = weight(3:1:-1)
    case(8)
       coord(1:4) = (/ 0.96028985649753623168_DP, 0.79666647741362673959_DP, &
                     & 0.52553240991632898582_DP, 0.18343464249564980494_DP /)
       coord(5:8) = -coord(4:1:-1)
       weight(1:4) = (/ 0.10122853629037625915_DP, 0.22238103445337447054_DP, &
                      & 0.31370664587788728734_DP, 0.36268378337836198296_DP /)
       weight(5:8) = weight(4:1:-1)
    case(9)
       coord(1:5) = (/ 0.96816023950762608984_DP, 0.83603110732663579430_DP, &
                     & 0.61337143270059039730_DP, 0.32425342340380892904_DP, &
                     & RZERO /)
       coord(6:9) = -coord(4:1:-1)
       weight(1:5) = (/ 0.081274388361574411972_DP, 0.18064816069485740406_DP, &
                      & 0.26061069640293546231_DP,  0.31234707704000284007_DP, &
                      & 0.33023935500125976316_DP /)          
       weight(6:9) = weight(4:1:-1)
    case(10)
       coord(1:5) = (/ 0.97390652851717172008_DP, 0.86506336668898451073_DP, &
                     & 0.67940956829902440623_DP, 0.43339539412924719080_DP, &
                     & 0.14887433898163121088_DP /)
       coord(6:10) = -coord(5:1:-1)
       weight(1:5) = (/ 0.066671344308688137594_DP, 0.14945134915058059315_DP, &
                      & 0.21908636251598204400_DP,  0.26926671930999635509_DP, &
                      & 0.29552422471475287017_DP /)
       weight(6:10) = weight(5:1:-1)
    case default
       print *, 'unsupported Gaussian Quadrature integration order:', intOrd
       stop
    end select

  end subroutine GaussSetup


  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  function basis(xsi) result(N)
    use constants, only : RONE, HALF
    real(DP), intent(in), dimension(:) :: xsi   ! Gauss pts
    real(DP), dimension(2,size(xsi)) :: N   ! all the basis fcns at all the pts

    integer(I4B) :: nPts

    ! first dimension of basis is the basis function
    ! second dimension of basis is the gauss pt location

    nPts = size(xsi)
    if(maxval(abs(xsi)) > RONE) print *, 'local coordinate out of range:',xsi

    ! linear elements
    N(1,1:nPts) = HALF*(RONE - xsi(1:nPts)) ! node 1 at xsi=-1
    N(2,1:nPts) = HALF*(RONE + xsi(1:nPts)) ! node 2 at xsi=+1

  end function basis

  !$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
  !$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
  ! GREEN'S FUNCTIONS

  ! basic 2D Green's function
  !##################################################
  function u(x1,x2,arg) result(pot)
    use constants, only : DP, RONE, TWOPI
    use complex_bessel, only : cbesk

    real(DP), intent(in), dimension(2) :: x1,x2 ! source pt (P), field pt (PQ)
    complex(DP), intent(in) :: arg  ! argument multiplied by radius
!!$    real(DP), intent(in) :: alpha
    complex(DP) :: pot    ! potential at field point

    real(DP), dimension(2) :: r  ! vector pointing from source to field point
    complex(DP), dimension(1) :: K0
    integer(I4B) :: nz, ierr 

    r(1:2) = x2(1:2) - x1(1:2)
    call cbesk(arg*sqrt(dot_product(r,r)), 0.0_DP, 1, 1, K0(1), nz, ierr)
    if(ierr /= 0) print *, 'error in bessel function routine: ',ierr
!!$    if(nz /= 0) print *, 'underflow in bessel function routine: ',nz

    ! Green's function for 2D modified helmholtz solution
    pot = K0(1)/TWOPI

  end function u

  ! x or y deriv of greens fcn (here x/y deriv is wrt source pt)
  !##################################################
  function Du(x1,x2,arg,xy) result(dpot)
    use constants, only : DP, RONE, TWOPI
    use complex_bessel, only : cbesk

    real(DP), intent(in), dimension(2) :: x1,x2 ! source pt (P), field pt (Q)
    complex(DP), intent(in) :: arg  ! argument multiplied by radius  
    integer(I4B), intent(in) :: xy !=1 for x, =2 for y
!!$    real(DP), intent(in) :: alpha
    complex(DP) :: dpot  ! potential at each point

    real(DP), dimension(2) :: r ! vector pointing from source to field point
    real(DP) :: dist ! length of r vector
    complex(DP), dimension(1) :: K1
    integer(I4B) :: nz, ierr

    if(xy /= 1 .and. xy /= 2) stop 'Du: incorrect derivative direction'

    r(1:2) = x2(1:2) - x1(1:2)
    dist = sqrt(dot_product(r,r))
    call cbesk(arg*dist, RONE, 1, 1, K1(1), nz, ierr)
    if(ierr /= 0) print *, 'error in bessel function routine: ',ierr
!!$    if(nz /= 0) print *, 'underflow in bessel function routine: ',nz

    ! deriv of Green's function in x or y direction
    ! negative from derivative of r cancels negative from derivative of K0()
    dpot = arg*r(xy)*K1(1)/(TWOPI*dist)

  end function Du

  ! directional derivative of Green's function (here x/y deriv is wrt field point)
  !##################################################
  function q(x1,x2,arg,nn) result(flux)
    use complex_bessel, only : cbesk
    use constants, only : TWOPI, RONE

    real(DP), intent(in), dimension(2) :: x1,x2,nn  ! source pt, field pt, unit normal
    complex(DP), intent(in) :: arg
!!$    real(DP), intent(in) :: alpha
    complex(DP) :: flux  ! deriviative of potential

    real(DP), dimension(2) :: r ! vector pointing from source to field point
    complex(DP), dimension(1) :: K1
    real(DP) :: dist
    integer(I4B) :: nz, ierr

    r(1:2) = x2(1:2) - x1(1:2)
    dist = sqrt(dot_product(r,r))
    call cbesk(arg*dist, RONE, 1, 1, K1(1), nz, ierr)
    if(ierr /= 0) print *, 'error in bessel function routine: ',ierr
!!$    if(nz /= 0) print *, 'underflow in bessel function routine: ',nz

    ! deriv of Green's function in direction of nn
    ! negative comes from derivative of K0()
    flux = -arg*dot_product(nn,r)*K1(1)/(TWOPI*dist) 
    
    ! sign must be opposite what I think it should be for the solution to work out.

  end function q

  ! X & Y deriv of normal derivative of Green's function (here x/y is source point)
  !##################################################
  function Dq(x1,x2,arg,nn,xy) result(Df)
    use complex_bessel, only : cbesk
    use constants, only : TWOPI

    real(DP), intent(in), dimension(2) :: x1,x2,nn  ! source pt, field pt, normal
    complex(DP), intent(in) :: arg
    integer(I4B), intent(in) :: xy ! =1 for x, =2 for y
!!$    real(DP), intent(in) :: alpha
    complex(DP) :: Df

    real(DP), dimension(2) :: r ! vector pointing from source to field point
    complex(DP), dimension(0:1) :: K
    real(DP) :: dist
    integer(I4B) :: nz, ierr

    if(xy /= 1 .and. xy /= 2) stop 'Dq: incorrect derivative direction'
    
    r(1:2) = x2(1:2) - x1(1:2)
    dist = sqrt(dot_product(r,r))
    call cbesk(arg*dist, 0.0_DP, 1, 2, K(:), nz, ierr)
    if(ierr /= 0) print *, 'error in bessel function routine: ',ierr
!!$    if(nz /= 0) print *, 'underflow in bessel function routine: ',nz

    ! derivative of normal flux in x or y direction
    Df = -(dot_product(nn,r)* (arg**2*r(xy)/(dist**2)* (K(1)/(arg*dist) + K(0)) + &
         & r(xy)*arg/(dist**3)*K(1)) - nn(xy)*arg*K(1)/dist)/TWOPI

  end function Dq

  !##################################################
  !##################################################
  function struveL(ord,arg) result(L)
    use constants, only : PI, CZERO, CONE

    integer(I4B), intent(in) :: ord
    complex(DP), intent(in) :: arg
    complex(DP) :: L, term
    integer, parameter :: MAXITER = 88
!!$    complex(DP), dimension(-1:MAX,0:MAX) :: ep
!!$    complex(DP) :: denom
    integer :: i
    logical :: small

    ! uses power series expansion from Abramowitz & Stegun (12.2.1)
    ! convergence test: if two terms in a row are smaller than epsilon

    !!!! should add a test for the size of the argument, and
    !!!! use an asymptotic expansion for large argument (= small time)

    small = .false.
    L = 0.0_DP

    ! sum terms
    loop: do i=0,MAXITER
!!$       term = (0.5_DP*arg)**(2*i)/(exp(lngamma(real(i,DP) + 1.5_DP))&
!!$            & *exp(lngamma(real(i+ord,DP) + 1.5_DP)))

       term = (0.5_DP*arg)**(2*i)/(gamma(real(i,DP) + 1.5_DP)&
            & *gamma(real(i+ord,DP) + 1.5_DP))

       ! loop until additional terms are < full precision
       if(abs(term) < epsilon(1.0_DP)) then
          if(small) then
             L = L + term
             exit loop
          else
             small = .true.
          end if
       else
          small = .false.
       end if
       L = L + term
       if(i==MAXITER) then
          write(30,*) 'STRUVEL: max iterations used:',abs(term), ' arg:',arg
          write(*,*) 'argument probably too large for Struve L power series:', arg
       end if
    end do loop

    ! apply factor to summation
    L = L*(0.5_DP*arg)**(ord+1)
  end function struveL


!!$    ! uses accelerated power series expansion from Abramowitz & Stegun (12.2.1)
!!$
!!$    ep(:,:) = CZERO  ! initialize epsilon table
!!$
!!$       ! sum terms
!!$       do i=0,MAX
!!$          term = ((0.5_DP*arg)**(2*i))/(gamma(real(i,DP) + 1.5_DP)* &
!!$               & gamma(real(i+ord,DP) + 1.5_DP))
!!$
!!$          ! add term to partial sums
!!$          ep(0,i:MAX) = term + ep(0,i:MAX)
!!$          
!!$       end do
!!$
!!$       ! build up epsilon table
!!$       do m=0,MAX-1 ! subscript
!!$          do j=0,MAX-1  ! superscript
!!$             denom = ep(m,j+1) - ep(m,j)
!!$             if(abs(denom) > 0.0_DP) then ! check for div by zero
!!$                ep(m+1,j) = ep(m-1,j+1) + CONE/denom
!!$             else
!!$                L = ep(m,j+1)
!!$                goto 777
!!$             end if
!!$             
!!$          end do
!!$       end do
!!$
!!$       ! if made all the way through table use corner value as answer
!!$       L = ep(MAX,0)
!!$
!!$777    continue
!!$
!!$       ! apply factor to summation
!!$       L = L*(0.5_DP*arg)**(ord+1)
!!$
!!$  end function struveL

  !##################################################
  !##################################################
  ! using double-precision complex routines from LAPACK 3.3
  function solve(A,b) result(x)

    ! LAPACK LU factorization
    interface 
       subroutine ZGETRF(M,N,A,LDA,IPIV,INFO)
         integer, intent(in) :: LDA, M, N
         complex(kind=8), intent(inout) :: A(LDA,*)
         integer, intent(inout) :: IPIV(*)
         integer, intent(inout) :: INFO
       end subroutine ZGETRF
    end interface

    ! LAPACK solution to A*x=b using factorization from ZGETRF
    interface
       subroutine ZGETRS(TRANS,N,NRHS,A,LDA,IPIV,B,LDB,INFO)
         character(1), intent(in) :: TRANS
         integer, intent(out) :: INFO
         integer, intent(in) :: LDA, LDB, N, NRHS
         integer, intent(in) :: IPIV(*)
         complex(kind=8), intent(in) :: A(LDA,*) 
         complex(kind=8), intent(inout) :: B(LDB,*)
       end subroutine ZGETRS
    end interface

    integer :: n, ierr
!!$    integer, parameter :: LWORK = 6400
    complex(DP), dimension(:,:), intent(in) :: A
    complex(DP), dimension(size(a,1),size(a,2)) :: Awrk
    complex(DP), dimension(:), intent(in) :: b
    complex(DP), dimension(size(b,1),1) :: bwrk
    complex(DP), dimension(size(A,dim=2)) :: x
    integer, dimension(size(A,1)) :: indx
!!$    complex(DP), dimension(LWORK) :: work

    indx(:) = 0
    n = size(A,1)
    Awrk(:,:) = A(:,:)

    call ZGETRF(n,n,Awrk,n,indx,ierr)
    if (ierr /= 0) write (*,*) 'error returned from ZGETRF ',ierr

    bwrk(:,1) = b(:)

    call ZGETRS('N',n,1,Awrk,n,indx,bwrk,n,ierr)
    if (ierr /= 0) write (*,*) 'error returned from ZGETRS ',ierr

    x(:) = bwrk(:,1)

  end function solve

end module biem_transient
