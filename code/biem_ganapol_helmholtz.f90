
! $Id: biem_ganapol_helmholtz.f90,v 1.1 2006/11/11 17:12:54 kris Exp kris $
program helmholtz_biem_main
  use constants, only : QR, DP, I4B, HALF, RTWO, RONE, EYE, CONE, RZERO, SMALL, PI, LN2, UNITI, UNITJ
  use biem_shared_data
  use biem_transient, only : biem_transient_solution
  use biem_steady, only : biem_steady_solution

  implicit none

  !$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
  !$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
  ! DECLARE VARIABLES

  ! !!!! element coordinates, connectivity and BC (from input file)
  real(DP), allocatable :: coordG(:,:), bc(:,:)
  integer(I4B), allocatable :: connectG(:,:), type(:)

  !! $ coordinates and connectivity for current element
  real(DP) :: coordL(2,2) ! always 2 dim x 2 nodes per element
  integer(I4B) :: connectL(2)  ! always 2 nodes per element

  !! !!! solution matricies in time domain
  real(DP), allocatable :: steady_u(:,:), steady_norm(:,:), steady_tan(:), steady_obsresults(:,:)
  real(DP), allocatable :: tsol_u(:,:), tsol_norm(:,:), tsol_tan(:), tobsResults(:,:)
  real(DP), allocatable :: obsLoc(:,:)

  !! Complex Laplace paramater and associated
  complex(DP), allocatable :: p(:)
  integer(I4B) :: invLapMethod, np, numt

  !! thermal/hydraulic diffusivity, conductivity for domain; time
  real(DP) :: alpha, cond
  real(DP), allocatable :: t(:)

  !! # elements, # nodes, integration order, # calculation pts inside domain
  integer(I4B) :: nElement, maxNode, intOrd, nObsPts
  integer(I4B) :: i,j,e,n,k
  real(DP), dimension(2) :: tangent
  real(DP) :: jac2

  character(2) :: nt, method

  !$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
  !$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
  ! READ INPUT PARAMETERS INTO MODULE VARS + ECHO TO OUTPUT

  open(unit=20, file='input.dat', status='old', action='read')
  open(unit=30, file='output.dat', status='replace', action='write')

  read(20,*) nElement, intOrd, nObsPts, alpha, cond, numt, invLapMethod
  allocate(t(numt))
  read(20,*) t(1:numt)
  write(30,'(A,I3)') 'number of elements= ', nElement
  write(30,'(A,I2)') 'integration order= ', intOrd
  write(30,'(A,I3)') 'number of calcualtion points= ', nObsPts
  write(30,'(A,ES14.7,1X,ES14.7)') 'hydraulic diffusivity/conductivity = ', alpha, cond
  write(30,'(A,I2)') 'number of solution times = ', numt
  write(30,*) 't = ', t

  write(30,'(A)', advance='no') 'inverse Laplace transform => '
  select case(invLapMethod)
  case(1)
     write(30,'(A)') 'de Hoog, et. al., Fourier series method'
     write(*,*) 'de Hoog, et. al., Fourier series method'
     method = 'FS'
     open(unit=77,file='fs-input.dat',status='old', action='read')
     read(77,*) dHm, dHalpha, dHtol; close(77)
  case(2)
     write(30,'(A)') 'fixed Talbot method'
     write(*,*) 'fixed Talbot method'
     method = 'TB'
     open(unit=77,file='tb-input.dat',status='old', action='read')
     read(77,*) talM; close(77)
  case(3)
     write(30,'(A)') 'Ganapol Romberg integration method'
     write(*,*) 'Ganapol Romberg integration method'
     method = 'GP'
  case(4)
     write(30,'(A)') 'Gaver-Stehfest method'
     write(*,*) 'Gaver-Stehfest method'
     method = 'GS'
     open(unit=77,file='gs-input.dat',status='old', action='read')
     read(77,*) gstN; close(77)
  case(5)
     write(30,'(A)') 'Schapery method'
     write(*,*) 'Schapery method'
     method = 'SH'
     open(unit=77,file='sh-input.dat',status='old', action='read')
     read(77,*) schpN, schpBase, schpFact; close(77)

     ! for checking steady solution
     open(unit=33, file='output-steady.dat', status='replace', action='write') 
  case(6)
     write(30,'(A)') 'Weeks method'
     write (*,*) 'Weeks method'
     method = 'WK'
     open(unit=77,file='wk-input.dat',status='old', action='read')
     read(77,*) weekN, weeksig0, weekEpsMax, weekEpsMin ; close(77)

  case default
     write(30,'(A,I2)') 'invalid inverse Laplace transform method chosen ', invLapMethod
     stop
  end select

  allocate(connectG(2,nElement),type(nElement),bc(nElement,2))

  ! read in / echo parameters associated with _elements_
  do i=1,nElement
     read(20,*) connectG(1:2,i), type(i), bc(i,1:2)
     write(30,100) 'elem: ',i,' nodes: ',connectG(1:2,i),' bc type: ',type(i),' bc_vals: ',bc(i,1:2)
  end do
100 format(A,I3,A,I3,1X,I3,A,I1,A,2(ES14.7,1X))

  maxNode = maxval(connectG(:,:))
  allocate(coordG(2,maxNode), obsLoc(2,nObsPts))

  ! read in / echo parameters associated with _nodes_
  do i=1,maxNode
     ! loop over nodes, reading in global coordinates (x,y)
     ! type = 1 : Dirichlet, type = 2 : Neumann
     read(20,*) coordG(1:2,i)
     write(30,101) 'node:',i,' (x,y):',coordG(:,i)
  end do

  ! read in / echo locations of calculation points inside the domain
  do i=1,nObsPts
     read(20,*) obsLoc(:,i)
     write(30,101) 'calc_pt:',i,' (x,y):',obsLoc(:,i)
  end do
101 format(A,I3,A,2(1X,F7.3))

  !! $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
  !! $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
  !! COMPUTE CONSTANT PROPERTIES OF BOUNDARY ELEMENTS

  ! allocate module variables
  allocate(otherBC(nElement,2,2),Jac(nElement),length(nElement), norm(nElement,2))

  ! calculate and save outward normal vector and Jacobian
  do e=1,nElement
     call normalJacobian(norm(e,1:2),Jac(e),coordG(1:2,connectG(1:2,e)))
     length(e) = RTWO*Jac(e)
  end do

  ! determine the element and node which shares the same global 
  ! node number with the current one
  do e=1,nElement
     do n=1,2
        otherBC(e,n,1:2) = nextBC(e,n,connectG(1:2,:),coordG(1:2,:))
     end do
  end do


     !! $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
     !! $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
     ! COMPUTE SOLUTION IN LAPLACE SPACE

     do i=1,np
        write(30,105) i,' solution for p= (',real(p(i)),',',aimag(p(i)),')' 
        write(*,105) i,' BIEM solution for p= (',real(p(i)),',',aimag(p(i)),')'
        call biem_transient_solution(coordG,connectG,intOrd,bc,type,sol_u(:,:,i),&
             & sol_norm(:,:,i),p(i),alpha,cond,obsResults(:,:,i),obsLoc(:,:))
     end do
105  format (I3,A,2(ES10.3,A))

     !! $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
     !! $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
     ! CALCULATE TANGENTIAL FLUX ALONG BOUNDARY ELEMENTS IN LAPLACE SPACE
     ! IT IS A CONSTANT OVER THE LINEAR ELEMENT

     do e=1,nElement
        connectL(1:2) = connectG(1:2,e)
        coordL(1:2,1:2) = coordG(1:2,connectL(1:2))
        tangent(1:2) = HALF*(coordL(:,2) - coordL(:,1))
        jac2 = RONE/sqrt(dot_product(tangent(:),tangent(:)))
        sol_tan(e,1:np) = -cond*jac2*HALF*(sol_u(e,2,1:np) - sol_u(e,1,1:np))
     end do

     ! calculate boundary tangential flux for steady-state problem
     if(invLapMethod == 5) then
        do e=1,nElement
           connectL(1:2) = connectG(1:2,e)
           coordL(1:2,1:2) = coordG(1:2,connectL(1:2))
           tangent(1:2) = HALF*(coordL(:,2) - coordL(:,1))
           jac2 = RONE/sqrt(dot_product(tangent(:),tangent(:)))
           steady_tan(e) = -cond*jac2*HALF*(steady_u(e,2) - steady_u(e,1))
        end do
     end if


     !! $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
     !! $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
     ! PERFORM NUMERICAL INVERSE LAPLACE TRANSFORM

     if(.not. allocated(tsol_u)) allocate(tsol_u(nElement,2),tsol_norm(nElement,2), &
          & tsol_tan(nElement),tobsResults(nObsPts,3))

     select case(invLapMethod)
     case(1) ! de Hoog

        do e=1,nElement
           do n=1,2
              bdryElem = e ! set module variables
              bdryNode = n
              ! check for identically zero solutions (deHoog gives NaN in response)
              if(any(abs(sol_u(e,n,1:np)) > SMALL)) then
                 tsol_u(e,n) = deHoog_invlap(bdry_pot,t(k),dHalpha,dHtol,dHM,.false.)
              else
                 tsol_u(e,n) = RZERO
              end if
              if(any(abs(sol_norm(e,n,1:np)) > SMALL)) then
                 tsol_norm(e,n) = deHoog_invlap(bdry_nflux,t(k),dHalpha,dHtol,dHM,.false.)
              else
                 tsol_norm(e,n) = RZERO
              end if
           end do
           ! tangential flux is constant along linear elements
           if(any(abs(sol_tan(e,1:np)) > SMALL)) then
              tsol_tan(e) =  deHoog_invlap(bdry_tflux,t(k),dHalpha,dHtol,dHM,.false.)
           else
              tsol_tan(e) = RZERO
           end if
        end do
        do i=1,nObsPts   
           obsPt = i
           if(any(abs(obsResults(i,1,1:np)) > SMALL)) then
              tobsResults(i,1) = deHoog_invlap(domain_pot,  t(k),dHalpha,dHtol,dHM,.false.)
           else
              tobsResults(i,1) = RZERO
           end if
           if(any(abs(obsResults(i,2,1:np)) > SMALL)) then      
              tobsResults(i,2) = deHoog_invlap(domain_xflux,t(k),dHalpha,dHtol,dHM,.false.)
           else
              tobsResults(i,2) = RZERO
           end if
           if(any(abs(obsResults(i,3,1:np)) > SMALL)) then
              tobsResults(i,3) = deHoog_invlap(domain_yflux,t(k),dHalpha,dHtol,dHM,.false.)
           else
              tobsResults(i,3) = RZERO
           end if
        end do

     case(2) ! Talbot

        do e=1,nElement
           do n=1,2
              tsol_u(e,n) =    talbot_invlap(p(1:talM),   sol_u(e,n,1:np),taltheta(1:talM-1),t(k))
              tsol_norm(e,n) = talbot_invlap(p(1:talM),sol_norm(e,n,1:np),taltheta(1:talM-1),t(k))
           end do
           tsol_tan(e) =  talbot_invlap(p(1:np),sol_tan(e,1:np),taltheta(1:talM-1),t(k))
        end do
        do i=1,nObsPts
           do e=1,3 ! head, x-flux, y-flux
              tobsResults(i,e) = talbot_invlap(p(1:np),obsResults(i,e,1:np),taltheta(1:talM-1),t(k))
           end do
        end do

     case(3) ! Ganapol

     case(4) ! Stehfest (vectorized wrt largest dimension)
        ! only use the real portion of the solution in Laplace space
        ! assuming that a real Laplace parameter -> real solution

        do n=1,2 ! nodes of elements
           tsol_u(1:nElement,n) = LN2/t(k)*sum(spread(gstV(1:gstN),dim=1,ncopies=nElement)* &
                & real(sol_u(1:nElement,n,1:np)),dim=2)
           tsol_norm(1:nElement,n) = LN2/t(k)*sum(spread(gstV(1:gstN),dim=1,ncopies=nElement)* &
                & real(sol_norm(1:nElement,n,1:np)),dim=2)
        end do
        tsol_tan(1:nElement) = LN2/t(k)*sum(spread(gstV(1:gstN),dim=1,ncopies=nElement)* &
             & real(sol_tan(1:nElement,1:np)),dim=2)

        do i=1,3 ! head, x-flux, y-flux
           tobsResults(1:nObsPts,i) = LN2/t(k)*sum(spread(gstV(1:gstN),dim=1,ncopies=nObsPts)* &
                & real(obsResults(1:nObsPts,i,1:np)),dim=2)
        end do

     case(5) ! Scapery

        do e=1,nElement
           do n=1,2
              schpA(1:np) = matmul(schpP(:,:),sol_u(e,n,:)-steady_u(e,n)/p(:))
              tsol_u(e,n) = steady_u(e,n) + real(sum(schpA(1:np)*exp(-p(1:np)*t(k))))

              schpA(1:np) = matmul(schpP(:,:),sol_norm(e,n,:)-steady_norm(e,n)/p(:))
              tsol_norm(e,n) = steady_norm(e,n) + real(sum(schpA(1:np)*exp(-p(1:np)*t(k))))
           end do

           schpA(1:np) = matmul(schpP(:,:),sol_tan(e,:)-steady_tan(e)/p(:))
           tsol_tan(e) = steady_tan(e) + real(sum(schpA(1:np)*exp(-p(1:np)*t(k))))
        end do

        do i=1,nObsPts
           do j=1,3
              schpA(1:np) = matmul(schpP(:,:),obsResults(i,j,:)-steady_obsResults(i,j)/p(:))
              tobsResults(i,j) = steady_obsResults(i,j) + real(sum(schpA(1:np)*exp(-p(1:np)*t(k))))
           end do
        end do

     case(6)  ! Weeks

        do e=1,nElement
           do n=1,2
              tsol_u(e,n) = week_invlap(weekN,sol_u(e,n,1:np),&
                   & weeksig,weekb,t(k),weekth(:),weekth2(:))
              tsol_norm(e,n) = week_invlap(weekN,sol_norm(e,n,1:np),&
                   & weeksig,weekb,t(k),weekth(:),weekth2(:))
           end do
           tsol_tan(e) = week_invlap(weekN,sol_tan(e,1:np),&
                & weeksig,weekb,t(k),weekth(:),weekth2(:))
        end do

        do i=1,nObsPts
           do e=1,3 ! head, x-flux, y-flux
              tobsResults(i,e) = week_invlap(weekN,obsResults(i,e,1:np),&
                   & weeksig,weekb,t(k),weekth(:),weekth2(:))
           end do
        end do

     end select

     deallocate(sol_u, sol_norm, sol_tan, obsResults)

     !! $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
     !! $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
     ! WRITE BOUNDARY RESULTS TO OUTPUT FILE

     write(30,'(A)') 'bdry potential & normal flux &
          &(element, node, (x,y), head, normal flux, tangent flux)'
     do e=1,nElement
        connectL(1:2) = connectG(1:2,e)
        coordL(1:2,1:2) = coordG(1:2,connectL(1:2))
        do n=1,2
           write(30,103) e,n,coordL(1:2,n),tsol_u(e,n),tsol_norm(e,n),tsol_tan(e)
        end do
     end do

103  format(2(1X,I3),5(1X,F9.4))

     ! output file name has method name code and integer indicating which time
     open(unit=40,file='output-plot-'//method//'-'//nt//'.dat',status='replace',action='write')

     ! write potential, x-flux and y-flux at boundary nodes
     do e=1,nElement
        connectL(1:2) = connectG(1:2,e)
        coordL(1:2,1:2) = coordG(1:2,connectL(1:2))
        tangent(1:2) = (/ -norm(e,2), norm(e,1)  /)
        do n=1,1 ! only output at one node
           write(40,104) coordL(1:2,n),tsol_u(e,n), &
                & dot_product(UNITI,-norm(e,:) )*tsol_norm(e,n) + &
                & dot_product(UNITI,tangent(:))* tsol_tan(e), &
                & dot_product(UNITJ,-norm(e,:) )*tsol_norm(e,n) + &
                & dot_product(UNITJ,tangent(:))* tsol_tan(e) 
        end do
     end do

     ! for schapery method, write steady solution to separte output file for checking
     if (invLapMethod == 5) then
        do e=1,nElement
           connectL(1:2) = connectG(1:2,e)
           coordL(1:2,1:2) = coordG(1:2,connectL(1:2))
           tangent(1:2) = (/ -norm(e,2), norm(e,1)  /)
           do n=1,2
              write(33,104) coordL(1:2,n),steady_u(e,n), &
                   & dot_product(UNITI,-norm(e,:) )*steady_norm(e,n) + &
                   & dot_product(UNITI,tangent(:))* steady_tan(e), &
                   & dot_product(UNITJ,-norm(e,:) )*steady_norm(e,n) + &
                   & dot_product(UNITJ,tangent(:))* steady_tan(e) 
           end do
        end do
     end if


     !! $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

     !! write interior results to output file
     write(30,*) 'results inside the domain: (x,y), head, x-flux, y-flux'
     write(40,*) '# LT-BIEM solution for time=',t(k)
     do i=1,nObsPts
        write(30,104) obsLoc(1:2,i),tobsResults(i,1:3)
        write(40,104) obsLoc(1:2,i),tobsResults(i,1:3)
        if(invLapMethod == 5) write(33,104) obsLoc(1:2,i),steady_obsResults(i,1:3)
     end do



104  format (5(1X,ES15.7))

  end do  ! end of loop over times
  close(20); close(30); close(40); if(invLapMethod == 5) close(33)
  deallocate(Jac,length,norm,otherBC)
  !!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
  !!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
  ! end of program flow

contains

  ! determine which element shares current node (2d, 2 node elements only)
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  function nextBC(ee,nn,connect,coord) result(otherBC)
    integer(I4B), intent(in) :: ee,nn ! element and node 
    integer(I4B), dimension(2) :: otherBC
    integer(I4B), intent(in), dimension(:,:) :: connect ! connectivity matrix for element
    real(DP), intent(in), dimension(:,:) :: coord       ! coordinates of nodes

    integer(I4B) :: ii,jj, numE
    numE = size(connect,dim=2)

    findBC: do ii=1,numE
       if (ii/=ee) then ! loop over _other_ elements
          do jj=1,2 ! loop over their nodes
             ! find other node with same global index
             if (connect(jj,ii) == connect(nn,ee)) then 
                otherBC(1:2) = (/ ii,jj /)
                exit findBC ! done, only one node to find
             end if
          end do
       end if
    end do findBC

  end function nextBC

  ! calculate normal vector and Jacobian for linear (2-node) elements
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  subroutine normalJacobian(norm,jac,coord)
    real(DP), dimension(2), intent(out) :: norm  ! normal to element
    real(DP), intent(out) :: jac 
    real(DP), intent(in) :: coord(:,:)   ! global coords of nodes in element

    real(DP), dimension(2) :: tangent 

    ! sum of deriv of basis * location vector across all nodes
    tangent(1:2) = sum(spread((/ -HALF, HALF /),dim=1,ncopies=2)*coord(1:2,1:2),dim=2)

    norm(1) = +tangent(2)
    norm(2) = -tangent(1)

    ! jacobian is length of normal vector
    jac = sqrt(dot_product(norm(:),norm(:)))

    ! normalize normal vector to unit length
    if (jac > 0.0) then
       norm(1:2) = norm(1:2)/jac
    end if

  end subroutine normalJacobian

  ! fixed Talbot inversion algorithm (Abate & Valko, 2004)
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  function talbot_InvLap(p,fp,theta,t) result(ft)
    use constants, only : DP, HALF, RONE, RZERO
    ! laplace parameter & function evaluated at p
    !! talbot parameter (r) is the real part of the first entry of p
    !! f(r) is the first entry of fp
    complex(DP), intent(in), dimension(:) :: p, fp 
    ! Bromwich contour is parameterized wrt theta (0 -> pi)
    real(DP), intent(in), dimension(:) :: theta
    real(DP), intent(in) :: t
    real(DP) :: ft     ! solution

    integer(I4B) :: M
    real(DP) :: r, fr
    complex(DP), dimension(1:size(p)-1) :: sigmai  ! intermediate result

    M = size(p)
    r = real(p(1))
    fr = real(fp(1))

    sigmai(1:M-1) = (theta(1:M-1) + (theta(1:M-1)/tan(theta(1:M-1)) - RONE) &
         & /tan(theta(1:M-1)))*cmplx(RZERO, RONE, DP)
    ft = r/real(M,DP)*real(HALF*fr*exp(r*t) + &
         & sum(exp(t*p(2:M))*fp(2:M)*(RONE + sigmai(1:M-1))))

  end function talbot_InvLap

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  function week_invLap(N,fp,sig,b,t,th,th2) result(ft)
    real(DP), intent(in) :: b,t,sig
    integer(I4B), intent(in) :: N
    complex(DP), dimension(1:), intent(in) :: fp
    real(DP), dimension(0:), intent(in) :: th, th2
    real(DP) :: ft
    real(DP) :: x

    integer :: k
    complex(DP), dimension(1:N+1) :: y

    x = 2.0_DP*b*t

    ! use Clenshaw recurrence for solution
    do k = N-1,1,-1 ! descending recurrence
       y(k) = weekalpha(k,x)*y(k+1) + weekbeta(k+1)*y(k+2) + &
            & weekcoeff(k,N,th(0:N-1),th2(0:N-1),fp(:),b)
    end do
    ft = exp((sig-b)*t)*real(weekbeta(1)*y(2) + (1.0_DP - x)*y(1) + &
         & weekcoeff(0,N,th(0:N-1),th2(0:N-1),fp(:),b))

  end function week_invLap


  !! alpha and beta are the coefficients in the Laguerre polynomial
  !! recurrence relations, which is used by the clenshaw algorithm
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  real(DP) function weekalpha(nn,xx)
    integer, intent(in) :: nn
    real(DP), intent(in) :: xx

    weekalpha = (real(2*nn+1)-xx)/real(nn+1,DP)
  end function weekalpha

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  real(DP) function weekbeta(nn)
    integer, intent(in) :: nn

    weekbeta = real(-nn,DP)/real(nn+1,DP)
  end function weekbeta

  !! use midpoint rule for calculating a_n coefficients
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  function weekcoeff(kk,NN,th,th2,fp,b) result(a)
    integer, intent(in) :: kk  ! order (n)
    integer, intent(in) :: NN  ! M in Kano implementation
    real(DP), dimension(0:NN-1), intent(in) :: th,th2  ! theta_m and theta_{m+1/2}
    complex(DP), intent(in), dimension(0:NN-1) :: fp ! image function values
    real(DP), intent(in) :: b  ! Weeks parameter
    complex(DP) :: a

    a = exp(-EYE*kk*PI/real(2*NN,DP))/real(NN,DP)*&
         & sum(exp(-EYE*kk*th(0:NN-1))*2.0_DP*b*fp(0:NN-1)/(1.0_DP-exp(EYE*th2(0:NN-1))))

  end function weekcoeff


  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  ! using double-precision complex routines from LAPACK 3.3
  function invert_matrix(ai) result(inv)

    ! interfaces to LAPACK generated by ifort -gen-interfaces
    ! LAPACK LU decomposition 
    INTERFACE 
       SUBROUTINE ZGETRF(M,N,A,LDA,IPIV,INFO)
         INTEGER, intent(in) :: LDA, M, N
         COMPLEX(KIND=8), intent(inout) :: A(LDA,*)
         INTEGER, intent(inout) :: IPIV(*)
         INTEGER, intent(inout) :: INFO
       END SUBROUTINE ZGETRF
    END INTERFACE

    ! LAPACK inverse calculation from results of LU
    INTERFACE 
       SUBROUTINE ZGETRI(N,A,LDA,IPIV,WORK,LWORK,INFO)
         INTEGER, intent(in) :: LDA, N
         COMPLEX(KIND=8), intent(inout) :: A(LDA,*)
         INTEGER, intent(inout) :: IPIV(*)
         COMPLEX(KIND=8), intent(inout) :: WORK(*)
         INTEGER, intent(in) :: LWORK
         INTEGER, intent(inout) :: INFO
       END SUBROUTINE ZGETRI
    END INTERFACE

    integer :: n, ierr
    integer, parameter :: LWORK = 6400
    complex(DP), dimension(:,:), intent(in) :: ai
    complex(DP), dimension(size(ai,1),size(ai,1)) :: inv
    integer, dimension(size(ai,1)) :: indx
    complex(DP), dimension(LWORK) :: work

    indx = 0
    n = size(ai,1)
    inv = ai    

    call zgetrf(n,n,inv,n,indx,ierr)
    if (ierr /= 0) write (*,*) 'error returned from ZGETRF'

    call zgetri(n,inv,n,indx,work,LWORK,ierr)
    if (ierr /= 0) write (*,*) 'error returned from ZGETRI'

  end function invert_matrix

end program helmholtz_biem_main
