! $Id: invlap.f90,v 1.3 2006/10/22 19:14:05 kris Exp $
module deHoog_inverse_transform
  use constants, only : DP
  implicit none

  ! default values used in both procedures
  real(DP), parameter :: DEFALPHA = 1.0D-7, DEFTOL = 1.0D-9
  integer, parameter :: DEFM = 20
  logical, parameter :: DEFSMOOTH = .false.

  private
  public :: deHoog_invLap, deHoog_scalart_determine_p

  interface deHoog_invLap
     module procedure invLap_tScalar, invLap_tVector
  end interface

contains
  ! algorithm: de Hoog et al's quotient difference method with accelerated 
  !   convergence for the continued fraction expansion
  !   [de Hoog, F. R., Knight, J. H., and Stokes, A. N. (1982). An improved 
  !    method for numerical inversion of Laplace transforms. S.I.A.M. J. Sci. 
  !    and Stat. Comput., 3, 357-366.]
  ! Modification: The time vector is split in segments of equal magnitude
  !   which are inverted individually. This gives a better overall accuracy.   

  !  details: de Hoog et al's algorithm f4 with modifications (T->2*T and 
  !    introduction of tol). Corrected error in formulation of z.
  !
  !  Copyright: Karl Hollenbeck
  !             Department of Hydrodynamics and Water Resources
  !             Technical University of Denmark, DK-2800 Lyngby
  !             email: karl@isv16.isva.dtu.dk
  !  22 Nov 1996, MATLAB 5 version 27 Jun 1997 updated 1 Oct 1998
  !  IF YOU PUBLISH WORK BENEFITING FROM THIS M-FILE, PLEASE CITE IT AS:
  !    Hollenbeck, K. J. (1998) INVLAP.M: A matlab function for numerical 
  !    inversion of Laplace transforms by the de Hoog algorithm, 
  !    http://www.isva.dtu.dk/staff/karl/invlap.htm 
  !
  !  ############################################################
  !
  ! converted by Kris Kuhlman to Fortran 90/95 November, 2005
  !
  ! the function to be inverted must accept a vector corresponding to 
  ! the Laplace parameter and returns the image function as a vector.  
  !
  ! It is assumed the other parameters needed by this function
  ! are passes to it via a module shared with the routine that calls this one

  !$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$  
  function invLap_tScalar(imageFcn, t, alpha, tol, M, smooth) result(f)

    interface
       function imageFcn(p) result (fn_return)
         use constants, only : DP
         implicit none
         complex(DP), intent(in), dimension(:) :: p
         complex(DP), dimension(size(p,1)) :: fn_return
       end function imageFcn
    end interface
    
    real(DP) :: f
    real(DP), intent(in) :: t
    real(DP), optional, intent(in) :: alpha, tol
    integer, optional, intent(in) :: M
    logical, optional, intent(in) :: smooth
    logical :: smooth1
    real(DP) :: alpha1, tol1
    integer :: M1

    ! default values for optional arguments
    if(.not. present(alpha)) then
       alpha1 = DEFALPHA
    else
       alpha1 = alpha
    end if
    
    if(.not. present(tol)) then
       tol1 = DEFTOL
    else
       tol1 = tol
    end if
    
    if(.not. present(M)) then
       M1 = DEFM
    else
       M1 = M
    end if

    if(.not. present(smooth)) then
       smooth1 = DEFSMOOTH
    else
       smooth1 = smooth
    end if


    ! wrap time argument in array constructor and pass to vector version
    ! use sum to scalarize one-element vector output
    f = sum(invLap_tVector(imageFcn, (/t/), alpha1, tol1, M1, smooth1))

  end function invLap_tScalar
  !$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

  function invLap_tVector(imageFcn, t, alpha, tol, M, smooth) result(f)
    use constants, only : PI, PISQ, CZERO, RONE, RTWO

    ! Laplace-domain function to be inverted
    interface
       function imageFcn(p) result (fn_return)
         use constants, only : DP
         implicit none
         complex(DP), intent(in), dimension(:) :: p
         complex(DP), dimension(size(p,1)) :: fn_return
       end function imageFcn
    end interface
    
    ! ########## input ##########
    ! times at which inverse is desired (sorted ascending)
    real(DP), intent(in), dimension(:) :: t

    ! real value to the right of all poles of image function
    real(DP), optional, intent(in) :: alpha
    real(DP) :: alpha1

    ! desired tolerance of result
    real(DP), optional, intent(in) :: tol
    real(DP) :: tol1

    ! 2M+1 = # of terms in Fourier series approximation
    integer, optional, intent(in) :: M
    integer :: M1

    ! true = use Lanczos sigma smoothing factors
    ! default (when argument not present) is no smoothing
    logical, optional, intent(in) :: smooth

    ! ########## output ##########
    ! resulting time-domain function
    real(DP), dimension(size(t,1)) :: f

    ! internal variables
    real(DP) :: lanczos, rj
    logical, dimension(size(t,1)) :: tmask
    real(DP) :: gamma, tee
    integer, allocatable :: run(:)
    complex(DP), allocatable :: s(:),a(:)
    complex(DP), allocatable :: e(:,:)
    complex(DP), allocatable :: q(:,:)
    complex(DP), allocatable :: d(:)
    complex(DP), allocatable :: z(:), h2m(:), R2Mz(:)
    complex(DP), allocatable :: Am(:,:), Bm(:,:)
    real(DP), allocatable :: t1(:)
    real(DP), dimension(size(t,1)) :: logt
    integer :: iminlogt, imaxlogt, ilogt, nt, r, rq, lo, i, n, j

    ! split up t vector in pieces of same order of magnitude, invert one piece
    !   at a time. simultaneous inversion for times covering several orders of 
    !   magnitudes gives inaccurate results for the small times.
    
    ! default values for parameters
    if(.not. present(alpha)) then
       alpha1 = DEFALPHA
    else
       alpha1 = alpha
    end if
    
    if(.not. present(tol)) then
       tol1 = DEFTOL
    else
       tol1 = tol
    end if
    
    if(.not. present(M)) then
       M1 = DEFM
    else
       M1 = M
    end if

    ! these must be allocatable, so M can be an optional argument
    allocate(s(2*M1+1),a(2*M1+1),e(2*M1+1,M1+1),q(2*M1,M1+1),d(2*M1+1),run(2*M1+1))

    lo = 1 ! lower index of this log-cycle

    logt = log10(t)

    iminlogt = floor(minval(logt))
    imaxlogt = ceiling(maxval(logt))
    run = (/ (i, i=0,2*M1) /)
    do ilogt = iminlogt, imaxlogt  ! loop through all pieces

       ! put this log cycle into t1, rather than re-use t
       if (allocated(t1)) deallocate(t1,Am,Bm,z,h2m,R2Mz)

       ! a mask of times in original t vector which are in current logcycle
       tmask = (logt >= real(ilogt,DP)).and.(logt < real(ilogt+1,DP))
       nt = count(tmask)
       
       allocate(t1(nt),Am(1:2*M1+2,nt),Bm(1:2*M1+2,nt),z(nt),h2m(nt),R2Mz(nt))
       t1 = pack(t,tmask)

       if (nt > 0) then ! maybe no elements in that magnitude

          tee = maxval(t1)*RTWO
          gamma = alpha1 - log(tol1)/(2.0_DP*tee)

          ! NOTE: The correction alpha1 -> alpha1-log(tol1)/(2*tee) is not in de Hoog's
          !   paper, but in Mathematica's Mathsource (NLapInv.m) implementation of 
          !   inverse transforms

          ! find F argument, call F with it, get 'a' coefficients in power series
          s = cmplx(gamma, PI*run/tee, DP)

          a = imagefcn(s) ! arguments passed to image function via module

          a(1) = a(1)/RTWO   ! zero term is halved

          ! accelerating the sigma-smoothed function doesn't converge 
          ! to the original function, but to the smoothed one

          ! WARNING: Lanczos-style sigma smoothing has erratic effects for small values of 2M1+1
          ! (but so does the un-smoothed variety, so maybe it isn't a big deal)
          ! Lanczos "sigma" smoothing: sigma_0 = 1 and sigma_n = 0

          if (present(smooth) .and. smooth) then
             lanczos = PISQ/(tee*real(2*M1,DP))
             do j = 1, 2*M1
                rj = real(j,DP)
                a(j+1) = a(j+1)*sin(rj*lanczos)/(rj*lanczos)
             end do
          end if


          ! build up e and q tables. superscript is now row index, subscript column
          ! CAREFUL: paper uses null index, so all indeces are shifted by 1 here
          e(1:2*M1+1, 1:M1+1) = CZERO
          q(1:2*M1,   1:M1+1) = CZERO     ! column 1 does not exist
          e(1:2*M1,1) = CZERO
          q(1:2*M1,2) = a(2:2*M1+1)/a(1:2*M1)

          do r = 2,M1+1     ! step through columns (called r...)
             e(1:2*(M1-r+1)+1,r) = &
                  & q(2:2*(M1-r+1)+2,r) - q(1:2*(M1-r+1)+1,r) + e(2:2*(M1-r+1)+2,r-1)
             if (r < M1+1) then         ! one column fewer for q
                rq = r+1
                q(1:2*(M1-rq+1)+2,rq) = &
                     & q(2:2*(M1-rq+1)+3,rq-1)*e(2:2*(M1-rq+1)+3,rq-1)/e(1:2*(M1-rq+1)+2,rq-1)
             end if
          end do

          ! build up d vector (index shift 1)
          d(1:2*M1+1) = CZERO
          d(1) = a(1)
          d(2:2*M1:2)   = -q(1,2:M1+1) ! even terms
          d(3:2*M1+1:2) = -e(1,2:M1+1) ! odd terms

          ! build up A and B matricies (index shift 2), one row for each time
          Am(1:2*M1+2,1:nt) = CZERO
          Bm(1:2*M1+2,1:nt) = CZERO
          Am(2,:) = d(1)
          Bm(1:2,:) = RONE
          z = exp((0.0_DP,1.0_DP)*PI*t1/tee)

          do n = 3,2*M1+2
             Am(n,:) = Am(n-1,:) + d(n-1)*z*Am(n-2,:)  ! different index 
             Bm(n,:) = Bm(n-1,:) + d(n-1)*z*Bm(n-2,:)  !  shift for d!
          end do

          ! double acceleration
          h2M(1:nt) = (RONE + (d(2*M1) - d(2*M1+1))*z )/RTWO
          R2Mz(1:nt) = -h2M*(RONE - sqrt(RONE + d(2*M1+1)*z/h2M**2))
          Am(2*M1+2,:) = Am(2*M1+1,:) + R2Mz(1:nt)*Am(2*M1,:)
          Bm(2*M1+2,:) = Bm(2*M1+1,:) + R2Mz(1:nt)*Bm(2*M1,:)

          ! inversion, vectorized for times, result a vector
          f(lo:lo+nt-1) =  exp(gamma*t1)/tee*real(Am(2*M1+2,:)/Bm(2*M1+2,:),DP)
          lo = lo+nt

       end if ! if not empty time piece

    end do ! loop through time vector pieces
  end function invLap_tvector

  ! calculation of values of p for inverting a single time
  function deHoog_scalart_determine_p(t,alpha,tol,M) result(p)
    use constants, only : DP, PI, RTWO
    real(DP), intent(in) :: t,alpha, tol 
    integer, intent(in) :: M                 ! # FS terms
    complex(DP), dimension(2*M+1) :: p           ! required values of p

    real(DP) :: tee
    integer, dimension(2*M+1) :: run
    integer :: i
    
    run(1:2*M+1) = (/(i,i=0,2*M)/)

    tee = t*RTWO
    p(1:2*M+1) = cmplx(alpha - log(tol)/(RTWO*tee), PI*run(1:2*M+1)/tee, DP)
    
  end function deHoog_scalart_determine_p
  
end module deHoog_inverse_transform
