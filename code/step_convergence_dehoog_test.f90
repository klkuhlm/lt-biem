!!!  This program attempts to implement the Fourier series transformation
!!!  published by Oleksy in Computer Physics Communications (1996).
!!!  "A convergence accleration method of Fourier series", but there are some
!!!  issues.  

!!! 1) any discontinuities must be at the ends of the intervals
!!! the method cannot handle discontinuous time 

!!! 2) the method does not accelerate
!!! but only increase the niceness of a FS, to allow more rapid acceleration --
!!! I was having trouble implementing it using the q-d acceleration, so I used
!!! the simpler Crump (Wynn epsilon table) method. 

!!! 3) since the method uses a scaling factor of 2**j, the higher-level
!!! series in the approximation have fewer and fewer terms, unless you 
!!! compute the coefficients for high order, which removes the benefit of the
!!! method.  I think in the paper he assumes each of the higher-level series
!!! has the same number of terms (which means they are of very high order), 
!!! which is not practical for my current application.

program dehoog_smoothing 
  use constants, only : DP, PI

  implicit none

  integer, parameter :: NUMT = 150

  real(DP), dimension(NUMT) :: t, fttrans, ft
  real(DP) :: alpha, tol, tee, steep, shift, maxt, mint
  integer, parameter :: M = 16

  complex(DP), dimension(2*M+1) :: p, fp
  integer :: i, k

  maxt = 2.0_DP*PI
  mint = PI+epsilon(1.0_DP)

  steep = 20.0_DP
  shift = mint  ! put step at end of interval

  alpha = 0.0D0
  tol = 1.0D-12

  ! spread times out logarithmically between mint and maxt
  t = 10.0_DP**((log10(maxt)-log10(mint))*[(real(i,DP)/real(NUMT,DP), i=0,NUMT-1)] + log10(mint))
!!$  print *, 't',t


  ! loop over times
  do k = 1, NUMT
     tee = 2.0_DP*t(k)
     p = cmplx(alpha - log(tol)/(2.0_DP*tee), real([(i,i=0,2*M)],DP)*PI/tee, DP)
     
     !  step at t=PI
     fp = exp(-shift*p)/p
     
     ! accelerated with q-d, untransformed
     ft(k) = deHoog_invlap(alpha,tol,t(k),tee,fp(:),M, &
          & smooth=.false.,accel=.true.,transform=.false.)

     ! transformed, not accelerated with q-d algorithm
     fttrans(k) = deHoog_invlap(alpha,tol,t(k),tee,fp(:),M, &
          & smooth=.false.,accel=.false.,transform=.true.)

  end do
  
  ! output results
  open(unit=20, file='smooth-test.out',status='replace',action='write')

  write(20,*) '# fcn: Heaviside step at t=',shift
  
  write(20,*) '# time,   actual function,    inverted (no accel, no xform),   inverted (xformed)'
  do k = 1,NUMT
     write(20,*) t(k), funt(t(k),1,shift,steep), ft(k), fttrans(k)
  end do


contains
  real(DP) function funt(t,nfcn,shift2,steep2)
    implicit none
    real(DP), intent(in) :: t
    integer, intent(in) :: nfcn
    real(DP), intent(in) :: shift2, steep2

    select case(nfcn)
    case(1)
       ! standard step at 5
       if(t <= shift2) then
          funt = 0.0_DP
       else
          funt = 1.0_DP
       end if
    case(2)
       funt = 0.5_DP*(derf((t-shift2)*steep2) + 1.0_DP)
    end select
    

  end function funt
  
  !! an implementation of the de Hoog method
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  function deHoog_invLap(alpha,tol,t,tee,fp,M,smooth,accel,transform) result(ft)
    use constants, only : DP, PI, CZERO, CONE, EYE, HALF, RONE, RZERO
    real(DP), intent(in) :: alpha  ! abcissa of convergence
    real(DP), intent(in) :: tol    ! desired accuracy
    real(DP), intent(in) :: t,tee  ! time and scaling factor
    integer, intent(in) :: M
    complex(DP), intent(in), dimension(0:2*M) :: fp
    logical, intent(in) :: smooth, accel,transform
    complex(DP), dimension(0:2*M) :: fph  ! fp with zero term halved
    integer :: nup, mmp
    real(DP) :: ft, ftA, rnup, rmmp

    integer, parameter :: NU = 3
    real(DP), dimension(0:NU-1) :: ftB

    real(DP), dimension(0:2*M) :: run

    real(DP), dimension(0:2*M) :: lanczos
    real(DP) :: gamma, arg, r2M
    integer ::  j, mm


    ! there will be problems is fp(:)=0
    if(maxval(abs(fp)) > epsilon(1.0D0)) then

       lanczos(0:2*M) = RONE
       !! sigma_0 = 1
       !! sigma_N = 0 (last term (one more than your are summing) drops off)
       if(smooth) then
          r2M = real(2*M+1,DP)
          do j=1,2*M
             arg = PI*real(j,DP)/r2M
             lanczos(j) = sin(arg)/arg  !! sinc function
          end do
       end if

       ! Re(p) -- this is the de Hoog parameter c
       gamma = alpha - log(tol)/(2.0_DP*tee)


       ! implement Oleksy FS transformation
       if(transform) then

          ! accelerated with Wynn epsilon algorithm (crump method)
          run(0:2*M) = real([(i,i=0,2*M)],DP)

          ! part A (always just one sum)
          nup = 2**NU; rnup = real(nup,DP)

          ! transformed, but unaccelerated fourier series
!!$          ftA = rnup*real(sum(fp(nup:2*M:nup)*exp(EYE*PI*(rnup*t)*run(nup:2*M:nup)/tee)))
!!$          print *, 'transform part A',ftA

          ! transformed and accelerated fourier series
          ftA = wynn_epsilon(fp(nup:2*M:nup)*rnup*exp(EYE*PI*(rnup*t)*run(nup:2*M:nup)/tee))
!!$          print *, 'accelerated part A',ftA
          
          ! part B (nu sums)
          do mm = 0, NU-1
             mmp = 2**mm; rmmp = real(mmp,DP)
             
             ! transformed, but unaccelerated fourier series
!!$             ftB(mm) = real(sum(fp(mmp:2*M:mmp)*rmmp* exp(EYE*PI*(PI + rmmp*t)*run(mmp:2*M:mmp)/tee)))
!!$             print *, 'transform part B, step:',mm, ftB(mm)

             ! transformed and accelerated fourier series   
             ftB(mm) = wynn_epsilon(fp(mmp:2*M:mmp)*rmmp* exp(EYE*PI*(PI + rmmp*t)*run(mmp:2*M:mmp)/tee))
!!$             print *, 'accelerated part B',mm,ftB(mm)
          end do

          ! assemble parts into final approximation of original series
          ft = 0.5_DP*real(fp(0)) + ftA - sum(ftB(0:NU-1))

       else
          if(accel) then

             ! diagonal Pade approximation
             ! F=A/B represents accelerated trapezoid rule
             fph(0) = fp(0)*HALF
             fph(1:) = fp(1:)
             ft =  exp(gamma*t)/tee * pade_accel_std(fph,exp(EYE*PI*t/tee))

          else  ! unaccelerated & untransformed, just for testing sigma factors

             lanczos(0) = 0.5_DP  ! put factor of 1/2 into lanczos vector
             run(0:2*M) = real([(i,i=0,2*M)],DP)

             ! unaccelerated trapezoid rule
             ft = exp(gamma*t)/tee * real(sum(lanczos(0:2*M)*fp(0:2*M)*exp(EYE*PI*t*run(0:2*M)/tee)))
          end if
         end if

    else
       ft = RZERO
    end if

  end function deHoog_invLap

  !! ###################################################
  !! wynn-epsilon acceleration of fourier series
  function wynn_epsilon(funp) result(accsum)
    use constants, only : DP, CONE, CZERO

    complex(DP), dimension(1:), intent(in) :: funp
    complex(DP) :: denom
    real(DP) :: accsum
    integer :: np, i, j, m

    complex(DP), dimension(-1:size(fp)-1,0:size(fp)-1) :: ep

!!$    print *, 'wynn-epsilon',funp

    np = size(funp)

    ! first column is all zero
    ep(-1,:) = CZERO

    forall(i=1:np)
       ! initialize epsilon algoritm with partial sums
       ep(0,i) = sum(funp(1:i))
    end forall
    
    ! build up epsilon table
    do m=0,np-1 ! subscript (rows)
       do j=0,np-1  ! superscript (columns)
          denom = ep(m,j+1) - ep(m,j)
          if(abs(denom) > tiny(1.0_DP)) then ! check for div by zero
             ep(m+1,j) = ep(m-1,j+1) + CONE/denom
          else
             accsum = real(ep(m,j+1))
             print *, 'severe cancellation in epsilon table subscript:',m,' superscript:',j
             goto 777
          end if
          
       end do
    end do
    
    ! if made all the way through table use corner value as answer
    ! even columns are accelerated approximations
    if(mod(np,2) == 0) then
       accsum = real(ep(np,0)) 
    else
       accsum = real(ep(np-1,0))
    end if
    
777 continue

  end function wynn_epsilon
  

  !! ####################################################
  !! quotient-difference algorithm for standard de Hoog algorithm
  !! (non-transformed Fourier series)
  function pade_accel_std(fp,z) result(accsum)
    use constants, only : DP, HALF, CONE, CZERO, EYE

    ! function in Laplace space - first term halved already if necessary
    complex(DP), dimension(0:), intent(in) :: fp 

    ! base of power series
    complex(DP), intent(in) :: z

    ! accelerated sum (result)
    real(DP) :: accsum

    integer :: r, rq, n, max, nterms
    complex(DP), allocatable :: e(:,:)
    complex(DP), allocatable :: q(:,:)
    complex(DP), allocatable :: d(:)
    complex(DP), allocatable :: A(:),B(:)
    complex(DP) :: brem,rem

    nterms = size(fp)-1  !! 2M in de Hoog paper
    allocate(e(0:nterms,0:nterms/2),q(0:nterms,1:nterms/2),&
         & d(0:nterms),A(-1:nterms),B(-1:nterms))

    ! initialize Q-D table 
    e(0:nterms,0) = CZERO
    q(0:nterms-1,1) = fp(1:nterms)/fp(0:nterms-1)
    
    ! rhombus rule for filling in triangular Q-D table
    do r = 1,nterms/2
       ! start with e, column 1, 0:nterms-2
       max = 2*(nterms/2-r)
       e(0:max,r) = q(1:max+1,r) - q(0:max,r) + e(1:max+1,r-1)
       if (r /= nterms/2) then
          ! start with q, column 2, 0:nterms-3
          rq = r+1
          max = 2*(nterms/2-rq)+1
          q(0:max,rq) = q(1:max+1,rq-1) * e(1:max+1,rq-1) / e(0:max,rq-1)
       end if
    end do
    
    ! build up continued fraction coefficients
    d(0) = fp(0)
    forall(r = 1:nterms/2)
       d(2*r-1) = -q(0,r) ! even terms
       d(2*r)   = -e(0,r) ! odd terms
    end forall
    
    ! seed A and B vectors for recurrence
    A(-1) = CZERO
    A(0) = d(0)
    B(-1:0) = CONE
    
    ! coefficients of Pade approximation
    ! using recurrence for all but last term
    do n = 1,nterms-1
       A(n) = A(n-1) + d(n)*A(n-2)*z
       B(n) = B(n-1) + d(n)*B(n-2)*z
    end do
    
    ! "improved remainder" to continued fraction
    brem = (CONE + (d(nterms-1) - d(nterms))*z)*HALF
    rem = -brem*(CONE - sqrt(CONE + d(nterms)*z/brem**2))
    
    ! last term of recurrence using new remainder
    A(nterms) = A(nterms-1) + rem*A(nterms-2)
    B(nterms) = B(nterms-1) + rem*B(nterms-2)
    
    accsum = real(A(nterms)/B(nterms))

  end function pade_accel_std
  
!!$  !! ##########################################
!!$  !! quotient-difference algorithm for transformed fourier series
!!$  function pade_accel_tr(fp,z,factor) result(accsum)
!!$    use constants, only : DP, HALF, CONE, CZERO
!!$
!!$    ! function in Laplace space -- no fp(0) term (everything shifted one)
!!$    complex(DP), dimension(1:), intent(in) :: fp 
!!$
!!$    ! base of power series
!!$    complex(DP), intent(in) :: z
!!$
!!$    ! multiplier introduced due to transformation
!!$    integer, intent(in) :: factor
!!$
!!$    ! accelerated sum (result)
!!$    real(DP) :: accsum
!!$
!!$    integer :: r, rq, n, max, j, nterms
!!$    complex(DP), allocatable :: e(:,:)
!!$    complex(DP), allocatable :: q(:,:)
!!$    complex(DP), allocatable :: d(:)
!!$    complex(DP), allocatable :: A(:),B(:)
!!$    complex(DP) :: brem,rem,z
!!$
!!$    !! no fp(0) term
!!$    !! but vectors are still zero-based, 
!!$    !! so everything is shifted one to the left compared to other algorithm
!!$    nterms = size(fp)
!!$
!!$    allocate(e(0:nterms,0:nterms/2),q(0:nterms,1:nterms/2),&
!!$         & d(0:nterms),A(-1:nterms),B(-1:nterms))
!!$
!!$    ! initialize Q-D table
!!$    e(0:nterms,0) = CZERO
!!$    q(0:nterms-1,1) = fp(1:nterms)/fp(0:nterms-1)
!!$    
!!$    ! rhombus rule for filling in triangular Q-D table
!!$    do r = 1,(nterms+1)/2
!!$       ! start with e, column 1, 0:nterms-2
!!$       max = 2*(nterms/2-r)
!!$       e(0:max,r) = q(1:max+1,r) - q(0:max,r) + e(1:max+1,r-1)
!!$       if (r /= nterms/2) then
!!$          ! start with q, column 2, 0:nterms-3
!!$          rq = r+1
!!$          max = 2*(nterms/2-rq)+1
!!$          q(0:max,rq) = q(1:max+1,rq-1) * e(1:max+1,rq-1) / e(0:max,rq-1)
!!$       end if
!!$    end do
!!$    
!!$    ! build up continued fraction coefficients
!!$    d(0) = fp(0)
!!$    forall(r = 1:nterms/2)
!!$       d(2*r-1) = -q(0,r) ! even terms
!!$       d(2*r)   = -e(0,r) ! odd terms
!!$    end forall
!!$    
!!$    ! seed A and B vectors for recurrence
!!$    A(-1) = CZERO
!!$    A(0) = d(0)
!!$    B(-1:0) = CONE
!!$    
!!$    ! base of the power series
!!$    z = exp(EYE*PI*t/tee)
!!$    
!!$    ! coefficients of Pade approximation
!!$    ! using recurrence for all but last term
!!$    do n = 1,nterms-1
!!$       A(n) = A(n-1) + d(n)*A(n-2)*z
!!$       B(n) = B(n-1) + d(n)*B(n-2)*z
!!$    end do
!!$    
!!$    ! "improved remainder" to continued fraction
!!$    brem = (CONE + (d(nterms-1) - d(nterms))*z)*HALF
!!$    rem = -brem*(CONE - sqrt(CONE + d(nterms)*z/brem**2))
!!$    
!!$    ! last term of recurrence using new remainder
!!$    A(nterms) = A(nterms-1) + rem*A(nterms-2)
!!$    B(nterms) = B(nterms-1) + rem*B(nterms-2)
!!$    
!!$    accsum = real(A(nterms)/B(nterms))
!!$
!!$  end function pade_accel_tr

end program dehoog_smoothing
