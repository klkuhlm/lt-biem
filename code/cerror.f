	SUBROUTINE CERROR(Z,CER)
C
C       ====================================================
C       Purpose: Compute error function erf(z) for a complex
C                argument (z=x+iy)
C       Input :  z   --- Complex argument
C       Output:  CER --- erf(z)
C       ====================================================
C
	IMPLICIT COMPLEX *16 (C,Z)
	DOUBLE PRECISION A0,PI
	A0=CDABS(Z)
	C0=CDEXP(-Z*Z)
	PI=3.141592653589793D0
	Z1=Z
	IF (REAL(Z).LT.0.0) THEN
	   Z1=-Z
	ENDIF
	IF (A0.LE.5.8D0) THEN    
	   CS=Z1
	   CR=Z1
	   DO 10 K=1,120
	      CR=CR*Z1*Z1/(K+0.5D0)
	      CS=CS+CR
	      IF (CDABS(CR/CS).LT.1.0D-15) GO TO 15
10         CONTINUE
15         CER=2.0D0*C0*CS/DSQRT(PI)
	ELSE                              
	   CL=1.0D0/Z1              
	   CR=CL
	   DO 20 K=1,13
	      CR=-CR*(K-0.5D0)/(Z1*Z1)
	      CL=CL+CR
	      IF (CDABS(CR/CL).LT.1.0D-15) GO TO 25
20         CONTINUE
25         CER=1.0D0-C0*CL/DSQRT(PI)
	ENDIF
	IF (REAL(Z).LT.0.0) THEN
	   CER=-CER
	ENDIF
	RETURN
	END


