! $Id: biem_steady_module.f90,v 1.4 2006/11/01 17:42:29 kris Exp kris $
module biem_steady
use constants, only : DP, I4B, PI, TWOPI, RONE, RTWO, RZERO
use biem_transient, only : gaussSetup, basis
implicit none

private
public :: biem_steady_solution

contains

subroutine biem_steady_solution(coordG,connectG,intOrd,bc,type,sol_u,sol_t,cond, &
       & obsResults,obsLoc)
    use constants, only : PI, TWOPI, CZERO, HALF, RONE, RTWO, RZERO, PIOV2
    use biem_shared_data, only : jac,norm,length,otherBC

    real(DP), intent(in), dimension(:,:) :: coordG
    integer(I4B), intent(in), dimension(:,:) :: connectG
    integer(I4B), intent(in) :: intOrd
    real(DP), intent(in), dimension(:,:) :: bc
    integer(I4B), intent(in), dimension(:) :: type
    real(DP), intent(out), dimension(size(bc,dim=1),2) :: sol_u,sol_t
    real(DP), intent(in) :: cond
    real(DP), intent(in) :: obsLoc(:,:) ! (x,y),nObsPts
    real(DP), intent(out) :: obsResults(size(obsLoc,dim=2),3) ! nObsPts, (pot,x-flux,y-flux)

    ! maximum Gauss Quadrature order
    integer, parameter :: MAXINTORD = 10
    real(DP), dimension(MAXINTORD) :: GaussCoord, GaussWeight
    real(DP), dimension(2,MAXINTORD) :: GN  !, GLN

!!!! Locally-indexed matricies of boundary integrals => H=flux, G=potential
    real(DP), allocatable :: G(:,:,:), H(:,:,:)

!!!! coordinates and connectivity for current element
    real(DP) :: coordL(2,2) ! always 2 dim x 2 nodes per element
    integer(I4B) :: connectL(2)  ! always 2 nodes per element

!!!! Globally-indexed matrix of unknown boundary values => A
!!!! Globally-indexed vector of known/unknown boundary quantities => b, x
    real(DP), allocatable :: A(:,:), b(:), x(:)
    integer(I4B), allocatable :: indx(:)

!!!!! intermediate integration results
    real(DP) :: arg !int1, int2, 

!!!!! intermediate results during integration inside domain
    real(DP) :: delT,delU,delSx,delSy,delRx,delRy

!!!!! 2D vector field point
    real(DP), dimension(2) :: field

!!!! counters/indicies
    integer(I4B) ::  i, e, n ,m ! j
    integer(I4B) :: nObsPts, nElement, nNode !nz, ierr, 

    nObsPts = size(obsLoc,dim=2)
    nElement = size(connectG,dim=2)
    nNode = size(coordG,dim=2)

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! INTEGRATION OF GREEN'S FUNCTIONS + BASIS FUNCTIONS

! matricies which hold numerical integration results
allocate(H(nNode,nElement,2),G(nNode,nElement,2),&
     & A(nNode,nNode),b(nNode),x(nNode),indx(nNode))

! initialize
H = RZERO; G = RZERO
call GaussSetup(intOrd,GaussCoord,GaussWeight)  ! same intOrd for each element

! calculate basis functions for all nodes & gauss pts
GN(1:2,1:intOrd) = basis(GaussCoord(1:intOrd))

do i=1,nNode  ! collocation point (source pt)
   do e=1,nElement  ! elements (field points)
      connectL(1:2) = connectG(1:2,e)
      coordL(1:2,1:2) = coordG(1:2,connectL(1:2))

      do n=1,2 ! local numbering of nodes

         ! non-singular integrations *********
         if(i /= connectL(n)) then
            do m=1,intOrd             
               ! calculate location of mth Gauss point in global coordinates
               field(1:2) = sum(spread(GN(1:2,m),dim=1,ncopies=2)* &
                    & coordL(1:2,1:2),dim=2)
               
               ! potential integral
               G(i,e,n) = G(i,e,n) + &
                    & GN(n,m)*u(coordG(1:2,i),field(1:2))*Jac(e)*GaussWeight(m)

               ! flux integral
               H(i,e,n) = H(i,e,n) + GN(n,m)*&
                    & q(coordG(1:2,i),field(1:2),norm(e,1:2))*Jac(e)*GaussWeight(m)
            end do
!!$            write(30,'(2(I3),1X,2(A,F8.5))') e,n,'G:',G(i,e,n),' H:',H(i,e,n)
          
         ! singular integrations (diagonals) **********
         ! collocation point coincides with this node
         elseif(i == connectL(n)) then 

            ! determine H(i,i) for singular integral 
            ! (fraction of angle or -sum of off-diagonal terms)

            ! fraction of whole circle this interior angle makes
            ! 180 - theta = interior angle; where theta is angle between normals
            
            if(norm(e,1)==norm(otherBC(e,n,1),1) .and. &
                 & norm(e,2)==norm(otherBC(e,n,1),2)) then ! a flat stretch
               H(i,e,n) = HALF*HALF
            else ! some other angle
               arg = dot_product(norm(e,:),norm(otherBC(e,n,1),:))/&
                    & (Jac(e)*Jac(otherBC(e,n,1)))
               if (arg > RONE) then  ! reflect about x=1
                  arg = RONE - (arg - RONE)
               end if
               H(i,e,n) = (PI - acos(arg))/TWOPI*HALF
            end if
            
            G(i,e,n) = length(e)/(RTWO*cond*TWOPI)*(1.5_DP + log(RONE/length(e)))
!!$            G(i,e,n) = -length(e)/(8.0_DP*PI)*(RTWO*log(length(e)) + 3.0_DP)
!!$            write(30,'(2(I3),1X,2(A,F8.5))') e,n,'G:',G(i,e,n),' H:',H(i,e,n)
         end if
      end do
   end do
end do

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! ASSEMBLE GLOBAL LHS AND RHS FROM LOCALLY INDEXED MATRICIES AND SPECIFIED BC

A(:,:) = RZERO; b(:) = RZERO   ! lhs, rhs

! DelU == G (potential integral)*(flux); begins on RHS
! DelT == H (flux integral)*(potential); begins on LHS

do e=1,nElement
   connectL(1:2) = connectG(1:2,e)
   do n=1,2
      select case(type(e))
      case(2) ! Neumann bc
         ! flux is known (put on RHS)
         ! set rhs: (computed potential integral)*(specified normal flux)
         b(1:nNode) = b(1:nNode) + G(1:nNode,e,n)*bc(e,n)
         
         select case(type(otherBC(e,n,1)))
         case(2) ! adjoining element is also a Neumann bc
            ! potential is unknown (put on LHS)
            ! set lhs: computed flux integral
            A(1:nNode,connectL(n)) = A(1:nNode,connectL(n)) + H(1:nNode,e,n)
         case(1) ! adjoining element is Dirichlet bc
            ! potential is known (put on RHS)
            ! set rhs: (-1)*(computed flux integral)*(specified head)
            b(1:nNode) = b(1:nNode) - H(1:nNode,e,n)*bc(otherBC(e,n,1),otherBC(e,n,2))
         end select
      case(1) ! Dirichlet bc
         ! flux is unknown (put on LHS)
         ! set lhs: (-1)*(computed head integral)
         A(1:nNode,connectL(n)) = A(1:nNode,connectL(n)) - G(1:nNode,e,n)
         ! head is known (put on RHS)
         ! set rhs: (-1)*(computed flux integral)*(specified head)
         b(1:nNode) = b(1:nNode) - H(1:nNode,e,n)*bc(e,n)
      case default
         print *, 'only type I or II bc currently handled ',bc(e,n)
         stop
      end select
   end do
end do

!!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! SOLVE MATRIX PROBLEM USING LU DECOMPOSITION

call ludcmp(A,indx)
x = b
call lubksb(A,indx,x)

write(30,*) 'x:',x

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! BUILD UP VECTORS OF BOUNDARY HEAD AND N-FLUX FOR POST-PROCESSING

do e=1,nElement
   connectL(1:2) = connectG(1:2,e)
   do n=1,2
      select case(type(e))
      case(2)
         ! Neumann BC - specified flux
         sol_t(e,n) = bc(e,n)
         
         select case(type(otherBC(e,n,1)))
         case(2)
            ! adjoining element is also Neumann
            ! therefore head was calculated
            sol_u(e,n) = x(connectL(n))
         case(1)
            ! adjoining element is Dirichlet
            ! therefore head was specified (for other element)
            sol_u(e,n) = bc(otherBC(e,n,1),otherBC(e,n,2))
         end select
      case(1)
         ! Dirichlet BC - specified head, computed flux
         sol_u(e,n) = bc(e,n)
         sol_t(e,n) = x(connectL(n))
      end select
   end do
end do

write(30,*) 'sol_u:',sol_u
write(30,*) 'sol_t:',sol_t

!!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! CALCULATE POTENTIAL AND FLUX VECTOR AT POINTS INSIDE DOMAIN

obsResults(:,:) = RZERO ! obs index; head,x-flux,y-flux

do i=1,nObsPts
   do e=1,nElement
      connectL(1:2) = connectG(1:2,e)
      coordL(1:2,1:2) = coordG(1:2,connectL(1:2))

      do n=1,2

         ! calculate potential inside the domain
         delT=RZERO; delU=RZERO; delSx=RZERO; delSy=RZERO;
         delRx=RZERO; delRy=RZERO;

         do m=1,intOrd
            field(1:2) = sum(spread(GN(1:2,m),dim=1,ncopies=2)* &
                 & coordL(1:2,1:2),dim=2)

            ! potential
            delU = delU + GN(n,m)*u(obsLoc(:,i),field(:))*Jac(e)*GaussWeight(m)
            delT = delT + GN(n,m)*q(obsLoc(:,i),field(:),norm(e,:))*Jac(e)*GaussWeight(m)

            ! x-flux
            delSx = delSx + GN(n,m)*Du(obsLoc(:,i),field(:),1)*Jac(e)*GaussWeight(m)
            delRx = delRx + GN(n,m)*Dq(obsLoc(:,i),field(:),norm(e,:),1)*Jac(e)*GaussWeight(m)
            
            ! y-flux
            delSy = delSy + GN(n,m)*Du(obsLoc(:,i),field(:),2)*Jac(e)*GaussWeight(m)
            delRy = delRy + GN(n,m)*Dq(obsLoc(:,i),field(:),norm(e,:),2)*Jac(e)*GaussWeight(m)
         end do
         ! potential
         obsResults(i,1) = obsResults(i,1) - delT*sol_u(e,n) + delU*sol_t(e,n) 
         ! x-flux
         obsResults(i,2) = obsResults(i,2) - delRx*sol_u(e,n) + delSx*sol_t(e,n)
         ! y-flux
         obsResults(i,3) = obsResults(i,3) - delRy*sol_u(e,n) + delSy*sol_t(e,n)
      end do
   end do
end do

obsResults(:,2:3) = -cond*obsResults(:,2:3)

end subroutine biem_steady_solution

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! GREEN'S FUNCTIONS

! basic 2D Green's function
!##################################################
function u(x1,x2) result(pot)
  use constants, only : RONE, TWOPI
  real(DP), intent(in), dimension(2) :: x1,x2 ! source pt (P), field pt (Q)
  real(DP) :: pot  ! potential at each point
  real(DP), dimension(2) :: r ! vector pointing from source to field point

  r(1:2) = x2(1:2) - x1(1:2)

  ! Green's function for 2D Laplace solution
  pot = -log(sqrt(dot_product(r,r)))/(TWOPI)

end function u

! x or y deriv of basic greens fcn (with respect to source point)
!##################################################
function Du(x1,x2,xy) result(dpot)
  real(DP), intent(in), dimension(2) :: x1,x2 ! source pt (P), field pt (Q)
  integer(I4B), intent(in) :: xy
  real(DP) :: distsq,dpot  ! potential at each point
  real(DP), dimension(2) :: r ! vector pointing from source to field point

  if(xy /= 1 .and. xy /= 2) stop 'incorrect value for derivative direction'
  
  r(1:2) = x2(1:2) - x1(1:2)
  distsq = dot_product(r,r)

  ! Green's function for 2D Laplace solution
  dpot = r(xy)/(TWOPI*distsq)

end function Du

! directional derivative of Green's function 
!##################################################
function q(x1,x2,nn) result(flux)
  real(DP), intent(in), dimension(2) :: x1,x2,nn  ! source pt, field pt, unit normal
  real(DP) :: distsq,flux  ! length of r squared, deriviative of potential
  real(DP), dimension(2) :: r ! vector pointing from source to field point

  r(1:2) = x2(1:2) - x1(1:2)
  distsq = dot_product(r,r)
  
  flux = -dot_product(nn,r)/(TWOPI*distsq)

end function q

! X or Y deriv of normal derivative of Green's function
!##################################################
function Dq(x1,x2,nn,xy) result(Df)
  real(DP), intent(in), dimension(2) :: x1,x2,nn  ! source pt, field pt, normal
  integer(I4B), intent(in) :: xy
  real(DP) :: Df, distsq
  real(DP), dimension(2) :: r ! vector pointing from source to field point

  if(xy /= 1 .and. xy /= 2) stop 'incorrect value for derivative direction'

  r(1:2) = x2(1:2) - x1(1:2)
  distsq = dot_product(r,r)
  
  Df = nn(xy)/(TWOPI*distsq) - dot_product(nn,r)*r(xy)/(PI*distsq**2)

end function Dq

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! MATRIX SOLVER (LU DECOMPOSITION)
! simplified version of NR version
! Press, etal, Numerical Recipes in f90, p1016
!##################################################
subroutine ludcmp(a,indx)
  use constants, only : RONE
  real(DP), dimension(:,:), intent(inout) :: a
  integer(I4B), dimension(:), intent(out) :: indx
  real(DP), dimension(size(a,dim=1)) :: vv
  real(DP), parameter :: TINY = 1.0E-20_DP
  integer(I4B) :: j,n,imax
  
  n = size(a,dim=1)
  vv = maxval(abs(a),dim=2)
  if (any(vv == 0.0)) print *, 'singular matrix in LUDCMP'
  vv = RONE/vv
  do j=1,n
     imax = (j-1) + sum(maxloc(vv(j:n)*abs(a(j:n,j))))
     if (j /= imax) then
        call swap(a(imax,:),a(j,:))
        vv(imax) = vv(j)
     end if
     indx(j) = imax
     if (a(j,j) == 0.0) a(j,j) = TINY
     a(j+1:n,j) = a(j+1:n,j)/a(j,j)
     a(j+1:n,j+1:n) = a(j+1:n,j+1:n) - outerprod(a(j+1:n,j),a(j,j+1:n))
  end do
end subroutine ludcmp

!##################################################
subroutine lubksb(a,indx,b)
  real(DP), intent(in), dimension(:,:) :: a
  integer(I4B), intent(in), dimension(:) :: indx
  real(DP), intent(inout), dimension(:) :: b
  
  integer(I4B) :: i,n,ii,ll
  real(DP) :: summ

  n = size(a,dim=1)
  ii = 0
  do i=1,n
     ll = indx(i)
     summ = b(ll)
     b(ll) = b(i)
     if (ii /= 0) then
        summ = summ - dot_product(a(i,ii:i-1),b(ii:i-1))
     elseif (summ /= 0.0) then
        ii = i
     end if
     b(i) = summ
  end do
  do i=n,1,-1
     b(i) = (b(i) - dot_product(a(i,i+1:n),b(i+1:n)))/a(i,i)
  end do
end subroutine lubksb

! numerical recipes utility routines
!##################################################
subroutine swap(a,b)
  real(DP), intent(inout), dimension(:) :: a,b
  real(DP), dimension(size(a)) :: tmp

  tmp = a
  a = b
  b = tmp
end subroutine swap

function outerprod(a,b) result(c)
  real(DP), intent(in), dimension(:) :: a,b
  real(DP), dimension(size(a),size(b)) :: c

  c = spread(a,dim=2,ncopies=size(b))* &
    & spread(b,dim=1,ncopies=size(a))
end function outerprod

end module biem_steady
