program laplace_biem_main

implicit none

!!!!! constants
integer, parameter :: DP = selected_real_kind(p=15,r=300)
integer, parameter :: I4B = selected_int_kind(r=8)
real(DP), parameter :: PI = 3.141592653589793238462643383279503_DP
real(DP), parameter :: TWOPI = 6.28318530717958647692528676655901_DP
real(DP), parameter :: ZERO = 0.0_DP, HALF = 0.5_DP, ONE = 1.0_DP, TWO = 2.0_DP

!!!!! element coordinates and connectivity (G = global, L = local)
real(DP), allocatable :: coordG(:,:), coordL(:,:), bc(:,:)
integer(I4B), allocatable :: connectG(:,:), connectL(:), type(:), indx(:)
!!!!! solution vectors/matricies
real(DP), allocatable :: sol_u(:,:), sol_t(:,:), obsLoc(:,:), obsRes(:,:)
!!!!! things related to Gauss Quadrature
real(DP), dimension(8) :: GaussCoord, GaussWeight, GLCoord, GLWeight, xsi2

!!$! matricies of numerical integration results 
!! Locally-indexed matricies of boundary integrals => H=flux, G=potential
!! Globally-indexed matrix of unknown boundary values => A
!! Globally-indexed vector of known/unknown boundary quantities => b, x
real(DP), allocatable :: G(:,:,:), H(:,:,:), A(:,:), b(:), x(:)
!!$real(DP), allocatable :: outpot(:,:), outflux(:,:,:), xout(:,:), yout(:,:)

integer(I4B) :: nElement, nodesPer, maxNode, intOrd, nObsPts,otherBC(2)
integer(I4B) ::  i, j, e, n ,m !!, mult
!!!!! Jacobian and intermediate results
real(DP) :: Jac, Jac2, int1, int2, k, J1, J2
!!!!! intermediate results during integration inside domain
real(DP) :: intp1, intp2, intfx1, intfx2, intfy1, intfy2
!!!!! basis functions at guass-points
real(DP), allocatable :: GN(:,:), GLN(:,:)
!!!!! 2D vectors
real(DP), dimension(2) :: norm, field, n1, n2
!!$character(7) :: prefix = 'lap-bem'

open(unit=20, file='input.dat', status='old', action='read')
open(unit=30, file='output.dat', status='replace', action='write')

! read input from file, echo to output file for debugging
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
read(20,*) nElement, nodesPer, intOrd, nObsPts, k
write(30,*) 'number of elements= ',nElement
write(30,*) 'number of nodes per element= ',nodesPer
write(30,*) 'integration order= ',intOrd
write(30,*) 'number of calcualtion points= ', nObsPts
write(30,*) 'hydraulic conductivity = ', k

allocate(connectG(nodesPer,nElement),GN(nodesPer,8),GLN(nodesPer,8),&
     & type(nElement), bc(nElement,nodesPer))

do i=1,nElement
   ! loop over elements, reading in global indicies, bc type, and bc values
   read(20,*) connectG(1:nodesPer,i), type(i), bc(i,1:nodesPer)
   write(30,*) 'elem:',i,' node:',connectG(1:nodesPer,i),&
        & ' bc:',type(i),' bc_val:',bc(i,1:nodesPer)
end do

maxNode = maxval(connectG(:,:))
allocate(coordG(2,maxNode), connectL(nodesPer), coordL(2,nodesPer),obsLoc(2,nObsPts))

do i=1,maxNode
   ! loop over nodes, reading in global coordinates (x,y)
   ! type = 1 : Dirichlet
   ! type = 2 : Neumann
   ! type = 3 : mixed (not yet)
   read(20,*) coordG(1:2,i)
end do

! echo input node coordinates for debugging ###############
do i=1,maxNode
   write(30,'(A5,I3,A7,2(1X,F7.3))') 'node:',i,' (x,y):',coordG(:,i)
end do

do i=1,nObsPts
   read(20,*) obsLoc(:,i)
end do

! echo input observation locations inside domain for debugging ###############
do i=1,nObsPts
   write(30,'(A8,I3,A7,2(1X,F7.3))') 'calc_pt:',i,' (x,y):',obsLoc(:,i)
end do


! matricies which hold numerical integration results
allocate(H(maxNode,nElement,nodesPer),G(maxNode,nElement,nodesPer),&
     & A(maxNode,maxNode),b(maxNode),x(maxNode),indx(maxNode))

H = ZERO; G = ZERO

! calculate off-diagonal entries of matricies 
!############################################################
call GaussSetup(intOrd,GaussCoord,GaussWeight)  ! same intOrd for each element
call GaussLaguerreSetup(intOrd,GLCoord,GLWeight)

! calculate basis functions for all nodes & gauss pts
! (same for all off-diagonal elements)
GN(1:nodesPer,1:intOrd) = basis(GaussCoord(1:intOrd),nodesPer)

do i=1,maxnode  ! collocation point (source pt)
   do e=1,nElement  ! elements (field points)
      connectL(1:nodesPer) = connectG(1:nodesPer,e)
      coordL(1:2,1:nodesPer) = coordG(1:2,connectL(1:nodesPer))

      do n=1,nodesPer ! local numbering of nodes

         ! only if colocation pt & field pt are different nodes
         ! non-singular integrals for off-diagonal terms
         !$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
         if(i /= connectL(n)) then
            call normalJacobian(norm(1:2),Jac,ZERO,2,coordL(1:2,1:nodesPer))
            write(30,*) 'std src:',i,' element:',e,' node:',n, ' norm:',norm,' jacobian:',Jac
            do m=1,intOrd
               
               ! calculate location of mth Gauss point in global coordinates
               field(1:2) = sum(spread(GN(1:nodesPer,m),dim=1,ncopies=2)* &
                    & coordL(1:2,1:nodesPer),dim=2)
               
               ! potential integral
               G(i,e,n) = G(i,e,n) + &
                    & GN(n,m)*u(coordG(1:2,i),field(1:2))*Jac*GaussWeight(m)

               ! flux integral
               H(i,e,n) = H(i,e,n) + &
                    & GN(n,m)*q(coordG(1:2,i),field(1:2),norm(1:2))*Jac*GaussWeight(m)
            end do
            write(30,*) 'G:',G(i,e,n),'H:',H(i,e,n)
          
         ! calculate diagonal entries (singular integrals)
         !$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
         elseif(i == connectL(n)) then ! collocation point coincides with this node
            
            ! determine interior angle at the collocation point
            otherBC(1:2) = nextBC(e,n)
            call normalJacobian(n1(1:2),J1,ZERO,2,coordL(1:2,1:2)) ! this element
            call normalJacobian(n2(1:2),J2,ZERO,2,&
                 & coordG(1:2,connectG(1:2,otherBC(1)))) !adjoining element for this node

            ! fraction of whole circle this interior angle makes
            ! 180 - theta = interior angle; where theta is angle between normals
            
            ! zero flux normal to element, due to symmetry (linear element only) 
            ! only term remaining is due to geometry of corner
            if(n1(1) == n2(1) .and. n1(2)==n2(2)) then ! a flat stretch (1/2 a circle)
               H(i,e,n) = HALF  
            else ! some other angle
               H(i,e,n) = (PI - acos(dot_product(n1,n2)/(J1*J2)))/TWOPI
            end if
            
            call normalJacobian(norm(1:2),Jac,GaussCoord(1),nodesPer,coordL(1:2,1:nodesPer))
!!$            write(30,*) 'sng src:',i,' element:',e,' node:',n, ' jacobian:',Jac

            int2 = sum(GN(n,1:intOrd)*log(ONE/(TWO*Jac))*Jac*GaussWeight(1:intOrd))
!!$            write(30,*) 'int2',int2 ! this should be a constant, 
!!$            ! if all elements have same Jacobian everywhere

            ! Jacobian2 is from Gaussian Quad coords -> G-L Quad coords
            ! this integration is with from 0->1
            if(n == 1) then
               Jac2 = TWO
               xsi2(1:intOrd) = TWO*GLCoord(1:intOrd) - ONE
            elseif(n == 2) then
               Jac2 = TWO ! removed negative
               xsi2(1:intOrd) = ONE - TWO*GLCoord(1:intOrd)
            else
               print *, 'unsupported number of nodes per element',n
               stop
            end if

            call normalJacobian(norm(1:2),Jac,xsi2(1),nodesPer,coordL(1:2,1:nodesPer))
!!$            write(30,*) 'sng GL:',Jac

            GLN(1:nodesPer,1:intOrd) = basis(xsi2(1:intOrd),nodesPer)
             
            int1 = sum(GLN(n,1:intOrd)*Jac2*Jac*GLWeight(1:intOrd))
            ! this sum can have 2 values, depending on 
            ! which node (n=1 or 2) has the singular point
!!$            write(30,*) 'int1:',int1

            G(i,e,n) = (int1 + int2)/TWOPI
            write(30,*) 'i:',i,'e:',e,'e:',n,'G:',G(i,e,n),'H:',H(i,e,n)
         end if
      end do
   end do
end do


!!$do i=1,maxNode
!!$   write(30,*) 'G(',i,',e,n) ',G(i,:,:)
!!$   write(30,*) 'H(',i,',e,n) ',H(i,:,:)
!!$end do


! assemble global LHS and RHS from locally indexed matricies and specified BC
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
A(:,:) = ZERO; b(:) = ZERO   ! lhs, rhs

! DeltaU == G (potential integral)*(flux); begins on RHS
! DeltaT == H (flux integral)*(potential); begins on LHS

do e=1,nElement
   connectL(1:nodesPer) = connectG(1:nodesPer,e)
   do n=1,nodesPer
      select case(type(e))
      case(2) ! Neumann bc
         ! flux is known (put on RHS)
         ! set rhs: (computed potential integral)*(specified normal flux)
         b(1:maxNode) = b(1:maxNode) + G(1:maxNode,e,n)*bc(e,n)

         ! moved this code to a function below
         otherBC(1:2) = nextBC(e,n)

         select case(type(otherBC(1)))
         case(2) ! adjoining element is also a Neumann bc
            ! potential is unknown (put on LHS)
            ! set lhs: computed flux integral
!!$            write(30,*) 'N-N',e,n
            A(1:maxNode,connectL(n)) = A(1:maxNode,connectL(n)) + H(1:maxNode,e,n)
         case(1) ! adjoining element is Dirichlet bc
            ! potential is known (put on RHS)
            ! set rhs: (-1)*(computed flux integral)*(specified head)
!!$            write(30,*) 'N-D',e,n
            b(1:maxNode) = b(1:maxNode) - H(1:maxNode,e,n)*bc(e,n)
         end select
      case(1) ! Dirichlet bc
         ! flux is unknown (put on LHS)
         ! set lhs: (-1)*(computed head integral)
!!$         write(30,*) 'D-*',e,n
         A(1:maxNode,connectL(n)) = A(1:maxNode,connectL(n)) - G(1:maxNode,e,n)
         ! head is known (put on RHS)
         ! set rhs: (-1)*(computed flux integral)*(specified head)
         b(1:maxNode) = b(1:maxNode) - H(1:maxNode,e,n)*bc(e,n)
      case default
         print *, 'only type I or II bc currently handled ',bc(e,n)
         stop
      end select
   end do
end do

!!$write(30,*) 'A'
!!$write(30,*) A
!!$write(30,*) 'b'
!!$write(30,*) b


! solve matrix problem using LU decomposition
!########################################################################
call ludcmp(A,indx)
x = b
call lubksb(A,indx,x)

! build up vectors of head and flux (specified and calculated) for post-processing
!########################################################################
allocate(sol_u(nElement,nodesPer),sol_t(nElement,nodesPer),obsRes(nObsPts,3))

do e=1,nElement
   connectL(1:nodesPer) = connectG(1:nodesPer,e)
   do n=1,nodesPer
      select case(type(e))
      case(2)
         ! Neumann BC - specified flux
         sol_t(e,n) = bc(e,n)
         
         otherBC(1:2) = nextBC(e,n)
         select case(type(otherBC(1)))
         case(2)
            ! adjoining element is also Neumann
            ! therefore head was calculated
            sol_u(e,n) = x(connectL(n))
!!$            write(30,*) 'N-N',e,n
         case(1)
            ! adjoining element is Dirichlet
            ! therefore head was specified
            sol_u(e,n) = bc(otherBC(1),otherBC(2))
!!$            write(30,*) 'N-D',e,n
         end select
      case(1)
         ! Dirichlet BC - specified head, computed flux
         sol_u(e,n) = bc(e,n)
         sol_t(e,n) = x(connectL(n))
!!$         write(30,*) 'D-*',e,n
      end select
   end do
end do

! write boundary results to output file
!########################################################################
write(30,*) 'specified/calculated head at each element and node &
     & (element, node, (x,y), head, normal flux)'
do e=1,nElement
   connectL(1:nodesPer) = connectG(1:nodesPer,e)
   coordL(1:2,1:nodesPer) = coordG(1:2,connectL(1:nodesPer))
   do n=1,nodesPer
      write(30,'(2(1X,I3),4(1X,F9.4))') e,n,coordL(1:2,n),sol_u(e,n),sol_t(e,n)
   end do
end do

! calculate potential and flux vector at points inside domain
!########################################################################

obsRes(:,:) = ZERO

do i=1,nObsPts
   do e=1,nElement
      connectL(1:nodesPer) = connectG(1:nodesPer,e)
      coordL(1:2,1:nodesPer) = coordG(1:2,connectL(1:nodesPer))
      do n=1,nodesPer

         ! calculate potential inside the domain
         intp1 = ZERO; intp2 = ZERO; intfx1 = ZERO; intfx2 = ZERO; intfy1 = ZERO; intfy2 = ZERO
         do m=1,intOrd
            call normalJacobian(norm(1:2),Jac,GaussCoord(m),nodesPer,coordL(1:2,1:nodesPer)) 
            field(1:2) = sum(spread(GN(1:nodesPer,m),dim=1,ncopies=2)* &
                 & coordL(1:2,1:nodesPer),dim=2)

            ! potential
            intp2 = intp2 + GN(n,m)*u(obsLoc(1:2,i),field(1:2))*Jac*GaussWeight(m)
            intp1 = intp1 + GN(n,m)*q(obsLoc(1:2,i),field(1:2),norm(1:2))*Jac*GaussWeight(m)

            ! x-flux
            intfx2 = intfx2 + GN(n,m)*Dux(obsLoc(1:2,i),field(1:2))*Jac*GaussWeight(m)
            intfx1 = intfx1 + GN(n,m)*Dqx(obsLoc(1:2,i),field(1:2),norm(1:2))*Jac*GaussWeight(m)
            
            ! y-flux
            intfy2 = intfy2 + GN(n,m)*Duy(obsLoc(1:2,i),field(1:2))*Jac*GaussWeight(m)
            intfy1 = intfy1 + GN(n,m)*Dqy(obsLoc(1:2,i),field(1:2),norm(1:2))*Jac*GaussWeight(m)
         end do
         obsRes(i,1) = obsRes(i,1) + intp1*sol_u(e,n) - intp2*sol_t(e,n) ! potential
         obsRes(i,2) = obsRes(i,2) + intfx1*sol_u(e,n) - intfx2*sol_t(e,n) ! x-flux
         obsRes(i,3) = obsRes(i,3) + intfy1*sol_u(e,n) - intfy2*sol_t(e,n) ! y-flux
      end do
   end do
end do

!!! write results to a table
write(30,*) 'results inside the domain: (x,y), head, x-flux, y-flux'
do i=1,nObsPts
   write(30,'(5(1X,F9.4))') obsLoc(1:2,i),obsRes(i,1:3)
end do
close(20); close(30)


!!$! write output for surface/contour plot in matlab
!!$! to scale rectangular domain
!!$mult=2
!!$
!!$! build up output into matricies first, then output
!!$allocate(outpot(mult*2+1,mult*3+1),outflux(mult*2+1,mult*3+1,2),&
!!$     & xout(mult*2+1,mult*3+1),yout(mult*2+1,mult*3+1))
!!$
!!$open(unit=40,file='x.dat',status='replace',action='write')
!!$do j = 1,mult*2+1
!!$   xout(j,:) = (/ (real(i)/real(mult),i=0,mult*3) /)
!!$   write(40,*) xout(j,:)
!!$end do
!!$close(40)
!!$
!!$open(unit=40,file='y.dat',status='replace',action='write')
!!$do j = mult*2+1,1,-1
!!$   yout(j,:) = spread(real(j-1)/real(mult),dim=1,ncopies=mult*3+1)
!!$   write(40,*) yout(j,:)
!!$end do
!!$close(40)
!!$
!!$do i=1,mult*2+1 ! rows
!!$   print *, 'i',i
!!$   do j=1,mult*3+1 ! columns
!!$      print *, 'j',j
!!$      if(i==1 .or. i==(mult*2+1) .or. j==1 .or. j==(mult*3+1)) then ! boundary of rectange
!!$
!!$         ! this only finds _one_ of the two elements associated with the given point (node)
!!$         ! this is a problem for fluxes (associated with the element, not the node)
!!$         call find_bdry_pt(xout(i,j),yout(i,j),e,n)
!!$         print *, xout(i,j),yout(i,j),'e:',e,'n:',n
!!$         if(e == -1 .or. n == -1) then
!!$            print *, 'bdry node not found at:',xout(i,j),yout(i,j),i,j
!!$            stop
!!$         end if
!!$         outpot(i,j) = sol_u(e,n)
!!$         
!!$         ! get direction associated with element
!!$         call normalJacobian(norm(1:2),Jac,ZERO,2,coordG(1:2,connectG(1:nodesPer,e))) 
!!$
!!$         ! thses may be very wrong on corners
!!$         outflux(i,j,1) = norm(1)*sol_t(e,n)
!!$         outflux(i,j,2) = norm(2)*sol_t(e,n)
!!$      else ! inside the domain
!!$         n = find_domain_pt(xout(i,j),yout(i,j))
!!$         print *, xout(i,j),yout(i,j),'n',n
!!$         if(n == -1) then
!!$            print *, 'interior node not found at:',xout(i,j),yout(i,j),i,j
!!$            stop
!!$         end if
!!$         outpot(i,j) = obsRes(n,1)
!!$         outflux(i,j,1) = obsRes(n,2)
!!$         outflux(i,j,2) = obsRes(n,3)
!!$      end if
!!$   end do
!!$end do
!!$
!!$open(unit=40,file='mat-pot.dat',status='replace',action='write')
!!$do j = 1,mult*2+1
!!$   write(40,*) outpot(j,:)
!!$end do
!!$close(40)
!!$
!!$open(unit=40,file='mat-flux-x.dat',status='replace',action='write')
!!$do j = 1,mult*2+1
!!$   write(40,*) outflux(j,:,1)
!!$end do
!!$close(40)
!!$
!!$open(unit=40,file='mat-flux-y.dat',status='replace',action='write')
!!$do j = 1,mult*2+1
!!$   write(40,*) outflux(j,:,2)
!!$end do
!!$close(40)
!!$
!!$open(unit=40,file='gnuplot-head.dat',status='replace',action='write')
!!$do j = 1,mult*2+1
!!$   do i = 1,mult*3+1
!!$      write(40,*) xout(j,i),yout(j,i),outpot(j,i)
!!$   end do
!!$end do
!!$close(40)


contains

!!$function find_domain_pt(x,y) result(res)
!!$  real(DP), parameter :: TOL = 1.0D-1
!!$  real(DP), intent(in) :: x,y
!!$  integer(I4B) :: res,jj
!!$  
!!$  res = -1 ! indicates point not found
!!$  search: do jj=1,nObsPts
!!$     
!!$     if((x-obsLoc(1,jj))**2 + (y-obsLoc(2,jj))**2 <= TOL**2) then
!!$        res = jj
!!$        exit search
!!$     end if
!!$  end do search
!!$  
!!$end function find_domain_pt
!!$
!!$subroutine find_bdry_pt(x,y,res1,res2)
!!$  real(DP), parameter :: TOL = 1.0D-2
!!$  real(DP), intent(in) :: x,y
!!$  integer(I4B),intent(out) :: res1,res2
!!$  integer(I4B) :: ee,nn
!!$  
!!$  res1 = -1; res2 = -1
!!$  search: do ee=1,nElement
!!$     connectL(1:2) = connectG(1:2,ee)
!!$     do nn=1,2
!!$        if(sqrt((x-coordG(1,connectL(nn)))**2 + (y-coordG(2,connectL(nn)))**2) <= TOL) then
!!$           res1 = ee
!!$           res2 = nn
!!$           exit search
!!$        end if
!!$     end do
!!$  end do search
!!$    
!!$end subroutine find_bdry_pt

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! abcissa and weights for Gauss and Gauss-Laguerre quadrature

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subroutine GaussSetup(intOrd,coord,weight)
  integer(I4B), intent(in) :: intOrd    ! Gauss integration order
  real(DP), intent(out), dimension(:) :: coord,weight

  ! initialize
  coord(:) = -9.99E9
  weight(:) = ZERO

  ! Abramowitz & Stegun Handbook (Table 25.4), p916
  select case (intOrd)
  case(1)  ! one Gauss point (center)
     coord(1) = ZERO
     weight(1) = TWO
  case(2)  ! two points
     coord(1) = -0.577350269189626_DP
     coord(2) = -coord(1)
     weight(1:2) = ONE
  case(3)  ! three points
     coord(1:2) = (/ -0.774596669241483_DP, ZERO /)
     coord(3) = -coord(1)
     weight(1:2) = (/ 0.555555555555556_DP, 0.888888888888889_DP /)
     weight(3) = weight(1)
  case(4)  ! four points
     coord(1:2) = (/ -0.861136311594053_DP, -0.339981043584856_DP /)
     coord(3:4) = -coord(2:1:-1)
     weight(1:2) = (/ 0.347854845137454_DP, 0.652145154862546_DP /)
     weight(3:4) = weight(2:1:-1)
  case(5)  ! five points
     coord(1:3) = (/ -0.906179845938664_DP, -0.538469310105683_DP, &
                   &  ZERO /)
     coord(4:5) = -coord(2:1:-1)
     weight(1:3) = (/ 0.236926885056189_DP, 0.478628670499366_DP, &
                   &  0.568888888888889_DP /)
     weight(4:5) = weight(2:1:-1)
  case(6)  ! six points
     coord(1:3) = (/ -0.932469514203152_DP, -0.661209386466265_DP, &
                   & -0.238619186083197_DP /)
     coord(4:6) = -coord(3:1:-1)
     weight(1:3) = (/ 0.171324492379170_DP, 0.360761573048139_DP, &
                   &  0.467913934572691_DP /)
     weight(4:6) = weight(3:1:-1)
  case(7)
     coord(1:4) = (/ -0.949107912342759_DP, -0.741531185599394_DP, &
                   & -0.405845151377397_DP, ZERO /)
     coord(5:7) = -coord(3:1:-1)
     weight(1:4) = (/ 0.129484966168870_DP, 0.279705391489277_DP, &
                    & 0.381830050505119_DP, 0.417959183673469_DP /)
     weight(5:7) = weight(3:1:-1)
  case(8)
     coord(1:4) = (/ -0.960289856497536_DP, -0.796666477413627_DP, &
                   & -0.525532409916329_DP, -0.183434642495650_DP /)
     coord(5:8) = -coord(4:1:-1)
     weight(1:4) = (/ 0.101228536290376_DP, 0.222381034453374_DP, &
                    & 0.313706645877887_DP, 0.362683783378362_DP /)
     weight(5:8) = weight(4:1:-1)
  case default
     print *, 'unsupported Gaussian Quadrature integration order:', intOrd
     stop
  end select

end subroutine GaussSetup

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subroutine GaussLaguerreSetup(intOrd,coord,weight)
  integer(I4B), intent(in) :: intOrd    ! Gauss-Laguerre integration order
  real(DP), intent(out), dimension(:) :: coord,weight

  ! initialize
  coord(:) = -9.99E9
  weight(:) = ZERO

  ! Gaussian Quadrature Formulas, Stroud & Secrest, 1966. (Table 9), p303
  ! integration from 0->1, with ln(1/r) singularity at 0
  select case (intOrd)
  case(1)
     coord(1) = HALF
     weight(1) = ONE; 
  case(2)
     coord(1:2) = (/ 0.11200880616697618295_DP, 0.60227690811873810275_DP /)
     weight(1:2) = (/ 0.71853931903038444066_DP, 0.28146068096961555933_DP /)
  case(3)
     coord(1:3) = (/ 0.063890793087325404996_DP, 0.36899706371561876554_DP, &
                   & 0.76688030393894145542_DP /)
     weight(1:3) = (/ 0.51340455223236332512_DP, 0.39198004120148455480_DP, &
                    & 0.094615406566149120064_DP /)
  case(4)
     coord(1:4) = (/ 0.041448480199383220803_DP, 0.24527491432060225193_DP, &
                   & 0.55616545356027583718_DP,  0.84898239453298517464_DP /)
     weight(1:4) = (/ 0.38346406814513512485_DP, 0.38687531777476262733_DP, &
                    & 0.19043512695014241536_DP, 0.039225487129959832452_DP  /)
  case(5)
     coord(1:5) = (/ 0.029134472151972053303_DP, 0.17397721332089762870_DP, &
                   & 0.41170252028490204317_DP, 0.67731417458282038070_DP, &
                   & 0.89477136103100828363_DP /)
     weight(1:5) = (/ 0.29789347178289445727_DP, 0.34977622651322418037_DP, &
                    & 0.23448829004405251888_DP, 0.098930459516633146976_DP, &
                    & 0.018911552143195796489_DP /)
  case(6)
     coord(1:6) = (/ 0.021634005844116948995_DP, 0.12958339115495079613_DP, &
                   & 0.31402044991476550879_DP, 0.53865721735180214454_DP, &
                   & 0.75691533737740285216_DP, 0.92266885137212023733_DP /)
     weight(1:6) = (/ 0.23876366257854756972_DP, 0.30828657327394679296_DP, &
                    & 0.24531742656321038598_DP, 0.14200875656647668542_DP, &
                    & 0.055454622324886290015_DP, 0.010168958692932275886_DP /)
  case(7)
     coord(1:7) = (/ 0.016719355408258515941_DP, 0.10018567791567512158_DP, &
                   & 0.24629424620793059904_DP, 0.43346349325703310583_DP, &
                   & 0.63235098804776608846_DP, 0.81111862674040557652_DP, &
                   & 0.94084816674334772176_DP /)
     weight(1:7) = (/ 0.19616938942524820752_DP, 0.27030264424727298214_DP, &
                    & 0.23968187200769094830_DP, 0.16577577481043290656_DP, &
                    & 0.088943227137657964435_DP, 0.033194304356571067025_DP, &
                    & 0.0059327870151259239991_DP /)
  case(8)
     coord(1:8) = (/ 0.013320244160892465012_DP, 0.079750429013894938409_DP, &
                   & 0.19787102932618805379_DP, 0.35415399435190941967_DP, &
                   & 0.52945857523491727770_DP, 0.70181452993909996383_DP, &
                   & 0.84937932044110667604_DP, 0.95332645005635978876_DP /)
     weight(1:8) = (/ 0.16441660472800288683_DP, 0.23752561002330602050_DP, &
                    & 0.22684198443191912636_DP, 0.17575407900607024498_DP, &
                    & 0.11292403024675905185_DP, 0.057872210717782072398_DP, &
                    & 0.020979073742132978043_DP, 0.0036864071040276190133_DP /)
  case default
     print *, 'unsupported Gauss-Laguerre integration order:', intOrd
     stop
  end select
end subroutine GaussLaguerreSetup

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! determine which element shares current node (2D, 2 node elements only)

function nextBC(ee,nn) result(otherBC)
  integer(I4B), intent(in) :: ee,nn ! element and node 
  integer(I4B), dimension(2) :: otherBC
  
  integer(I4B) :: ii,jj

  ! this uses global variables defined in parent (which contains this function);
  ! it is sloppy coding, but it makes the function's arguments much simpler

  findBC: do ii=1,nElement 
     if (ii/=ee) then ! loop over other elements
        do jj=1,nodesPer ! loop over their nodes
           ! find other node with same global index
           if (connectG(jj,ii) == connectL(nn)) then 
              otherBC(1:2) = (/ ii,jj /)
              exit findBC ! done, only one node to find
           end if
        end do
     end if
  end do findBC

end function nextBC

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! basis functions, normals and jacobians

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subroutine normalJacobian(norm,jac,xsi,nodes,coord)
  real(DP), dimension(2), intent(out) :: norm  ! normal to element
  real(DP), intent(out) :: jac         ! jacobian*2 = element length (linear elements)
  real(DP), intent(in) :: xsi          ! arc length where jac & norm are needed 
  ! (same for all xsi for linear elements)
  integer(I4B), intent(in) :: nodes    ! # nodes per element
  real(DP), intent(in) :: coord(:,:)   ! global coords of nodes in element

  real(DP), dimension(size(coord,dim=2)) :: Dbasis
  real(DP), dimension(2) :: tangent 


  ! derivative of basis functions
  select case(nodes)
!!$  case(1)
!!$     Dbasis(1) = ZERO    
  case(2)
     Dbasis(1:2) = (/ -HALF, HALF /)
  case(3)
     Dbasis(1:3) = (/ xsi-HALF, xsi+HALF, -TWO*xsi /)
  case default
     print *, 'number of nodes per element not implemented:', nodes
     stop
  end select
  
  ! sum of deriv of basis * location vector across all nodes
  tangent(1:2) = sum(spread(Dbasis(:),dim=1,ncopies=2)*coord(1:2,:),dim=2)


  ! had to change signs here to make normal point outward for finite domain (CW nodes)
  norm(1) = +tangent(2)
  norm(2) = -tangent(1)

  ! jacobian is length of normal vector
  jac = sqrt(dot_product(norm(:),norm(:)))

  ! normalize normal vector to unit length
  if (jac > 0.0) then
     norm(1:2) = norm(1:2)/jac
  end if

end subroutine normalJacobian

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function basis(xsi, nodes) result(N)
  real(DP), intent(in), dimension(:) :: xsi   ! Gauss pts to calculate basis fcn at
  integer(I4B), intent(in) :: nodes           ! number of nodes (# of basis fcns) 
  real(DP), dimension(nodes,size(xsi)) :: N   ! all the basis fcns at all the pts

  integer(I4B) :: nPts

  ! first dimension of basis is the basis function
  ! second dimension of basis is the gauss pt location

  nPts = size(xsi)
  if(maxval(abs(xsi)) > ONE) print *, 'local coordinate out of range:',xsi

  select case(nodes)
  case(1)
     ! constant elements (not a function of xsi)
     N(1,1:nPts) = ONE
  case(2)
     ! linear elements
     N(1,1:nPts) = HALF*(ONE - xsi(:)) ! node 1 at xsi=-1
     N(2,1:nPts) = HALF*(ONE + xsi(:)) ! node 2 at xsi=+1
  case(3)
     ! quadratic elements
     N(3,1:nPts) = ONE - xsi(:)**2   ! node 3 at xsi=0
     N(1,1:nPts) = HALF*xsi(:)*(xsi(:) - ONE) ! node 1 at xsi=-1
     N(2,1:nPts) = HALF*xsi(:)*(xsi(:) + ONE) ! node 2 at xsi=+1
  case default
     print *, 'unsupported number of element nodes:',nodes
  end select

end function basis

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! Green's function
!##################################################
function u(x1,x2) result(pot)
  real(DP), intent(in), dimension(1:2) :: x1 ! source point
  real(DP), intent(in), dimension(1:2) :: x2 ! field point (gauss pt)
  real(DP) :: pot  ! potential at each point

  ! Green's function for 2D Laplace solution
  pot = log(ONE/sqrt((x2(1)-x1(1))**2 + (x2(2)-x1(2))**2))/(k*TWOPI)

end function u

! X-deriv of Green's function
!##################################################
function Dux(x1,x2) result(Dpotx)
  real(DP), intent(in), dimension(1:2) :: x1 ! source point
  real(DP), intent(in), dimension(1:2) :: x2 ! field point (gauss pt)
  real(DP) :: x,rsq,Dpotx
  
  x = x2(1) - x1(1)
  rsq = (x2(1)-x1(1))**2 + (x2(2)-x1(2))**2

  ! X-deriv of 2D Green's function for Laplace eqn
  Dpotx = -x/(rsq*k*TWOPI)

end function Dux

! Y-deriv of Green's function
!##################################################
function Duy(x1,x2) result(Dpoty)
  real(DP), intent(in), dimension(1:2) :: x1 ! source point
  real(DP), intent(in), dimension(1:2) :: x2 ! field point (gauss pt)
  real(DP) :: y,rsq,Dpoty
  
  y = x2(2) - x1(2)
  rsq = (x2(1)-x1(1))**2 + (x2(2)-x1(2))**2

  ! Y-deriv of 2D Green's function for Laplace eqn
  Dpoty = -y/(rsq*k*TWOPI)

end function Duy

! normal derivative of Green's function (wrt boundary)
!##################################################
function q(x1,x2,n) result(flux)
  real(DP), intent(in), dimension(1:2) :: x1  ! source point
  real(DP), intent(in), dimension(1:2) :: x2 ! field point
  real(DP), intent(in), dimension(1:2) :: n  ! outward normal to bdry at field pt
  real(DP) :: flux  ! deriviative of potential

  real(DP), dimension(1:2) :: r ! vector pointing from source to field point

  r(1:2) = x2(1:2) - x1(1:2)
  
  ! cos(theta)/2*pi*r ; cos(theta) = (n .dot. r)/r
  flux = dot_product(n,r)/(TWOPI*dot_product(r,r))

end function q

! X-deriv of normal derivative of Green's function
!##################################################
function Dqx(x1,x2,n) result(Dfx)
  real(DP), intent(in), dimension(1:2) :: x1  ! source point
  real(DP), intent(in), dimension(1:2) :: x2 ! field point
  real(DP), intent(in), dimension(1:2) :: n  ! outward normal to bdry at field pt
  real(DP) :: rsq, Dfx

  real(DP), dimension(1:2) :: r ! vector pointing from source to field point

  r(1:2) = x2(1:2) - x1(1:2)
  rsq = dot_product(r,r)
  
  Dfx = (dot_product(n,r)*TWO*r(1) - n(1))/(TWOPI*rsq)

end function Dqx

! Y-deriv of normal derivative of Green's function
!##################################################
function Dqy(x1,x2,n) result(Dfy)
  real(DP), intent(in), dimension(1:2) :: x1  ! source point
  real(DP), intent(in), dimension(1:2) :: x2 ! field point
  real(DP), intent(in), dimension(1:2) :: n  ! outward normal to bdry at field pt
  real(DP) :: rsq, Dfy

  real(DP), dimension(1:2) :: r ! vector pointing x2 -> x1

  r(1:2) = x2(1:2) - x1(1:2)
  rsq = dot_product(r,r)
  
  Dfy = (dot_product(n,r)*TWO*r(2) - n(2))/(TWOPI*rsq)

end function Dqy

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! matrix solver (LU decomposition)
! simplified version of NR version
! Press, etal, Numerical Recipes in f90, p1016
!##################################################
subroutine ludcmp(a,indx)
  real(DP), dimension(:,:), intent(inout) :: a
  integer(I4B), dimension(:), intent(out) :: indx
  real(DP), dimension(size(a,dim=1)) :: vv
  real(DP), parameter :: TINY = 1.0E-20_DP
  integer(I4B) :: j,n,imax
  
  n = size(a,dim=1)
  vv = maxval(abs(a),dim=2)
  if (any(vv == 0.0)) print *, 'singular matrix in LUDCMP'
  vv = ONE/vv
  do j=1,n
     imax = (j-1) + sum(maxloc(vv(j:n)*abs(a(j:n,j))))
     if (j /= imax) then
        call swap(a(imax,:),a(j,:))
        vv(imax) = vv(j)
     end if
     indx(j) = imax
     if (a(j,j) == 0.0) a(j,j) = TINY
     a(j+1:n,j) = a(j+1:n,j)/a(j,j)
     a(j+1:n,j+1:n) = a(j+1:n,j+1:n) - outerprod(a(j+1:n,j),a(j,j+1:n))
  end do
end subroutine ludcmp

!##################################################
subroutine lubksb(a,indx,b)
  real(DP), intent(in), dimension(:,:) :: a
  integer(I4B), intent(in), dimension(:) :: indx
  real(DP), intent(inout), dimension(:) :: b
  
  integer(I4B) :: i,n,ii,ll
  real(DP) :: summ

  n = size(a,dim=1)
  ii = 0
  do i=1,n
     ll = indx(i)
     summ = b(ll)
     b(ll) = b(i)
     if (ii /= 0) then
        summ = summ - dot_product(a(i,ii:i-1),b(ii:i-1))
     elseif (summ /= 0.0) then
        ii = i
     end if
     b(i) = summ
  end do
  do i=n,1,-1
     b(i) = (b(i) - dot_product(a(i,i+1:n),b(i+1:n)))/a(i,i)
  end do
end subroutine lubksb

! numerical recipes utility routines
!##################################################
subroutine swap(a,b)
  real(DP), intent(inout), dimension(:) :: a,b
  real(DP), dimension(size(a)) :: tmp

  tmp = a
  a = b
  b = tmp
end subroutine swap

function outerprod(a,b) result(c)
  real(DP), intent(in), dimension(:) :: a,b
  real(DP), dimension(size(a),size(b)) :: c

  c = spread(a,dim=2,ncopies=size(b))* &
    & spread(b,dim=1,ncopies=size(a))
end function outerprod

end program laplace_biem_main
