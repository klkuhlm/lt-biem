
program Laguerre_Test
  use function, only : fLap,fTime
  implicit none

!!!! constants
  character(2) :: num
  character(13) :: fmt = '(ES14.6,1X))'
  integer, parameter :: DP = 8, NUMT = 15, NFUN = 6
  real(DP), parameter :: PI =  3.141592653589793238462643383279503_DP
  complex(DP), parameter :: EYE = (0.0_DP, 1.0_DP)

!!!! adjustable parameters
  integer, parameter :: N = 16 ! number of terms in Laguerre series 
  integer, parameter :: M = N  ! 2M terms in fourier-series expansion for a_n
  real(DP), parameter :: sig0 = 0.0_DP  ! abcissa of convergence
  real(DP) :: sig  ! determined for each value of t?? -> sig=max(0,sig0+1/t)
  real(DP) :: b    ! Weeks parameter ->                    b=N/(2t)

  !! to make switching ranges easier
  integer, parameter :: MLO = 0, MHI = M-1

!!!! internal / intermediate variables
  real(DP), dimension(MLO:MHI) :: theta, theta2 ! theta_m and theta_{m+1/2}
  complex(DP), dimension(MLO:MHI) :: p, fp
  real(DP),dimension(NUMT) :: t    ! times to obtain inverse solution at
  complex(DP),dimension(1:N+1) :: y  ! from 0:N-1 + 2 terms for padding
  real(DP),dimension(NFUN) :: stdSoln
  real(DP) :: x  ! argument to L()
  integer :: i,j,k ! counters

  ! vector of times to calculate inversion at (geometric series)
  t(1:NUMT) = 10.0_DP**(/(3.0_DP*i/real(NUMT,DP)-1.0_DP , i=0,NUMT-1)/)

  ! write results to file
  open(unit=20,file='laguerre.dat',status='replace',action='write') 
  write(20,*) '# t:',t

  ! -pi < theta < pi
  theta(MLO:MHI) = real((/(j, j=MLO,MHI)/),DP)*PI/real(M,DP)
  theta2(MLO:MHI) = (/(real(j,DP)+0.5_DP, j=MLO,MHI)/)*PI/real(M,DP)
  write(20,*) '# theta:',theta
  write(20,*) '# theta + 1/2:',theta2

  ! results are in columns
  write(20,*) '# time, actual (F1), actual-inverted (F1), actual (F2), actual-inverted (F2), ...'
  write(num,'(I2.2)') NFUN*2+1  ! format string multiplier

  do i=1,NUMT   ! loop over times

     sig = max(0.0_DP, sig0 + 1.0_DP/t(i))  ! weeks original rule of thumb
     b = real(N,DP)/(0.5_DP*t(i))           ! also weeks original
!!$     b = 5.0_DP
     p(MLO:MHI) = sig - b*(exp(EYE*theta2(MLO:MHI))+1.0_DP)/(exp(EYE*theta2(MLO:MHI))-1.0_DP)
     x = 2.0_DP*b*t(i)  ! argument to L()
     write(20,*) '# sigma_0:',sig0,' sigma:', sig, ' b:',b, ' x:',x
!!$     write(20,*) '# p:',p
     
     ! use Clenshaw recurrence for calculating summation
     ! to avoid cancellation from summing Laguerre polynomials directly

     do j=1,NFUN
        y(N:N+1) = (0.0_DP, 0.0_DP)
        do k=MLO,MHI
           fp(k) = fLap(p(k),j)
        end do
        do k = N-1,1,-1 ! descending recurrence
           y(k) = alpha(k,x)*y(k+1) + beta(k+1)*y(k+2) + &
                & coeff(k,M,theta(MLO:MHI),theta2(MLO:MHI),fp(MLO:MHI),b)
        end do
        ! only use real portion of solution
        ! L(0,x) == 1, L(1,x) == (1-x)
        stdSoln(j) = exp((sig-b)*t(i))*real(beta(1)*y(2) + (1.0_DP - x)*y(1) + &
             & coeff(0,M,theta(MLO:MHI),theta2(MLO:MHI),fp(MLO:MHI),b))
     end do
     
     ! each function has two columns (inverted and actual-inverted)
     write(20,'('//num//fmt) t(i), (ftime(t(i),j), ftime(t(i),j)-stdSoln(j), j=1,NFUN)
  end do
contains

!! alpha and beta are the coefficients in the Laguerre polynomial
!! recurrence relations, which is used by the clenshaw algorithm
real(DP) function alpha(nn,xx)
  integer, intent(in) :: nn
  real(DP), intent(in) :: xx

  alpha = (real(2*nn+1)-xx)/real(nn+1,DP)
end function alpha

real(DP) function beta(nn)
  integer, intent(in) :: nn

  beta = real(-nn,DP)/real(nn+1,DP)
end function beta

!! use midpoint rule for calculating a_n coefficients
function coeff(kk,NN,th,th2,fp,b) result(a)
  integer, intent(in) :: kk  ! order (n)
  integer, intent(in) :: NN  ! M in Kano implementation
  real(DP), dimension(MLO:MHI), intent(in) :: th,th2  ! theta_m and theta_{m+1/2}
  complex(DP), intent(in), dimension(MLO:MHI) :: fp ! image function values
  real(DP), intent(in) :: b  ! Weeks parameter
  complex(DP) :: a
  integer :: mult

  ! if only evaluating 0:M-1, then take 2*soln, using symmetry of fp
  if(MLO == -M) then
     mult = 2  ! integrate all the way around unit circle
  elseif(MLO == 0) then
     mult = 1  ! only integrate half of circle, Im(p) >= 0
  end if
  
  a = exp(-EYE*kk*PI/real(2*NN,DP))/real(mult*NN,DP)*&
       & sum(exp(-EYE*kk*th(MLO:MHI))*2.0_DP*b*fp(MLO:MHI)/(1.0_DP-exp(EYE*th2(MLO:MHI))))

end function coeff

end program Laguerre_Test
