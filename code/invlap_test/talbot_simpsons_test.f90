!! written by Kris Kuhlman, October 2006 (should work with any f90 compiler)
!! this is a self-contained "fixed talbot" implementation, with a simple testing framework
!! more functions can be added to fLap and fTime below, then increase the constant in the 
!! main program to match the number of functions.

!! The implementation follows that given in J. Abate and P. Valko, 2004.
!! it could be improved by accelerating the trapezoid rule used for the
!! integration.

module function
  implicit none

  integer, parameter :: DP = 8
  real(DP), parameter :: PI =  3.141592653589793238462643383279503_DP

contains

  ! return value of function in laplace space, given
  ! laplace parameter and function index
  function fLap(p,idx) result(fbar)
    complex(DP), intent(in) :: p
    complex(DP) :: fbar
    integer, intent(in) :: idx

    select case(idx)
    case(1)
       ! constant
       fbar = 1.0D0/p
    case(2)
       ! monomial
       fbar = 1.0D0/(p**2)
    case(3)
       ! 1/sqrt(pi*t)
       fbar = 1.0D0/sqrt(p)
    case(4)
       ! cos(t)
       fbar = p/(p**2 + 1.0_DP)
    case(5)
       ! erfc(1/(2sqrt(t)))
       fbar = exp(-sqrt(p))/p
    case(6)
       ! cosh(t)
       fbar = p/(p**2 - 1.0_DP)
    case default
       stop 'unimplemented function index'
    end select
  end function fLap
  

  ! return value of function in time domain, given
  ! time and function index
  function fTime(t,idx) result(ft)
    real(DP), intent(in) :: t
    real(DP) :: ft
    integer, intent(in) :: idx

    select case(idx)
    case(1)
       ! constant
       ft = 1.0D0
    case(2)
       ! monomial
       ft = t
    case(3)
       ! 1/sqrt(pi*t)
       ft = 1.0D0/sqrt(PI*t)
    case(4)
       ! cos(t)
       ft = cos(t)
    case(5)
       ft = derfc(1.0_DP/(2.0_DP*sqrt(t)))
    case(6)
       ft = cosh(t)
    case default
       stop 'unimplemented function index'
    end select
  end function fTime
  
end module function

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

program talbot_Test
  use function, only : fLap,fTime
  implicit none

  !! talbot method invlap stuff
  character(2) :: n
  integer, parameter :: DP = 8, NUMT = 20, NFUN = 6
  real(DP), parameter :: PI =  3.141592653589793238462643383279503_DP
  integer, parameter :: M = 10    ! number of intervals in Simpson's rule
  real(DP) :: r                   ! parameter determining shape of parabola
  real(DP), dimension(2*M-1) :: theta  ! p(theta)    
  complex(DP), dimension(0:2*M-1) :: p
  real(DP),dimension(NUMT) :: t       ! times to obtain inverse solution at
  real(DP),dimension(0:2*M-1,NFUN) :: ft  ! inverted solution (three functions)
  real(DP),dimension(NFUN) :: stdSoln
  real(DP),dimension(0:2*M-1) :: fact 
  real(DP), dimension(1:2*M-1) :: sigma   ! intermediate term
  integer :: i,j,k ! counters

  ! vector of times to calculate inversion at (geometric series)
  t(1:NUMT) = 10.0_DP**(/(3.0_DP*i/real(NUMT,DP)-1.0_DP , i=0,NUMT-1)/)
  write(n,'(i2.2)') M

  ! write results to file
  open(unit=20,file='talbot_simp_'//trim(n)//'.dat',status='replace',action='write') 

  write(n,'(i2.2)') 2*NFUN+1

  ! 0 < theta < pi
  theta(1:2*M-1) = (/(j, j=1,2*M-1)/)*PI/real(2*M,DP)

!!$  do i=1,2*M-1
!!$     write(20,*) 't:',i,theta(i)
!!$  end do
  
  ! results are in columns
  write(20,*) '#time,  actual soln, stdSoln, acceleratedSoln, ...'

  do i=1,NUMT   ! loop over times

     ! calculate r parameter
     r = real(2*M,DP)/(5.0_DP*t(i))   ! Abate & Valko rule (eqn 19, p 985)

     p(0) = cmplx(r,0.0_DP,DP)
     p(1:2*M-1) = r*theta(1:2*M-1)*(1.0_DP/tan(theta(1:2*M-1)) + (0.0_DP, 1.0_DP))

     do k=1,NFUN
        ft(0,k) = real(exp(p(0)*t(i))*fLap(p(0),k))
     end do

     ! calculate all the sigmas at once
     sigma(1:2*M-1) = theta(:) + (theta(:)/tan(theta(:)) - 1.0_DP)/tan(theta(:))
     
     do j=1,2*M-1
        do k=1,NFUN
           ft(j,k) = real(exp(t(i)*p(j))*fLap(p(j),k)*(1.0_DP + (0.0_DP,1.0_DP)*sigma(j)))
        end do
     end do

     fact(0) = r/real(6*M,DP)
     fact(1:2*M-1:2) = 4.0_DP*r/real(6*M,DP)
     fact(2:2*M-1:2) = 2.0_DP*r/real(6*M,DP)

!!$     if(i==1) then
!!$        print *, 'r',r
!!$        print *, 'M',M
!!$        print *, 'fact',fact
!!$     end if

     ! sum terms in Simpson's rule
     stdSoln(1:NFUN) = sum(ft(0:2*M-1,1:NFUN)*spread(fact(0:2*M-1),dim=2,ncopies=NFUN),dim=1)

     ! each function has two columns (inverted and actual-inverted)
     write(20,'('//n//'(ES22.14,1X))') t(i), (ftime(t(i),k), ftime(t(i),k)-stdSoln(k), k=1,NFUN)

  end do
  
!!444 format(n(ES22.14,1X))
end program talbot_Test
