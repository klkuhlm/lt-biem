module function
  implicit none
contains
  elemental function f(p) result(fp)
    complex(8), intent(in) :: p
    complex(8) :: fp

!!$    ! constant
!!$    fp = 1.0D0/p
!!$
!!$    ! monomial
!!$    fp = 1.0D0/(p*p)
!!$
    ! 1/sqrt(pi*t)
    fp = 1.0D0/sqrt(p)

  end function f
  
end module function


program stehfest_test
  use stehfest_coeff, only : coeff
  use constants, only : DP, PI, LN2
  use function, only : f
  implicit none

  integer, parameter :: N = 16, NUMT = 20
  complex(DP), dimension(N) :: p
  real(DP),dimension(NUMT) :: t, ans
  real(DP),dimension(N) :: ft, gstV
  integer :: i

  t(1:NUMT) = 10.0_DP**(/(3.0_DP*i/real(NUMT,DP)-2.0_DP , i=0,NUMT-1)/)

  open(unit=20,file='stehfest.dat',status='replace',action='write') 

  gstV(1:N) = coeff(N)
  write(20,'(A,I2)') 'N:',N
  write(20,*) 'V(N):',gstV


  do i=1,NUMT
     write(20,*) 'i:',i
     p(1:N) = cmplx(LN2*(/ (i, i=1,N) /)/t(i),0.0_DP,DP)
     write(20,*) 'p:',real(p)
     ans(i) = LN2/t(i)*sum(gstV(1:N)*real(f(p(1:N))))
     write(20,*) t(i), ans(i), ans(i)-1.0D0/sqrt(PI*t(i))
  end do
  

end program stehfest_test
