module function
  implicit none

  integer, parameter :: DP = 8
  real(DP), parameter :: PI =  3.141592653589793238462643383279503_DP

contains

  ! return value of function in laplace space, given
  ! laplace parameter and function index
  elemental function fLap(p,idx) result(fbar)
    complex(DP), intent(in) :: p
    complex(DP) :: fbar
    integer, intent(in) :: idx

    select case(idx)
    case(1)
       ! constant
       fbar = 1.0D0/p
    case(2)
       ! monomial
       fbar = 1.0D0/(p**2)
    case(3)
       ! 1/sqrt(pi*t)
       fbar = 1.0D0/sqrt(p)
    case(4)
       ! cos(t)
       fbar = p/(p**2 + 1.0_DP)
    case(5)
       ! erfc(1/(2sqrt(t)))
       fbar = exp(-sqrt(p))/p
    case(6)
       ! cosh(t)
       fbar = p/(p**2 - 1.0_DP)
    case default
       ! unimplemented index flag
       fbar = (-9.999D+99, -9.999D+99)
    end select
  end function fLap
  

  ! return value of function in time domain, given
  ! time and function index
  elemental function fTime(t,idx) result(ft)
    real(DP), intent(in) :: t
    real(DP) :: ft
    integer, intent(in) :: idx

    select case(idx)
    case(1)
       ! constant
       ft = 1.0D0
    case(2)
       ! monomial
       ft = t
    case(3)
       ! 1/sqrt(pi*t)
       ft = 1.0D0/sqrt(PI*t)
    case(4)
       ft = cos(t)
    case(5)
       ft = derfc(1.0_DP/(2.0_DP*sqrt(t)))
    case(6)
       ft = cosh(t)
    case default
       ft = -9.999D+99
    end select
  end function fTime
  
end module function
