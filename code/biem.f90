program biem_main

  use stehfest_coeff, only : coeff

implicit none

integer, parameter :: DP = selected_real_kind(p=15,r=300), MAXGINT = 8, MAXGLINT = 8
integer, parameter :: I4B = selected_int_kind(r=8)
real(DP), parameter :: PI = 3.141592653589793238462643383279503_DP
real(DP), parameter :: LN2 = 0.693147180559945309417232121458177_DP
real(DP), parameter :: ZERO = 0.0_DP, HALF = 0.5_DP, ONE = 1.0_DP, TWO = 2.0_DP

! element coordinates and connectivity (G = global, L = local)
real(DP), allocatable :: coordG(:,:), coordL(:,:), bc(:,:), sol_u(:,:,:), sol_t(:,:,:)
real(DP), allocatable :: obsLoc(:,:), obsRes(:,:), results(:), stehfestV(:), arg(:)
integer(I4B), allocatable :: connectG(:,:), connectL(:), type(:), indx(:)
real(DP), dimension(MAXGINT) :: GaussCoord, GaussWeight
real(DP), dimension(MAXGLINT) :: GLCoord, GLWeight, xsi2

!!$! matricies of numerical integration results 
!! Locally-indexed matricies of boundary integrals => H=flux, G=potential
!! Globally-indexed matrix of unknown boundary values => A
!! globally-indexed vector of known/unknown boundary quantities => b, x
real(DP), allocatable :: G(:,:,:), H(:,:,:), A(:,:), b(:), x(:)

integer(I4B) :: nElement, nodesPer, maxNode, intOrd, nObsPts, &
     & ii, i, j, e, n ,m, otherBC(2), stehfestN
real(DP) :: Jac, Jac2, int1, int2, int3, t
real(DP), allocatable :: GN(:,:), GLN(:,:)
real(DP), dimension(2) :: norm, field

open(unit=20, file='input.dat', status='old', action='read')
open(unit=30, file='output.dat', status='replace', action='write')

! read input from file, echo to output file for debugging
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
read(20,*) nElement, nodesPer, intOrd, nObsPts, t, stehfestN
write(30,*) 'number of elements= ',nElement
write(30,*) 'number of nodes per element= ',nodesPer
write(30,*) 'integration order= ',intOrd
write(30,*) 'number of calcualtion points= ', nObsPts
write(30,*) 'value of t for inversion= ',t
write(30,*) 'order of Stehfest inversion= ',stehfestN

allocate(connectG(nodesPer,nElement),GN(nodesPer,MAXGINT),GLN(nodesPer,MAXGLINT),&
     & type(nElement), bc(nElement,nodesPer),stehfestV(1:stehfestN),arg(1:stehfestN))

do i=1,nElement
   ! loop over elements, reading in global index, bc type, and bc values
   read(20,*) connectG(1:nodesPer,i), type(i), bc(i,1:nodesPer)
   write(30,*) 'elem:',i,' node:',connectG(1:nodesPer,i), &
        & ' bc:',type(i),' bc_val:',bc(i,1:nodesPer)
end do

maxNode = maxval(connectG(:,:))

allocate(coordG(2,maxNode), connectL(nodesPer), coordL(2,nodesPer),obsLoc(2,nObsPts))

do i=1,maxNode
   ! loop over nodes, reading in global coordinates (x,y)
   ! type = 1 : Dirichlet
   ! type = 2 : Neumann
   ! type = 3 : mixed (not yet)
   read(20,*) coordG(1:2,i)
end do

! echo input for debugging
do i=1,maxNode
   write(30,'(A5,I3,A7,2(1X,F7.3))') 'node:',i,' (x,y):',coordG(:,i)
end do

do i=1,nObsPts
   read(20,*) obsLoc(:,i)
end do

! echo input for debugging
do i=1,nObsPts
   write(30,'(A8,I3,A7,2(1X,F7.3))') 'calc_pt:',i,' (x,y):',obsLoc(:,i)
end do

! matricies which hold numerical integration results
allocate(H(maxNode,nElement,nodesPer),G(maxNode,nElement,nodesPer),&
     & A(maxNode,maxNode),b(maxNode),x(maxNode),indx(maxNode))

call GaussSetup(intOrd,GaussCoord,GaussWeight)  ! assume same intOrd for each node
!!$write(30,*) 'std Gauss quadrature pts:',GaussCoord(1:intOrd)
call GaussLaguerreSetup(MAXGLINT,GLCoord,GLWeight)  
! integrate approximate polynomial at equal order (12)
!!$write(30,*) 'Gauss-Laguerre quadrature pts:', GLCoord(1:intOrd)

! solve BEM for each value of the laplace parameter
arg(1:stehfestN) = log(TWO)*(/ (i, i=1,stehfestN) /)/t
do ii = 1,stehfestN 

   H = ZERO; G = ZERO

   ! calculate off-diagonal entries of matricies 
   !############################################################

   ! calculate basis functions for all nodes & gauss pts
   ! (same for all off-diagonal elements)
   GN(1:nodesPer,1:intOrd) = basis(GaussCoord(1:intOrd),nodesPer)
!!$do i=1,nodesPer
!!$   write(30,*) 'basis fcns at std Gauss pts:',i,GN(i,1:intOrd)
!!$end do
   do i=1,maxnode  ! collocation point (source pt)
      do e=1,nElement  ! elements (field points)
         connectL(1:nodesPer) = connectG(1:nodesPer,e)
         coordL(1:2,1:nodesPer) = coordG(1:2,connectL(1:nodesPer))

         do n=1,nodesPer ! local numbering of nodes

            ! only if colocation pt is at different node 
            ! non-singular integrals for off-diagonal terms
            !$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
            if(i /= connectL(n)) then

               ! for linear elements (2 nodes) jacobian
               ! and normal are constant along element
               call normalJacobian(norm(1:2),Jac,GaussCoord(1),nodesPer,&
                    &coordL(1:2,1:nodesPer))
!!$            write(30,*) 'std src:',i,' element:',e,' node:',n,&
!!$                 &' norm:',norm,' jacobian:',Jac

               do m=1,intOrd
                  ! calculate location of Gauss point in global coordinates
                  field(1:2) = sum(spread(GN(1:nodesPer,m),dim=1,ncopies=2)* &
                       & coordL(1:2,1:nodesPer),dim=2)

                  ! potential integral
                  G(i,e,n) = G(i,e,n) + GN(n,m)*&
                       & u(coordG(1:2,i),field(1:2),arg(ii))*Jac*GaussWeight(m)

                  ! flux integral
                  H(i,e,n) = H(i,e,n) + GN(n,m)*&
                       &q(coordG(1:2,i),field(1:2),norm(1:2),arg(ii))*Jac*GaussWeight(m)
               end do
!!$               write(30,*) 'G:',G(i,e,n),'H:',H(i,e,n)

               ! calculate diagonal entries (singular integrals)
               !$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
            elseif(i == connectL(n)) then ! collocation point coincides with this node
               H(i,e,n) = ZERO ! zero flux normal to element, due to symmetry

               call normalJacobian(norm(1:2),Jac,GaussCoord(1),nodesPer,&
                    & coordL(1:2,1:nodesPer))
!!$            write(30,*) 'sng src:',i,' element:',e,' node:',n,' jacobian:',Jac

               ! does element "length" fit inside polynomial approx entirely?
               ! element has length 2 in Gaussian Quadrature coords, *Jac
               ! to convert it to its 'actual' length
               if (arg(ii)*Jac < ONE) then

                  int2 = ZERO; int3 = ZERO

                  ! non-singular portions of the integral (use std Gauss Quadrature)
                  do m=1,intOrd
                     int2 = int2 + GN(n,m)* &
                          & P2(arg(ii)*GaussCoord(m)*Jac)* &
                          & log(TWO/(Jac*arg(ii)))*Jac*GaussWeight(m)
                     int3 = int3 + GN(n,m)* &
                          & P1(arg(ii)*GaussCoord(m)*Jac)* &
                          & Jac*GaussWeight(m)
                  end do

                  int1 = ZERO
                  ! Jacobian2 is from Gaussian Quad coords, use G-L Quad coords

                  Jac2 = TWO
                  if(n == 1) then
                     xsi2(1:intOrd) = TWO*GLCoord(1:intOrd)-ONE
                  elseif(n == 2) then
                     xsi2(1:intOrd) = ONE - TWO*GLCoord(1:intOrd)
                  else
                     print *, 'unsupported number of nodes per element',n
                     stop
                  end if

                  GLN(1:nodesPer,1:intOrd) = basis(xsi2(1:intOrd),nodesPer)

                  call normalJacobian(norm(:),Jac,xsi2(1),nodesPer,coordL(1:2,1:nodesPer))

                  ! this integration is with respect to xsi2
                  do m=1,intOrd
                     int1 = int1 + GLN(n,m)* &
                          & P2(Jac*arg(ii)*xsi2(m))*Jac2*Jac*GLWeight(m)
                  end do

               ! use normal Gaussian quadrature of K0 outside the singular region,
               ! plus the integral of the singular portion
               elseif(arg(ii)*Jac >= ONE) then

                  int2 = ZERO
                  do m=1,intOrd
                     int2 = int2 + GN(n,m)*P1(Jac*arg(ii)*GaussCoord(m))*Jac*GaussWeight(m)
                  end do

                  int1 = ZERO
                  ! Jacobian2 is from Gaussian Quad coords -> G-L Quad coords

                  Jac2 = TWO
                  if(n == 1) then
                     xsi2(1:intOrd) = TWO*GLCoord(1:intOrd)-ONE
                  elseif(n == 2) then
                     xsi2(1:intOrd) = ONE - TWO*GLCoord(1:intOrd)
                  else
                     print *, 'unsupported number of nodes per element',n
                     stop
                  end if

                  GLN(1:nodesPer,1:intOrd) = basis(xsi2(1:intOrd),nodesPer)

                  call normalJacobian(norm(:),Jac,xsi2(1),nodesPer,coordL(1:2,1:nodesPer))

                  ! this integration is with respect to xsi2
                  do m=1,intOrd
                     int1 = int1 + GLN(n,m)*Jac2*Jac*GLWeight(m)
                  end do
               end if

               G(i,e,n) = (int1 + int2 + int3)/(TWO*PI)
            end if
         end do
      end do
   end do

   ! assemble global LHS and RHS from locally indexed matricies and specified BC
   !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   A(:,:) = ZERO ! lhs
   b(:) = ZERO   ! rhs

   do e=1,nElement
      connectL(1:nodesPer) = connectG(1:nodesPer,e)
      do n=1,nodesPer
         select case(type(e))
         case(2) ! Neumann bc
            ! set rhs: (specified normal flux)*(computed head integral)
            b(1:maxNode) = b(1:maxNode) + G(1:maxNode,e,n)*bc(e,n)

            ! moved this code to a function below
            otherBC(1:2) = nextBC(e,n)

            select case(type(otherBC(1)))
            case(2) ! adjoining element is Neumann bc
               ! set lhs: computed flux integral
               A(1:maxNode,connectL(n)) = A(1:maxNode,connectL(n)) + &
                    & H(1:maxNode,e,n)
            case(1) ! adjoining element is Dirichlet bc
               ! set lhs: (-1)*(computed flux integral)*(specified head)
               A(1:maxNode,connectL(n)) = A(1:maxNode,connectL(n)) - &
                    & H(1:maxNode,e,n)*bc(otherBC(1),otherBC(2))
            end select
         case(1) ! Dirichlet bc
            ! set lhs: (-1)*(computed head integral)
            A(1:maxNode,connectL(n)) = A(1:maxNode,connectL(n)) - &
                 & G(1:maxNode,e,n)
            ! set rhs: (-1)*(computed flux integral)*(specified head)
            b(1:maxNode) = b(1:maxNode) - H(1:maxNode,e,n)*bc(e,n)
         case default
            print *, 'only type I or II bc currently handled ',bc(e,n)
            stop
         end select
      end do
   end do

   ! solve matrix problem using LU decomposition
   !########################################################################

!!$write(30,*) 'assembled LHS matrix:',A
!!$write(30,*) 'assembled RHS vector:',b

   call ludcmp(A,indx)
   x = b
   call lubksb(A,indx,x)

!!$write(30,*) 'computed unknowns on boundary:',x

   ! build up vectors of head and flux (specified and calculated) for post-processing
   !########################################################################
   if (.not. allocated(sol_u)) then
      allocate(sol_u(nElement,nodesPer,stehfestN+1),sol_t(nElement,nodesPer,stehfestN+1), &
           & obsRes(nObsPts,stehfestN))
   end if

   do e=1,nElement
      connectL(1:nodesPer) = connectG(1:nodesPer,e)
      do n=1,nodesPer
         select case(type(e))
         case(2)
            ! Neumann BC - specified flux
            sol_t(e,n,ii) = bc(e,n)

            otherBC(1:2) = nextBC(e,n)

            select case(type(otherBC(1)))
            case(2)
               ! adjoining element is also Neumann
               ! therefore head was calculated
               sol_u(e,n,ii) = x(connectL(n))
            case(1)
               ! adjoining element is Dirichlet
               ! therefore head was specified
               sol_u(e,n,ii) = bc(otherBC(1),otherBC(2))
            end select
         case(1)
            ! Dirichlet BC - specified head, computed flux
            sol_u(e,n,ii) = bc(e,n)
            sol_t(e,n,ii) = x(connectL(n))
         end select
      end do
   end do

   ! write boundary results to output file
   !########################################################################
   write(30,*) 'specified/calculated head at each element and node &
        & (element, node, (x,y), head, flux)'
   write(30,*) 'for value of laplace parameter',ii
   do e=1,nElement
      connectL(1:nodesPer) = connectG(1:nodesPer,e)
      coordL(1:2,1:nodesPer) = coordG(1:2,connectL(1:nodesPer))
      do n=1,nodesPer
         write(30,'(2(1X,I3),4(1X,ES11.4))') &
              & e,n,coordL(1:2,n),sol_u(e,n,ii),sol_t(e,n,ii)
      end do
   end do

   ! calculate potential at points inside domain
   !########################################################################

   obsRes(:,ii) = ZERO

   do i=1,nObsPts
      do e=1,nElement
         connectL(1:nodesPer) = connectG(1:nodesPer,e)
         coordL(1:2,1:nodesPer) = coordG(1:2,connectL(1:nodesPer))
         do n=1,nodesPer
            call normalJacobian(norm(1:2),Jac,GaussCoord(1),nodesPer,&
                 & coordL(1:2,1:nodesPer))
            int1 = ZERO; int2 = ZERO
            do m=1,intOrd
               ! calculate location of Gauss point in global coordinates
               field(1:2) = sum(spread(GN(1:nodesPer,m),dim=1,ncopies=2)* &
                    & coordL(1:2,1:nodesPer),dim=2)
               int2 = int2 + &
                    & GN(n,m)*u(obsLoc(1:2,i),field(1:2),arg(ii))*Jac*GaussWeight(m)
               int1 = int1 + &
                    & GN(n,m)*q(obsLoc(1:2,i),field(1:2),norm(1:2),arg(ii))*Jac*GaussWeight(m)
            end do
            obsRes(i,ii) = obsRes(i,ii) - int1*sol_u(e,n,ii) + int2*sol_t(e,n,ii)
         end do
      end do
   end do

end do ! end of loop over values of Laplace parameter

allocate(results(1:nObspts))
stehfestV(1:stehfestN) = coeff(stehfestN)

! do inverse laplace transform
results(1:nObsPts) = sum(spread(stehfestV(1:stehfestN),dim=1,ncopies=nObsPts)* &
     & obsRes(1:nObsPts,1:stehfestN),dim=2)

! print out results in time domain
write(30,*) 'head results inside the domain (x,y), head'
do i=1,nObsPts
   write(30,'(3(1X,ES12.4))') obsLoc(1:2,i),results(i) 
end do

! do inverse laplace transform for boundary results
sol_u(:,:,stehfestN+1)= sum(spread(spread(&
     & stehfestV(1:stehfestN),dim=1,ncopies=nElement),dim=2,ncopies=nodesPer)* &
     & sol_u(:,:,1:stehfestN),dim=3)

sol_t(:,:,stehfestN+1)= sum(spread(spread(&
     & stehfestV(1:stehfestN),dim=1,ncopies=nElement),dim=2,ncopies=nodesPer)* &
     & sol_t(:,:,1:stehfestN),dim=3)


! print out results on boundary in time domain
write(30,*) 'head results for boundary of domain: (x,y), head, normal flux'
do e=1,nElement
   connectL(1:nodesPer) = connectG(1:nodesPer,e)
   coordL(1:2,1:nodesPer) = coordG(1:2,connectL(1:nodesPer))
   ! just print head for first node of each element
   write(30,'(4(1X,F9.5))') coordL(1:2,1),sol_u(e,1,stehfestN+1),sol_t(e,1,stehfestN+1)
end do


contains
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subroutine GaussSetup(intOrd,coord,weight)
  integer(I4B), intent(in) :: intOrd    ! Gauss integration order
  real(DP), intent(out), dimension(:) :: coord,weight

  ! initialize
  coord(:) = -9.99E9
  weight(:) = ZERO

  ! Abramowitz & Stegun Handbook (Table 25.4), p916
  select case (intOrd)
  case(1)  ! one Gauss point (center)
     coord(1) = ZERO
     weight(1) = TWO
  case(2)  ! two points
     coord(1) = -0.577350269189626_DP
     coord(2) = -coord(1)
     weight(1:2) = ONE
  case(3)  ! three points
     coord(1:2) = (/ -0.774596669241483_DP, ZERO /)
     coord(3) = -coord(1)
     weight(1:2) = (/ 0.555555555555556_DP, 0.888888888888889_DP /)
     weight(3) = weight(1)
  case(4)  ! four points
     coord(1:2) = (/ -0.861136311594053_DP, -0.339981043584856_DP /)
     coord(3:4) = -coord(2:1:-1)
     weight(1:2) = (/ 0.347854845137454_DP, 0.652145154862546_DP /)
     weight(3:4) = weight(2:1:-1)
  case(5)  ! five points
     coord(1:3) = (/ -0.906179845938664_DP, -0.538469310105683_DP, &
                   &  ZERO /)
     coord(4:5) = -coord(2:1:-1)
     weight(1:3) = (/ 0.236926885056189_DP, 0.478628670499366_DP, &
                   &  0.568888888888889_DP /)
     weight(4:5) = weight(2:1:-1)
  case(6)  ! six points
     coord(1:3) = (/ -0.932469514203152_DP, -0.661209386466265_DP, &
                   & -0.238619186083197_DP /)
     coord(4:6) = -coord(3:1:-1)
     weight(1:3) = (/ 0.171324492379170_DP, 0.360761573048139_DP, &
                   &  0.467913934572691_DP /)
     weight(4:6) = weight(3:1:-1)
  case(7)
     coord(1:4) = (/ -0.949107912342759_DP, -0.741531185599394_DP, &
                   & -0.405845151377397_DP, ZERO /)
     coord(5:7) = -coord(3:1:-1)
     weight(1:4) = (/ 0.129484966168870_DP, 0.279705391489277_DP, &
                    & 0.381830050505119_DP, 0.417959183673469_DP /)
     weight(5:7) = weight(3:1:-1)
  case(8)
     coord(1:4) = (/ -0.960289856497536_DP, -0.796666477413627_DP, &
                   & -0.525532409916329_DP, -0.183434642495650_DP /)
     coord(5:8) = -coord(4:1:-1)
     weight(1:4) = (/ 0.101228536290376_DP, 0.222381034453374_DP, &
                    & 0.313706645877887_DP, 0.362683783378362_DP /)
     weight(5:8) = weight(4:1:-1)
  case(12) ! special case for 12th degree polynomial approx to K0
     coord(1:6) = (/ -0.981560634246719_DP, -0.904117256370475_DP, &
                   & -0.769902674194305_DP, -0.587317954286617_DP, &
                   & -0.367831498998180_DP, -0.125233408511469_DP /)
     coord(7:12) = -coord(6:1:-1)
     weight(1:6) = (/ 0.047175336386512_DP, 0.106939325995318_DP, &
                    & 0.160078328543346_DP, 0.203167426723066_DP, &
                    & 0.233492536538355_DP, 0.249147045813403_DP /)
     weight(7:12) = weight(6:1:-1)
  case default
     print *, 'unsupported Gaussian Quadrature integration order:', intOrd
     stop
  end select

end subroutine GaussSetup

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subroutine GaussLaguerreSetup(intOrd,coord,weight)
  integer(I4B), intent(in) :: intOrd    ! Gauss-Laguerre integration order
  real(DP), intent(out), dimension(:) :: coord,weight

  ! initialize
  coord(:) = -9.99E9
  weight(:) = ZERO

  ! Gaussian Quadrature Formulas, Stroud & Secrest, 1966. (Table 9), p303
  ! integration from 0->1, with ln(1/r) singularity at 0
  select case (intOrd)
  case(1)
     coord(1) = HALF
     weight(1) = ONE; 
  case(2)
     coord(1:2) = (/ 0.11200880616697618295_DP, 0.60227690811873810275_DP /)
     weight(1:2) = (/ 0.71853931903038444066_DP, 0.28146068096961555933_DP /)
  case(3)
     coord(1:3) = (/ 0.063890793087325404996_DP, 0.36899706371561876554_DP, &
                   & 0.76688030393894145542_DP /)
     weight(1:3) = (/ 0.51340455223236332512_DP, 0.39198004120148455480_DP, &
                    & 0.094615406566149120064_DP /)
  case(4)
     coord(1:4) = (/ 0.041448480199383220803_DP, 0.24527491432060225193_DP, &
                   & 0.55616545356027583718_DP,  0.84898239453298517464_DP /)
     weight(1:4) = (/ 0.38346406814513512485_DP, 0.38687531777476262733_DP, &
                    & 0.19043512695014241536_DP, 0.039225487129959832452_DP  /)
  case(5)
     coord(1:5) = (/ 0.029134472151972053303_DP, 0.17397721332089762870_DP, &
                   & 0.41170252028490204317_DP, 0.67731417458282038070_DP, &
                   & 0.89477136103100828363_DP /)
     weight(1:5) = (/ 0.29789347178289445727_DP, 0.34977622651322418037_DP, &
                    & 0.23448829004405251888_DP, 0.098930459516633146976_DP, &
                    & 0.018911552143195796489_DP /)
  case(6)
     coord(1:6) = (/ 0.021634005844116948995_DP, 0.12958339115495079613_DP, &
                   & 0.31402044991476550879_DP, 0.53865721735180214454_DP, &
                   & 0.75691533737740285216_DP, 0.92266885137212023733_DP /)
     weight(1:6) = (/ 0.23876366257854756972_DP, 0.30828657327394679296_DP, &
                    & 0.24531742656321038598_DP, 0.14200875656647668542_DP, &
                    & 0.055454622324886290015_DP, 0.010168958692932275886_DP /)
  case(7)
     coord(1:7) = (/ 0.016719355408258515941_DP, 0.10018567791567512158_DP, &
                   & 0.24629424620793059904_DP, 0.43346349325703310583_DP, &
                   & 0.63235098804776608846_DP, 0.81111862674040557652_DP, &
                   & 0.94084816674334772176_DP /)
     weight(1:7) = (/ 0.19616938942524820752_DP, 0.27030264424727298214_DP, &
                    & 0.23968187200769094830_DP, 0.16577577481043290656_DP, &
                    & 0.088943227137657964435_DP, 0.033194304356571067025_DP, &
                    & 0.0059327870151259239991_DP /)
  case(8)
     coord(1:8) = (/ 0.013320244160892465012_DP, 0.079750429013894938409_DP, &
                   & 0.19787102932618805379_DP, 0.35415399435190941967_DP, &
                   & 0.52945857523491727770_DP, 0.70181452993909996383_DP, &
                   & 0.84937932044110667604_DP, 0.95332645005635978876_DP /)
     weight(1:8) = (/ 0.16441660472800288683_DP, 0.23752561002330602050_DP, &
                    & 0.22684198443191912636_DP, 0.17575407900607024498_DP, &
                    & 0.11292403024675905185_DP, 0.057872210717782072398_DP, &
                    & 0.020979073742132978043_DP, 0.0036864071040276190133_DP /)
  case(12)  ! special case for integrating 12th order polynomial approx of K0
     coord(1:12) = (/ 0.0065487222790800587892_DP, 0.038946809560449959161_DP, &
                    & 0.098150263106006628862_DP, 0.18113858159063157735_DP, &
                    & 0.28322006766737255470_DP, 0.39843443516343664370_DP, &
                    & 0.51995262679235266272_DP, 0.64051091671610645430_DP, &
                    & 0.75286501205183057837_DP, 0.85024002416230220067_DP, &
                    & 0.92674968322391410104_DP, 0.97775612968999747917_DP /)
     weight(1:12) = (/ 0.093192691443931324491_DP, 0.14975182757632236417_DP, &
                     & 0.16655745436459300532_DP, 0.15963355943698765116_DP, &
                     & 0.13842483186483562106_DP, 0.11001657063572116233_DP, &
                     & 0.079961821770828970264_DP, 0.052406954824641770650_DP, &
                     & 0.030071088873761187123_DP, 0.014249245587998279107_DP, &
                     & 0.0048999245823217609390_DP, 0.00083402903805690336469_DP /)
  case default
     print *, 'unsupported Gauss-Laguerre integration order:', intOrd
     stop
  end select
end subroutine GaussLaguerreSetup


!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function basis(xsi, nodes) result(N)
  real(DP), intent(in), dimension(:) :: xsi   ! Gauss pts to calculate basis fcn at
  integer(I4B), intent(in) :: nodes  ! number of nodes (# of basis fcns) 
  real(DP), dimension(nodes,size(xsi)) :: N ! all the basis fcns at all the pts

  integer(I4B) :: nPts

  ! first dimension of basis is the basis function
  ! second dimension of basis is the gauss pt location

  nPts = size(xsi)
  if(maxval(abs(xsi)) > ONE) print *, 'local coordinate out of range:',xsi

  select case(nodes)
  case(1)
     ! constant elements (not a function of xsi)
     N(1,1:nPts) = ONE
  case(2)
     ! linear elements
     N(1,1:nPts) = HALF*(ONE - xsi(:)) ! node 1 at xsi=-1
     N(2,1:nPts) = HALF*(ONE + xsi(:)) ! node 2 at xsi=+1
  case(3)
     ! quadratic elements
     N(3,1:nPts) = ONE - xsi(:)**2   ! node 3 at xsi=0
     N(1,1:nPts) = HALF*xsi(:)*(xsi(:) - ONE) ! node 1 at xsi=-1
     N(2,1:nPts) = HALF*xsi(:)*(xsi(:) + ONE) ! node 2 at xsi=+1
  case default
     print *, 'unsupported number of element nodes:',nodes
  end select

end function basis

! determine which element shares current node (2D, 2 node elements only)
!############################################################
function nextBC(e,n) result(otherBC)
  integer(I4B), intent(in) :: e,n ! element and node 
  integer(I4B), dimension(2) :: otherBC
  
  ! this uses global variables defined in parent;
  ! it is sloppy coding, but just moved it here for now.

  findBC: do i=1,nElement 
     if (i/=e) then ! loop over other elements
        do j=1,nodesPer ! loop over their nodes
           ! find other node with same global index
           if (connectG(j,i) == connectL(n)) then 
              otherBC(1:2) = (/ i,j /)
              exit findBC ! done, only one node to find
           end if
        end do
     end if
  end do findBC

end function nextBC


!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subroutine normalJacobian(norm,jac,xsi,nodes,coord)
  real(DP), dimension(2), intent(out) :: norm  ! normal to element
  real(DP), intent(out) :: jac         ! jacobian
  real(DP), intent(in) :: xsi          ! arc length where jac & norm are needed
  integer(I4B), intent(in) :: nodes    ! # nodes per element
  real(DP), intent(in) :: coord(:,:)   ! global coords of nodes in element

  real(DP), dimension(size(coord,dim=2)) :: Dbasis
  real(DP), dimension(2) :: tangent 


  ! derivative of basis functions
  select case(nodes)
  case(1)
     Dbasis(1) = ZERO
  case(2)
     Dbasis(1:2) = (/ -HALF, HALF /)
  case(3)
     Dbasis(1:3) = (/ xsi-HALF, xsi+HALF, -TWO*xsi /)
  end select
  
  ! sum of deriv of basis * location vector across all nodes
  tangent(1:2) = sum(spread(Dbasis(:),dim=1,ncopies=2)*coord(1:2,:),dim=2)

  norm(1) = +tangent(2)   ! cross product of tangent with k (z)
  norm(2) = -tangent(1)

  ! jacobian is length of tangent vector
  jac = sqrt(dot_product(tangent,tangent))

  ! normalize tangent vector to unit length
  if (jac > 0.0) then
     tangent(1:2) = tangent(1:2)/jac
  end if

end subroutine normalJacobian

! Green's function given a source and obs location (standard case)
!##################################################
function u(x1,x2,arg) result(pot)

  real(DP), intent(in), dimension(1:2) :: x1 ! source point
  real(DP), intent(in), dimension(1:2) :: x2 ! field point (gauss pt)
  ! assume real Laplace parameter (Stehfest method)
  real(DP), intent(in) :: arg ! other part of K0 argument, besides r
  real(DP) :: ra, pot, K0  ! distance, r*arg, potential at each point
  real(DP), dimension(2) :: rvect

  rvect(:) = x2(:) - x1(:)
  ra = arg*sqrt(dot_product(rvect,rvect))
  
  ! Green's function for 2D modified Helmholtz solution
  ! polynomial approximations to K0 from Abramowitz & Stegun
  if(ra < TWO) then
     K0 = -log(ra/TWO)*P1(ra) + P2(ra)
  else
     K0 = P3(ra)*exp(-ra)/sqrt(ra)
  end if
  pot = K0/(TWO*PI)
end function u

! normal derivative of Green's function given a source and obs location (std case)
!##################################################
function q(x1,x2,n,arg) result(flux)
 
  real(DP), intent(in), dimension(1:2) :: x1  ! source point
  real(DP), intent(in), dimension(1:2) :: x2 ! field point
  real(DP), intent(in), dimension(1:2) :: n  ! normal to bdry at field pt
  ! assume real Laplace parameter (Stehfest method)
  real(DP), intent(in) :: arg 
  real(DP) :: flux, ra, K1  ! deriviative of potential
  
  real(DP), dimension(1:2) :: rvect ! vector pointing x1 -> x2

  rvect(:) = x2(:) - x1(:)
  ra = arg*sqrt(dot_product(rvect,rvect))

  ! for mod Helmholtz solution
  if (ra < TWO) then
     K1 = ra*log(ra/TWO)*P4(ra) + P5(ra)/ra     
  else
     K1 = P6(ra)*exp(-ra)/sqrt(ra)
  end if

  flux = -dot_product(n,rvect)*arg*K1/(TWO*PI*dot_product(rvect,rvect))

end function q

!! power-series approximation of K0 (for complex z)
!! simplified by Maxima - from Abramowitz & Stegun 9.6.13+14
!!$GAMMA = 5.772156649015329D-1
!!$
!!$K0 = (z**10/1.47456d+7 + z**8/1.47456d+5 + z**6/2.304d+3 + &
!!$     & z**4/6.4d+1 + z**2/4.0d+0 + 1)*(-log(z/2.0d+0)-GAMMA) + &
!!$     & 1.548484519675926d-7*z**10 + 1.41285083912037d-5*z**8 + &
!!$     & 7.957175925925926d-4*z**6 + 2.34375d-2*z**4 + z**2/4.0d+0


! the polynomial associated with the polynomial
! approximation for I0 (9.8.1 in Abramowitz & Stegun)
function P1(x) result(poly)
  real(DP), intent(in) :: x
  real(DP) :: poly, tsq
  
  tsq = (x/3.75_DP)**2
  ! error < 1.6E-7 for x<=3.75
  
  poly = ONE + tsq*(3.5156229_DP + tsq*(3.0899424_DP + &
       & tsq*(1.2067492_DP + tsq*(0.2659732_DP + &
       & tsq*(0.0360768_DP + tsq*0.0045813_DP)))))

end function P1

! the polynomial associated with the polynomial
! approximation for K0 (9.8.5 in Abramowitz & Stegun)
function P2(x) result(poly)
  real(DP), intent(in) :: x
  real(DP) :: poly, xo2sq
  
  xo2sq = (x/2.0_DP)**2
  ! error < 1.0E-8 for x<=2
  
  poly = -0.57721566_DP + xo2sq*(0.42278420_DP + &
       & xo2sq*(0.23069756_DP + xo2sq*(0.03488590_DP + &
       & xo2sq*(0.00262698_DP + xo2sq*(0.00010750_DP + &
       & xo2sq*0.00000740_DP)))))

end function P2

! the polynomial associated with the polynomial
! approximation for K0 (9.8.6 in Abramowitz & Stegun)
function P3(x) result(poly)
  real(DP), intent(in) :: x
  real(DP) :: poly, tox
  
  tox = 2.0_DP/x
  ! error < 1.9E-7 for x>2
  
  poly = 1.25331414_DP - tox*(0.07832358_DP - &
       & tox*(0.02189568_DP - tox*(0.01062446_DP - &
       & tox*(0.00587872_DP - tox*(0.00251540_DP - &
       & tox*0.00053208_DP)))))

end function P3

! the polynomial associated with the polynomial
! approximation for I1 (9.8.3 in Abramowitz & Stegun)
function P4(x) result(poly)
  real(DP), intent(in) :: x
  real(DP) :: poly, tsq
  
  tsq = (x/3.75_DP)**2
  ! error < 8.0E-9 for x<=3.75
  
  poly = HALF + tsq*(0.87890594_DP + tsq*(0.51498869_DP + &
       & tsq*(0.15084934_DP + tsq*(0.02658733_DP + &
       & tsq*(0.00301532_DP + tsq*0.00032411_DP)))))

end function P4

! the polynomial associated with the polynomial
! approximation for K1 (9.8.7 in Abramowitz & Stegun)
function P5(x) result(poly)
  real(DP), intent(in) :: x
  real(DP) :: poly, xo2sq
  
  xo2sq = (x/2.0_DP)**2
  ! error < 8E-9 for x<=2
  
  poly = ONE + xo2sq*(0.15443144_DP - xo2sq*(0.67278579_DP + &
       & xo2sq*(0.18156897_DP + xo2sq*(0.01919402_DP + &
       & xo2sq*(0.00110404_DP + xo2sq*0.00004686_DP)))))

end function P5

! the polynomial associated with the polynomial
! approximation for K1 (9.8.8 in Abramowitz & Stegun)
function P6(x) result(poly)
  real(DP), intent(in) :: x
  real(DP) :: poly, tox
  
  tox = 2.0_DP/x
  ! error < 2.2E-7 for x>=2
  
  poly = 1.25331414_DP + tox*(0.23498619_DP - &
       & tox*(0.03655620_DP - tox*(0.01504268_DP - &
       & tox*(0.00780353_DP - tox*(0.00325614_DP - &
       & tox*0.00068245_DP)))))

end function P6

! simplified version of NR version
! Press, etal, Numerical Recipes in f90, p1016
!##################################################
subroutine ludcmp(a,indx)
  real(DP), dimension(:,:), intent(inout) :: a
  integer(I4B), dimension(:), intent(out) :: indx
  real(DP), dimension(size(a,dim=1)) :: vv
  real(DP), parameter :: TINY = 1.0E-20_DP
  integer(I4B) :: j,n,imax
  
  n = size(a,dim=1)
  vv = maxval(abs(a),dim=2)
  if (any(vv == 0.0)) print *, 'singular matrix in LUDCMP'
  vv = ONE/vv
  do j=1,n
     imax = (j-1) + sum(maxloc(vv(j:n)*abs(a(j:n,j))))
     if (j /= imax) then
        call swap(a(imax,:),a(j,:))
        vv(imax) = vv(j)
     end if
     indx(j) = imax
     if (a(j,j) == 0.0) a(j,j) = TINY
     a(j+1:n,j) = a(j+1:n,j)/a(j,j)
     a(j+1:n,j+1:n) = a(j+1:n,j+1:n) - outerprod(a(j+1:n,j),a(j,j+1:n))
  end do
end subroutine ludcmp

!##################################################
subroutine lubksb(a,indx,b)
  real(DP), intent(in), dimension(:,:) :: a
  integer(I4B), intent(in), dimension(:) :: indx
  real(DP), intent(inout), dimension(:) :: b
  
  integer(I4B) :: i,n,ii,ll
  real(DP) :: summ

  n = size(a,dim=1)
  ii = 0
  do i=1,n
     ll = indx(i)
     summ = b(ll)
     b(ll) = b(i)
     if (ii /= 0) then
        summ = summ - dot_product(a(i,ii:i-1),b(ii:i-1))
     elseif (summ /= 0.0) then
        ii = i
     end if
     b(i) = summ
  end do
  do i=n,1,-1
     b(i) = (b(i) - dot_product(a(i,i+1:n),b(i+1:n)))/a(i,i)
  end do
end subroutine lubksb

! numerical recipes utility routines
!##################################################
subroutine swap(a,b)
  real(DP), intent(inout), dimension(:) :: a,b
  real(DP), dimension(size(a)) :: tmp

  tmp = a
  a = b
  b = tmp
end subroutine swap

function outerprod(a,b) result(c)
  real(DP), intent(in), dimension(:) :: a,b
  real(DP), dimension(size(a),size(b)) :: c

  c = spread(a,dim=2,ncopies=size(b))* &
    & spread(b,dim=1,ncopies=size(a))
end function outerprod

end program biem_main
