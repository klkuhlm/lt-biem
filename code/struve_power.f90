module struve_l
  use constants, only : DP, I4B, CZERO, CONE

  implicit none

  ! modified Struve function for complex argument

contains
  subroutine struveL(ord,arg,L,num)
!!$    real, parameter :: LITTLE = epsilon(1.0D0)
    
    integer(I4B), intent(in) :: ord
    complex(DP), intent(in) :: arg
    complex(DP), intent(out) :: L
    complex(DP) :: term, denom
    integer, parameter :: MAX = 68 !! MAX must be even
    complex(DP), dimension(-1:MAX,0:MAX) :: ep
!!$    logical :: small
    integer, intent(out) :: num
    integer :: i,m,j

    ! uses power series expansion from Abramowitz & Stegun (12.2.1)
    ! convergence test: if two terms in a row are smaller than epsilon

    ! tested against Mathematica implementation of StruveL[{-1,0,1},z]
    ! it produces the same results to 14-digit precision, but takes many more
    ! iterations for large imaginary argument (10+10i) - not sure what the domain 
    ! of convergence for this power series is.

    ! if I am only using this for the singular elements, the potential issues at 
    ! large argument shouldn't be a problem

!!$    small = .false.
    ep(:,:) = CZERO

    ! sum terms
    loop: do i=0,MAX
       term = (0.5_DP*arg)**(2*i)/(gamma(real(i,DP) + 1.5_DP)&
            & *gamma(real(i+ord,DP) + 1.5_DP))

       ! add term to partial sums
       ep(0,i:MAX) = term + ep(0,i:MAX)

!!$       if(term /= term) then
!!$          print *, 'NaN',i
!!$          exit loop
!!$       end if
       

       ! loop until additional terms are < full precision
!!$       if(abs(term) < LITTLE) then
!!$          if(small) then
!!$             L = L + term
!!$             exit loop
!!$          else
!!$             small = .true.
!!$          end if
!!$       else
!!$          small = .false.
!!$       end if
!!$       L = L + term
!!$       if(i==MAX) print *, 'max iterations used:',abs(term)
    end do loop

!!$    open(unit=33, file='epsilon.out',status='old')
!!$    do j=0,MAX-1
!!$       write(33,*) 'ep(0,',j,')',ep(0,j)
!!$    end do
!!$    

    ! build up epsilon table
    do m=0,MAX-1 ! subscript
       do j=0,MAX-1  ! superscript
          denom = ep(m,j+1) - ep(m,j)
          if(abs(denom) > 0.0_DP) then
             ep(m+1,j) = ep(m-1,j+1) + CONE/denom
          else
             L = ep(m,j+1)
             goto 777
          end if
          
!!$          write(33,*) 'm,j',m+1,j,'ep:',ep(m+1,j)
       end do
    end do
    
    ! if made all the way through table use corner
    L = ep(MAX,MAX-1)
    
    ! apply factor to summation
777 continue
    L = L*(0.5_DP*arg)**(ord+1)
    num = MAX
  end subroutine struveL
end module struve_l


program test_struve
  use struve_l
  use constants, only : DP

  integer, parameter :: NUM = 100
  complex(DP), dimension(NUM) :: x
  complex(DP), dimension(NUM,-1:0) :: L
!!$  real(DP) :: alpha
  complex(DP) :: q!,p
  integer, dimension(NUM,-1:0) :: terms
  integer :: j, k
!!$
!!$  alpha = 1.0D0
!!$  p = cmplx(1.0D2, 5.0D2, DP)
  q = cmplx(7.5D1, 7.5D1, DP)
  
  x = [ (real(j,DP)/real(NUM,DP), j=0,NUM-1) ]*q
  
  do k=-1,0
     do j=1,NUM
        call struveL(k,x(j),L(j,k),terms(j,k))
     end do
  end do
  
  open(unit=20,file='struve.out',action='write')
  write(20,*) '# q=',q
  do k=-1,0
     write(20,*) '# struveL',k
     do j=1,NUM
        write(20,222) real(x(j)), aimag(x(j)), real(L(j,k)), aimag(L(j,k)), terms(j,k)
     end do
     
     write(20,*) ' '
     write(20,*) ' '
  end do
  
222 format(4(ES20.12E3,1X),I3)
end program test_struve
