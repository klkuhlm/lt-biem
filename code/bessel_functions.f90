module bessel_functions

  ! this module is a wrapper for the complex Bessel funcitons
  ! implemented by Amos, Algorithm 644 TOMS, Vol 21, No 4, 1995

  ! written by K Kuhlman, July 2006
  ! $Id: bessel_functions.f90,v 1.7 2006/09/01 20:27:33 kris Exp $

  implicit none

  interface BesselJ
     module procedure BesselJ_val, BesselJ_val_and_deriv
  end interface

  interface BesselY
     module procedure BesselY_val, BesselY_val_and_deriv
  end interface

  interface BesselI
     module procedure BesselI_val, BesselI_val_and_deriv
  end interface

  interface BesselK
     module procedure BesselK_val, BesselK_val_and_deriv
  end interface

  private
  public :: BesselJ, BesselY, BesselI, BesselK

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
contains 

  subroutine BesselJ_val(z,n,J)
    use constants, only : DP, SMALL
    use complex_bessel, only : cbesj

    complex(DP), intent(in) :: z
    integer, intent(in) :: n
    complex(DP), intent(out), dimension(0:n-1) :: J
    integer :: nz, ierr

    call cbesj(z, 0.0_DP, 1, n, J(0:n-1), nz, ierr)
    
    if (ierr /= 0) then
       select case(ierr)
       case(3)
          write(*,*) "CBESJ: loss of precision, z=",z
       case(1)
          write(*,*) "CBESJ: input error, z=",z," n=",n
          stop "CBESJ: input error"
       case(2)
          write(*,*) "CBESJ: overflow, z or order too large for unscaled output, z=",z," n=",n
          stop "CBESJ: overflow, z or order too large for unscaled output"
       case(4)
          write(*,*) "CBESJ: overflow, z or order too large, z=",z," n=",n
          stop "CBESJ: overflow, z or order too large"
       case(5)
          stop "CBESJ: algorithm termination not met"
       end select
    end if

  end subroutine BesselJ_val

  ! use recurrance relationships for derivatives
  subroutine BesselJ_val_and_deriv(z,n,J,JD)
    use constants, only : DP

    complex(DP), intent(in) :: z
    integer, intent(in) :: n
    complex(DP), intent(out), dimension(0:n-1) :: J, JD

    call BesselJ_val(z,n,J(0:n-1))

    ! middle
    JD(1:n-2) = 0.5_DP*(J(0:n-3) - J(2:n-1))

    ! low end
    JD(0) = - J(1)

    ! high end
    JD(n-1) = J(n-2) - real(n-1,DP)/z*J(n-1)

  end subroutine BesselJ_val_and_deriv

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  subroutine BesselY_val(z,n,Y)
    use constants, only : DP
    use complex_bessel, only : cbesy

    complex(DP), intent(in) :: z
    integer, intent(in) :: n
    complex(DP), intent(out), dimension(0:n-1) :: Y
    integer :: nz, ierr

    call cbesy(z, 0.0_DP, 1, n, Y(0:n-1), nz, ierr)
    
    if (ierr /= 0) then
       select case(ierr)
       case(3)
          write(*,*) "CBESY: loss of precision, z=",z
       case(1)
          write(*,*) "CBESY: input error, z=",z," n=",n
          stop "CBESY: input error"
       case(2)
          write(*,*) "CBESY: overflow, z or order too large for unscaled output, z=",z," n=",n
          stop "CBESY: overflow, z or order too large for unscaled output"
       case(4)
          write(*,*) "CBESY: overflow, z or order too large, z=",z," n=",n
          stop "CBESY: overflow, z or order too large"
       case(5)
          stop "CBESY: algorithm termination not met"
       end select 
    end if

  end subroutine BesselY_val

  ! use recurrance relationships for derivatives
  subroutine BesselY_val_and_deriv(z,n,Y,YD)
    use constants, only : DP

    complex(DP), intent(in) :: z
    integer, intent(in) :: n
    complex(DP), intent(out), dimension(0:n-1) :: Y, YD

    call BesselY_val(z,n,Y(0:n-1))

    ! middle
    YD(1:n-2) = 0.5_DP*(Y(0:n-3) - Y(2:n-2))

    ! low end
    YD(0) = - Y(1)

    ! high end
    YD(n-1) = Y(n-2) - real(n-1,DP)/z*Y(n-1)

  end subroutine BesselY_val_and_deriv

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  subroutine BesselI_val(z,n,I)
    use constants, only : DP, SMALL
    use complex_bessel, only : cbesi

    complex(DP), intent(in) :: z
    integer, intent(in) :: n
    complex(DP), intent(out), dimension(0:n-1) :: I
    integer :: nz, ierr

    call cbesi(z, 0.0_DP, 1, n, I(0:n-1), nz, ierr)
    
    if (ierr /= 0) then
       select case(ierr)
       case(3)
          write(*,*) "CBESI: loss of precision, z=",z
       case(1)
          write(*,*) "CBESI: input error, z=",z," n=",n
          stop "CBESI: input error"
       case(2)
          write(*,*) "CBESI: overflow, z or order too large for unscaled output, z=",z," n=",n
          stop "CBESI: overflow, z or order too large for unscaled output"
       case(4)
          write(*,*) "CBESI: overflow, z or order too large, z=",z," n=",n
          stop "CBESI: overflow, z or order too large"
       case(5)
          stop "CBESI: algorithm termination not met"
       end select 
    end if

  end subroutine BesselI_val

  ! use recurrance relationships for derivatives
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  ! this does not include the derivative of the argument
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  subroutine BesselI_val_and_deriv(z,n,I,ID)
    use constants, only : DP

    complex(DP), intent(in) :: z
    integer, intent(in) :: n
    complex(DP), intent(out), dimension(0:n-1) :: I, ID

    call BesselI_val(z,n,I(0:n-1))

    ! middle
    ID(1:n-2) = 0.5_DP*(I(0:n-3) + I(2:n-1))

    ! low end
    ID(0) = I(1)

    ! high end
    ID(n-1) = I(n-2) - real(n-1,DP)/z*I(n-1)

  end subroutine BesselI_val_and_deriv

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  subroutine BesselK_val(z,n,K)
    use constants, only : DP, LARGE
    use complex_bessel, only : cbesk

    complex(DP), intent(in) :: z
    integer, intent(in) :: n
    complex(DP), intent(out), dimension(0:n-1) :: K
    integer :: nz, ierr

    call cbesk(z, 0.0_DP, 1, n, K(0:n-1), nz, ierr)
    
    if (ierr /= 0) then
       select case(ierr)
       case(3)
          write(*,*) "CBESK: loss of precision, z=",z
       case(1)
          write(*,*) "CBESK: input error, z=",z," n=",n
          stop "CBESK: input error"
       case(2)
          write(*,*) "CBESK: overflow, z too small or order too large for unscaled output, z=",z," n=",n
          stop "CBESK: overflow, z too small or order too large for unscaled output"
       case(4)
          write(*,*) "CBESK: overflow, z too small or order too large, z=",z," n=",n
          stop "CBESK: overflow, z too small or order too large"
       case(5)
          stop "CBESK: algorithm termination not met"
       end select
    end if

  end subroutine BesselK_val

  ! use recurrance relationships for derivatives
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  ! this does not include the derivative of the argument
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  subroutine BesselK_val_and_deriv(z,n,K,KD)
    use constants, only : DP

    complex(DP), intent(in) :: z
    integer, intent(in) :: n
    complex(DP), intent(out), dimension(0:n-1) :: K, KD

    call BesselK_val(z,n,K(0:n-1))

    ! middle
    KD(1:n-2) = -0.5_DP*(K(0:n-3) + K(2:n-1))

    ! low end
    KD(0) = - K(1)

    ! high end
    KD(n-1) = -(K(n-2) + real(n-1,DP)/z*K(n-1))

  end subroutine BesselK_val_and_deriv



end module bessel_functions

