module stehfest_coeff
  implicit none

!!$  interface
!!$     elemental function gamma(x) result(g)
!!$       real(8), intent(in) :: x
!!$       real(8) :: g
!!$     end function gamma
!!$  end interface
  
  
  private
  public :: coeff
  
contains
function coeff(N) result(V)
  use constants, only : DP, I4B, PI, RZERO, RONE, RTWO, HALF
  integer(I4B), intent(in) :: N
  integer(I4B) :: Nh, sign

  real(DP), dimension(N) :: V ! result
  real(DP), dimension(0:N) :: G ! factorial
  integer(I4B) :: i,j,k, lo, hi
  

  if (mod(N,2) == 1) then
     print *, 'order of stehfest coefficient must be even,',N
     stop
  end if
  
  Nh = N/2;
  G = RZERO; V = RZERO

  ! this is more efficient, but has some inaccuracies??
  G(0:N) = gamma(real( (/(j+1,j=0,N)/),DP ))

! this works even without a built-in gamma function
!!$  do i = 0,N
!!$     G(i) = product(real( (/(j,j=1,i)/),DP ))
!!$  end do

  sign = (-1)**(Nh+1);

  do i = 1,N
     
     lo = floor(real(i+1)/RTWO);
     hi = min(i,Nh);

     do k = lo,hi
        V(i) = V(i) + k**Nh*G(2*k)/(G(Nh-k)*G(k)*G(k-1)*G(i-k)*G(2*k-i));
     end do

     V(i) = sign*V(i);
     sign = -sign;
  end do
end function coeff
end module stehfest_coeff
