program compute_besselk_integral
  implicit none

  use mathieu_functions, only : mmatce, mmatse, mmatKeKo
  use mcn_matrix_method
  use shared_mathieu, only : A, B

  ! M is size of 'infinite' matrix
  ! MM is maximum order of Mathieu functions to use
  integer, parameter :: M = 30, NORM = 1, MM = M-10
  real(DP) :: alpha, f, eta, psi
  complex(DP) :: p,q, total
  complex(DP), dimension(MM) :: Ke,Ko,ce,se
  integer :: i
  integer, dimension(0:MM-1) :: n = (/ i,i=0,MM-1  /)

  alpha = 1000.0_DP       ! hydraulic diffusivity
  p = (0.25_DP, 20.0_DP)  ! Laplace parameter
  f = 0.5_DP              ! semi-focal distance (1/2 line length)
  
  ! Mathieu parameter
  q = p*f**2/(2.0_DP*alpha)  ! negative in q is built into functions

  ! elliptical coordinates at the end of the line segment (x=0,y=0)
  eta = 0.0_DP  ! elliptical radial coord
  psi = PIOV2   ! elliptical angular coord

  ! calculate Mathieu characteristic numbers and matheiu coefficients for this q
  allocate(A(1:M,0:M-1,2),B(1:M,0:M-1,2))
  call mcn_matrix_method(q,A,B,NORM)
  
  ! need to determine coefficents of Mathieu function series here.  For a specified
  ! flux line element. (see mathieu_line.f90 program)
  
  ! get values of Mathieu functions
  ce(0:MM-1) = mmatce(n(0:MM-1),psi)
  se(0:MM-1) = mmatse(n(0:MM-1),psi)
  call mmatKeKo(n(0:MM-1),eta,Ke(0:MM-1),Ko(0:MM-1))

  total = Ke

  ! step through sum using MF values and coefficients calculated above
  do i=1,MM-1
     total = total + 


end program compute_besselk_integral
