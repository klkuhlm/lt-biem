set key left graph 0.4,0.9
set autoscale y
#set yrange [-2:0.25]
set xrange [0.002:5]
set logscale x
set title "Time history of potential at x=1/3\nsteady-state boundary conditions"
set xlabel 'time'
set ylabel 'potential error (BEM-FD)'
set output 'ss-pot-fdbem-difference-better.eps'
set terminal postscript eps enhanced monochrome linewidth 2.5 'Times' 22
plot 'time-fd-bem-compare.dat' u 1:($3-$2) t 'Fourier series, M=9' w lp, \
     'time-fd-bem-compare.dat' u 1:($4-$2) t 'Talbot, M=9' w lp, \
     'time-fd-bem-compare.dat' u 1:($5-$2) t 'Stehfest, N=10' w lp, \
     'time-fd-bem-compare.dat' u 1:($6-$2) t 'Shapery, M=9 + SS' w lp, \
     'time-fd-bem-compare.dat' u 1:($7-$2) t 'Weeks, M=9' w lp
## x-flux
set key graph 0.85,0.95
set yrange [-3:0.5]
set title "Time history of x-flux at x=1/3\nsteady-state boundary conditions"
set ylabel 'x-flux'
set output 'ss-xflux-time-series-01-least.eps'
plot 'time-series-01.dat' u 1:7 t 'Fourier series, M=5' w lp, \
     'time-series-01.dat' u 1:8 t 'Talbot, M=5' w lp, \
     'time-series-01.dat' u 1:9 t 'Stehfest, N=6' w lp, \
     'time-series-01.dat' u 1:10 t 'Shapery, M=5 + SS' w lp, \
     'time-series-01.dat' u 1:11 t 'Weeks, M=5' w lp

