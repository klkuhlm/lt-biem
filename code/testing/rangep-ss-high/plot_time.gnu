set key graph 0.38,0.4
set yrange [-2:0.25]
set xrange [0.002:5]
set logscale x
set title "Using same p for all t: Time history of potential at x=1/3\nsteady-state boundary conditions"
set xlabel 'time'
set ylabel 'potential'
set output 'ss-pot-rangep-better.eps'
set terminal postscript eps enhanced monochrome linewidth 2.5 'Times-Roman' 22
plot 'time-series-01.dat' u 1:2 t 'Fourier series, M=51' w lp, \
     'time-series-01.dat' u 1:3 t 'Talbot, M=51' w lp, \
     'time-series-01.dat' u 1:4 t 'Shapery, M=51 + SS' w lp, \
     'time-series-01.dat' u 1:5 t 'Weeks, M=51' w lp, \
     'heads.out' u 1:2 t 'FD solution' w l lw 2
## x-flux
set key graph 0.85,0.95
set yrange [-3.5:0.5]
set title "Using same p for all t: Time history of x-flux at x=1/3\nsteady-state boundary conditions"
set ylabel 'x-flux'
set output 'ss-xflux-rangep-better.eps'
plot 'time-series-01.dat' u 1:6 t 'Fourier series, M=51' w lp, \
     'time-series-01.dat' u 1:7 t 'Talbot, M=51' w lp, \
     'time-series-01.dat' u 1:8 t 'Shapery, M=51 + SS' w lp, \
     'time-series-01.dat' u 1:9 t 'Weeks, M=51' w lp
## y-flux#
#set title "y-flux time history in domain at x=1/3\nsteady-state boundary conditions, more accuracy"
#set ylabel 'y-flux'
#set output 'ss-yflux-time-series-01-low.eps'
#set terminal postscript eps enhanced color
#plot 'time-series-01.dat' u 1:10 t 'Fourier series' w lp, \
#     'time-series-01.dat' u 1:11 t 'Talbot' w lp, \
#     'time-series-01.dat' u 1:12 t 'Shapery' w lp, \
#     'time-series-01.dat' u 1:13 t 'Weeks' w lp
unset output
set term X11
