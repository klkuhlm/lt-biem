set key left bottom
set yrange [-1:0.5]
set xrange [0.01:0.1]
set logscale x
set title "Potential computed at several t, using the same p, at (x=1/3, y=1/3)\npulse boundary conditions"
set xlabel 'time'
set ylabel 'potential'
set output 'pulse-pot-time-series-01-comparet.eps'
set terminal postscript eps enhanced color linewidth 2.5 'Times' 22
plot 'time-series-01.dat' u 1:2 t 'Fourier series' w lp, \
     'time-series-01.dat' u 1:3 t 'Talbot' w lp, \
     'time-series-01.dat' u 1:4 t 'Shapery' w lp, \
     'time-series-01.dat' u 1:5 t 'Weeks' w lp
## x-flux
set yrange [-3:0.5]
set title "Potential computed at several t, using the same p, at (x=1/3, y=1/3)\npulse boundary conditions"
set ylabel 'x-flux'
set output 'pulse-xflux-time-series-01-comparet.eps'
set terminal postscript eps enhanced color
plot 'time-series-01.dat' u 1:6 t 'Fourier series' w lp, \
     'time-series-01.dat' u 1:7 t 'Talbot' w lp, \
     'time-series-01.dat' u 1:8 t 'Shapery' w lp, \
     'time-series-01.dat' u 1:9 t 'Weeks' w lp
## y-flux
set title "y-flux time history in domain at (1/3, 1/3)\npulse boundary conditions, more accuracy"
set ylabel 'y-flux'
set output 'pulsex-yflux-time-series-01-comparet.eps'
set terminal postscript eps enhanced color
plot 'time-series-01.dat' u 1:10 t 'Fourier series' w lp, \
     'time-series-01.dat' u 1:11 t 'Talbot' w lp, \
     'time-series-01.dat' u 1:12 t 'Shapery' w lp, \
     'time-series-01.dat' u 1:13 t 'Weeks' w lp
unset output
set term X11
