program build_laplace_biem_elements
  implicit none

  ! this program creates the input file for the biem.f90 program
  ! by discritizing the boundary (here a circle)

  integer, parameter :: DP = selected_real_kind(p=15,r=300)
  integer, parameter :: I4B = selected_int_kind(r=8)
  real(DP), parameter :: PI = 3.141592653589793238462643383279503_DP

  real(DP), allocatable :: coordG(:,:), bc(:), coordL(:,:)
  integer(I4B), allocatable :: connectL(:), connectG(:,:), type(:)

  ! geometry of the circular domain
  real(DP), parameter :: RADIUS = 2.0_DP
  real(DP) :: deltaPhi,k

  integer(I4B) :: i,j, nElement, nodesPer, intOrd, maxNode, nObsPts

  write(*,'(A)', advance='no') 'number of elements/element multiplier: '
  read(*,*) nElement
  write(*,'(A)', advance='no') 'number of nodes per element (2): '
  read(*,*) nodesPer
  write(*,'(A)', advance='no') 'Gauss quadrature order (2-8):'
  read(*,*) intOrd
  write(*,'(A)', advance='no') 'Hydraulic conductivity:'
  read(*,*) k
  
  open(unit=20, file='input.dat', status='replace', action='write')

  allocate(connectG(nodesPer,nElement),connectL(nodesPer),coordL(1:2,nodesPer))

  deltaPhi = 2.0_DP*PI/nElement
  nObsPts = nElement*3

  write(20,'(4(1X,I4),1X,ES14.6,1X,I4,1X,ES14.6)') nElement,nodesPer,intOrd, nObsPts,k

  ! evenly space out elements around a circle
  do i = 1,nElement
     connectG(1:nodesPer,i) = (/( (i-1)*(nodesPer-1) + j, j=1,nodesPer )/)
  end do

  maxNode = maxval(connectG)
  allocate(coordG(2,maxNode),bc(maxNode),type(maxNode))

  write(*,*) 'calculating coordinates of each node...'
  do i = 1, nElement
     select case(nodesPer)
!!$      case(1)
!!$         ! x coord, constant element, only (centered) node
!!$         coordG(1,(i-1)+1) = &
!!$              & RADIUS*cos((real(i-1)+0.5_DP)*deltaPhi)
!!$         ! y coord 
!!$         coordG(2,(i-1)+1) = &
!!$              & RADIUS*sin((real(i-1)+0.5_DP)*deltaPhi)
     case (2)
        ! x coord, linear element, first node (end)
        coordG(1,(i-1)+1) = RADIUS*cos((i-1)*deltaPhi)
        ! y coord 
        coordG(2,(i-1)+1) = RADIUS*sin((i-1)*deltaPhi)
        ! x coord, linear element, second node
        coordG(1,(i-1)+2) = RADIUS*cos(i*deltaPhi)
        ! y coord 
        coordG(2,(i-1)+2) = RADIUS*sin(i*deltaPhi)
!!$      case(3)
!!$         ! x coord, quadratic element, first node
!!$         coordG(1,(i-1)*3+1) = RADIUS*cos((i-1)*deltaPhi)
!!$         ! y coord
!!$         coordG(2,(i-1)*3+1) = RADIUS*sin((i-1)*deltaPhi)
!!$         ! x coord, quadratic element, third (middle) node
!!$         coordG(1,(i-1)*3+3) = RADIUS*cos((real(i)-0.5_DP)*deltaPhi)
!!$         ! y coord
!!$         coordG(2,(i-1)*3+3) = RADIUS*sin((real(i)-0.5_DP)*deltaPhi)
!!$         ! x coord, quadratic element, second (end) node
!!$         coordG(1,(i-1)*3+2) = RADIUS*cos(i*deltaPhi)
!!$         ! y coord
!!$         coordG(2,(i-1)*3+2) = RADIUS*sin(i*deltaPhi)
     case default
        print *, 'only linear elements currently supported:',nodesPer
        stop
     end select
  end do

  ! adjust polygon so last node = first node
  maxNode = maxNode-1
  ConnectG(nodesPer,nElement) = ConnectG(1,1)

  !^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
  ! set specified BC type and value (manually for now)
  ! type => BC type
  ! bc => bc value

  type(:) = 1  ! all specified head

  ! for two-node elements only
  ! (make head a smooth function around circle)
  do i=1,maxNode
     bc(i) = cos(real(i-1)*deltaPhi)
  end do

  !^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

  write(*,*) 'writing connectivity and BC of element nodes...'
  do i=1,nElement
     connectL(1:nodesPer) = connectG(1:nodesPer,i)
     write(20,*) connectL(1:nodesPer),' ',type(i),' ',bc(connectL(:))
  end do

  write(*,*) 'writing coordinates of each node...'
  do i = 1, maxNode
     write(20,'(2(1x,F10.5),1X,I1,1x,F10.5)') coordG(1:2,i)
  end do

  write(*,*) 'writing locations of points to compute results at'
  ! two circles of points at r=1/3 and 2/3 boundary radius
  do i = 1, nElement
     write(20,'(2(1X,F10.5))') 0.25_DP*RADIUS*cos(real(i-1)*deltaPhi), &
          & 0.25_DP*RADIUS*sin(real(i-1)*deltaPhi)
     write(20,'(2(1X,F10.5))') 0.5_DP*RADIUS*cos(real(i-1)*deltaPhi), &
          & 0.5_DP*RADIUS*sin(real(i-1)*deltaPhi)
     write(20,'(2(1X,F10.5))') 0.75_DP*RADIUS*cos(real(i-1)*deltaPhi), &
          & 0.75_DP*RADIUS*sin(real(i-1)*deltaPhi)
  end do


  write(*,*) 'wrote element data to input.dat'
  close(20)

end program build_laplace_biem_elements
