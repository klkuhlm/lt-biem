import numpy as np
import matplotlib.pyplot as plt

def dehoog_pvalues(t,M,T=None):
    "return optimal vector of p for given t=tmax"

    alpha = 1.0E-8
    tol = 1.0E-7
    if T == None:
        T = 2.0*t

    p = np.zeros((M+1,),dtype='complex') # standard is 2M+1
    for i in range(M+1):
        p[i] = alpha - np.log(tol)/T + np.pi*i/T*1j

    return p

def talbot_pvalues(t,M,tmax=None):
    "return optimal vector of p for fixed talbot approach"

    if tmax == None:
        tmax = t

    r = 2.0*M/(5.0*tmax)
    theta = np.linspace(0.0,np.pi,M+1)
    p = np.zeros((M,),dtype='complex')
    p[0] = r
    
    for i in range(1,M):
        p[i] = r*theta[i]*(np.cos(theta[i])/np.sin(theta[i]) + 1j)

    return p

def weeks_pvalues(t,M,tmax=None):
    "return optimal vector of p for Weeks approach"

    if tmax == None:
        tmax = t

    kappa = 1/tmax
    b = M/tmax

    # conformally mapped points 
    z = np.exp(2.0*np.pi*(1j)*(np.arange(1,M+1)-0.5)/M)

    # inverse mapping
    p = (kappa - b/2.0 + b/(1.0 - z))

    return p

def stehfest_pvalues(t,M):
    "return Stehfest p values"

    return np.arange(1,M+1)*np.log(2.0)/t
    

t = np.logspace(np.log10(0.003),np.log10(30.0),15)

M = 20

methods = [dehoog_pvalues, talbot_pvalues, weeks_pvalues, stehfest_pvalues]
colors = ['red','green','blue','cyan']

fig = plt.figure(1,figsize=(8,20))
axes = []

axes.append(fig.add_subplot(411))
axes.append(fig.add_subplot(412))
axes.append(fig.add_subplot(413))
axes.append(fig.add_subplot(414))

for m,ax,c in zip(methods,axes,colors):
    for i,tval in enumerate(t):
        p = m(tval,M)
        if not c == 'cyan' and not c == 'green':
            ax.loglog(p.real,p.imag,'x-',color=c)
        elif c == 'green':
            ax.loglog(np.abs(p.real),p.imag,'o-',color=c)
        else:
            ax.semilogx(p.real ,p.imag + 0.01*i,'o-',color=c)
        
plt.savefig('compare_pvalues.png')

