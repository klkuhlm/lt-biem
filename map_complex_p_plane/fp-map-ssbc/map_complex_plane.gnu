set dgrid 20,20,4
set hidden3d
set contour
unset key
unset zlabel
set cntrparam levels auto 20
set xlabel 'Re(p)'
set ylabel 'Im(p)'
## real potential
set title "Re(LT-BEM potential)\nsteady (1/p) boundary condition"
set term postscript eps enhanced monochrome 'Times' 22
set output 'potential_real_ssbc.eps'
splot 'fp_pot_01real.dat' u 1:2:3 w l
## imaginary potential
set title "Im(LT-BEM potential)\nsteady (1/p) boundary condition"
set output 'potential_imag_ssbc.eps'
splot 'fp_pot_01imag.dat' u 1:2:3 w l
## real xflux
set title "Re(LT-BEM x-flux)\nsteady (1/p) boundary condition"
set term postscript eps enhanced monochrome 'Times' 22
set output 'flux_real_ssbc.eps'
splot 'fp_xfl_01real.dat' u 1:2:3 w l
## imaginary potential
set title "Im(LT-BEM x-flux)\nsteady (1/p) boundary condition"
set output 'flux_imag_ssbc.eps'
splot 'fp_xfl_01imag.dat' u 1:2:3 w l
